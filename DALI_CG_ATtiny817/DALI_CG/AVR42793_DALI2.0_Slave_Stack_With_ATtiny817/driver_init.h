/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef DRIVER_INIT_H_INCLUDED
#define DRIVER_INIT_H_INCLUDED

#include <compiler.h>
#include <clock_config.h>
#include <port.h>
#include <atmel_start_pins.h>

#include <clkctrl.h>
#include <cpuint.h>
#include <slpctrl.h>
#include <bod.h>

#ifdef __cplusplus
		extern "C" {
#endif

	/**
	 * \brief System initialization
	 */
	void system_init(void);

#ifdef __cplusplus
		}
#endif

#endif /* DRIVER_INIT_H_INCLUDED */

