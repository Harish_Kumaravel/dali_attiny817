/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           main.c
 *
 * About:
 *  Example application for DALI Control Gear, using the Microchip DALI library.
 *
 * Hardware:
 *  ATTiny817 DALI Slave Demo Board 
 *
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/


#include <atmel_start.h>

// Library API
#include "dali_cg.h"

#include "random.h"

// Lamp hardware specific functionality.
#include "lamp_hardware.h"

#ifdef DALI_USE_DEVICE_TYPE_6
// LED Control Devices may implement lamp power measurements.
#include "lamp_reference_system_power.h"
#endif /* DALI_USE_DEVICE_TYPE_6 */




///*one mS tick flag*/
//volatile bool ms_tick_flag;

///*count for eeprom update period*/
//uint16_t  ms_count = 0;
///* Most time is 300 ms to save non-volatile memory */
//#define UPDATE_PERIOD   50 // 50ms for one page

void system_mcu_init(void);
//void dali_process_ms_tick(void);

/*! \brief Code of main loop.
 *
 *  This function processes dali slave protocol.
 *
 */
int main (void)
{	
        // Flags containing the status of the library, with data informing the
        // application of things such as the need to update the lamp power.
        tdali_flags_cg daliCGFlags;
        
 	system_mcu_init();
        random_init();
//        // Move to a different file
//	/* RTC from OSCULP32K is used for DALI random addressing seed */
//	/*32KHz from OSCULP32K*/
//        //RTC_CLKCTRL = 0;
//	RTC.PER = 0xFFFF;
//	/*Enable RTC*/
//	RTC.CTRLA = 0x01;
        
//        dali_tc_init();
        #ifdef DALI_USE_DEVICE_TYPE_6
//            // A successful reference system power measurement will have its results
//            // stored in non-volatile memory, such that it does not need to be rerun
//            // after a power failure.
//            lampRefSysPower_init();
            
            // LED Control Gears may be used with linear (non-logarithmic) dimming. In
            // this case, the pysical minimum level needs to be shifted to a new value
            // such that the light output at the physical minimum level is the same
            // regardless of dimming curve selection. Thus, the library needs to know
            // the corresponding value in the logarithimic table for the physical
            // minimum level.
            dali_setNonLogPhysicalMinimum(lamp_getDimmingTableValue(PHYSICAL_MINIMUM_LEVEL));
        #endif /* DALI_USE_DEVICE_TYPE_6 */
           
        dalihw_init();
         
 	dali_init();

        // Initialise the lamp control hardware, such as pin configuration and PWM
        // peripheral initialisation.
        lamp_init();
        
        // Start with all flags cleared.
        daliCGFlags.All = 0;
        
        // Enable global interrupts
        sei();
    
  	while (1) 
        {
            // Call DALI Library mainline code.
            dali_tasks();
            
//            dali_process_ms_tick();
            
            // Read flags from DALI subsystem. These are cleared (in the subsystem)
            // as soon as they're read. Bring anything "new" by ORing with the
            // previous saved state. Whenever we've finished with a flag, we'll
            // clear it in this copy as well.
            daliCGFlags.All |= dali_getFlags();

            // Grab the lamp status from the lamp subsystem and send it to the DALI
            // Library.
            dali_setStatus(lamp_getStatus());
            
            // The DALI Library may need us to start an identification procedure.
            // This is loosely defined by the DALI standard and is only called at
            // the beginning of an initialisation process. In this example we just
            // set the lamp power to maximum.
            if (daliCGFlags.startIdentificationProcedure == 1)
            {
                lamp_setPower(254, 0);
                daliCGFlags.startIdentificationProcedure = 0;
            }
            

#ifdef DALI_USE_DEVICE_TYPE_6
        if (daliCGFlags.updatedLampPower == 1)
        {
            lamp_setPower(dali_getPower(), daliCGFlags.nonLogDimming);
            // We need to clear both flags in our copy after using it to avoid
            // executing commands multiple times and to ensure correct execution
            // of the following update.
            daliCGFlags.updatedLampPower = 0;
            daliCGFlags.nonLogDimming = 0;
        }

        // Transfer the lamp operating mode to the library.
        dali_setOperatingMode(lamp_getOperatingMode());

//        // Transfer the lamp reference system power status to the library.
//        dali_setReferenceSystemPowerStatus(lampRefSysPower_getStatus());

//        // The library wants us to start a reference system power.
//        if (daliCGFlags.startReferenceSystemPower == 1)
//        {
//            lampRefSysPower_start();
//            daliCGFlags.startReferenceSystemPower = 0;
//        }
//        
//        // The library wants us to stop any running reference system power.
//        if (daliCGFlags.stopReferenceSystemPower == 1)
//        {
//            lampRefSysPower_stop();
//            daliCGFlags.stopReferenceSystemPower = 0;
//        }

//        // One ms has passed. Run the corresponding tick function for the
//        // reference system power.
//        if (daliCGFlags.tick1ms == 1)
//        {
//            lampRefSysPower_tick1ms();
//            daliCGFlags.tick1ms = 0;
//        }
#else
        if (daliCGFlags.updatedLampPower == 1)
        {
            lamp_setPower(dali_getPower(), 0);
            // We need to clear the flag in our copy after using it to avoid
            // executing commands multiple times.
            daliCGFlags.updatedLampPower = 0;
        }
#endif /* DALI_USE_DEVICE_TYPE_6 */
        
 	}
}


/**
 * \brief MCU system initialization
 */
void system_mcu_init(void)
{
	CCP = 0xD8;
	CLKCTRL.MCLKCTRLB = 0;
    /*As system power up indicator*/
    PORTA_DIRSET = 0x08;
    PORTA_OUTSET = 0x08;
}


///**
// * \brief All ms ticks used by dali
// */
//void dali_process_ms_tick(void)
//{
//	if (ms_tick_flag == true) 
//        {
//            dali_tick1ms();
////            ms_tick_flag = false;
////            dali_frame_ms_tick();
////            dali_command_fade_ms_tick();
////            ms_count++;
////            if (ms_count >= UPDATE_PERIOD) 
////            {
////              dali_eeprom_update_tick();
////              ms_count = 0;
////            }
//	}
//}


/*TCA ms interrupt*/
ISR(TCA0_OVF_vect)
{
	/*Clear OVF flag*/
#if defined(__GNUC__)
	TCA0_SINGLE_INTFLAGS = 0x01;
#elif defined(__ICCAVR__)
	TCA0.SINGLE.INTFLAGS = 0x01;	
//	TCA0.INTFLAGS = 0x01;
#endif	
        dali_tick1ms();
//	ms_tick_flag = true;
}


ISR(TCB0_INT_vect)
{
    /*Clear interrupt flag firstly*/
    dalihw_teTimerClearInterrupt();
    dali_interruptTeTimer();
}


ISR(PORTB_PORT_vect)
{
    /*Clear interrupt flag firstly*/
    PORTB.INTFLAGS = 0x01 << 3;
    dali_interruptExternal();
}
