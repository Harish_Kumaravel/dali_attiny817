/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <system.h>

void system_init()
{
	mcu_init();

	CLKCTRL_init();

	CPUINT_init();

	SLPCTRL_init();

	BOD_init();
}

