/**
 * \file
 *
 * \brief HAL(Hardware abstract layer)
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#include "dali_top.h"
#include "random.h"


/** \brief Obtain a truly random number.
 *
 * This is used to initialise the pseudo-random number generator included in
 * stdlib. The implementation generates a random number by sampling AN9,10
 * (RF4,5) which on the demo board are not connected.
 *
 * @return A random number.
 */
static uint8_t random_trulyRandomNumber();


/**
 * \brief Initialize ADC interface
 */
int8_t ADC_0_init()
{

	// ADC0.CALIB = ADC_DUTYCYC_DUTY50_gc; /* 50% Duty cycle */

	// ADC0.CTRLB = ADC_SAMPNUM_ACC1_gc; /* 1 ADC sample */

	// ADC0.CTRLC = ADC_PRESC_DIV2_gc /* CLK_PER divided by 2 */
	//		 | ADC_REFSEL_INTREF_gc /* Internal reference */
	//		 | 0 << ADC_SAMPCAP_bp; /* Sample Capacitance Selection: disabled */

	// ADC0.CTRLD = 0 << ADC_ASDV_bp /* Automatic Sampling Delay Variation: disabled */
	//		 | 0 << ADC_SAMPDLY_gp /* Sampling Delay Selection: 0 */
	//		 | ADC_INITDLY_DLY0_gc; /* Delay 0 CLK_ADC cycles */

	// ADC0.CTRLE = ADC_WINCM_NONE_gc; /* No Window Comparison */

	// ADC0.DBGCTRL = 0 << ADC_DBGRUN_bp; /* Debug run: disabled */

	// ADC0.EVCTRL = 0 << ADC_STARTEI_bp; /* Start Event Input Enable: disabled */

	// ADC0.INTCTRL = 0 << ADC_RESRDY_bp /* Result Ready Interrupt Enable: disabled */
	//		 | 0 << ADC_WCMP_bp; /* Window Comparator Interrupt Enable: disabled */

	// ADC0.MUXPOS = ADC_MUXPOS_AIN0_gc; /* ADC input pin 0 */

	// ADC0.SAMPCTRL = 0 << ADC_SAMPLEN_gp; /* Sample length: 0 */

	// ADC0.WINHT = 0; /* Window Comparator High Threshold: 0 */

	// ADC0.WINLT = 0; /* Window Comparator Low Threshold: 0 */

	ADC0.CTRLA = 0 << ADC_ENABLE_bp     /* ADC Enable: disabled */
	             | 0 << ADC_FREERUN_bp  /* ADC Freerun mode: disabled */
	             | ADC_RESSEL_10BIT_gc  /* 10-bit mode */
	             | 0 << ADC_RUNSTBY_bp; /* Run standby mode: disabled */

	return 0;
}

void ADC_0_enable()
{
	ADC0.CTRLA |= ADC_ENABLE_bm;
}

void ADC_0_disable()
{
	ADC0.CTRLA &= ~ADC_ENABLE_bm;
}

void ADC_0_start_conversion(adc_0_channel_t channel)
{
	ADC0.MUXPOS  = channel;
	ADC0.COMMAND = ADC_STCONV_bm;
}

bool ADC_0_is_conversion_done()
{
	return (ADC0.INTFLAGS & ADC_RESRDY_bm);
}

adc_result_t ADC_0_get_conversion_result(void)
{
	return (ADC0.RES);
}

adc_result_t ADC_0_get_conversion(adc_0_channel_t channel)
{
	adc_result_t res;

	ADC_0_start_conversion(channel);
	while (!ADC_0_is_conversion_done())
		;
	res = ADC_0_get_conversion_result();
	ADC0.INTFLAGS |= ADC_RESRDY_bm;
	return res;
}


void random_init()
{
    srand((random_trulyRandomNumber() << 8) | random_trulyRandomNumber());
}


uint8_t random_byte()
{
    return (uint8_t)rand();
}


static uint8_t random_trulyRandomNumber()
{
    uint8_t mess = 0;

    do
    {
        ADC_0_enable();
        
        mess += (uint8_t)(ADC_0_get_conversion(ADC_MUXPOS_AIN6_gc) & 0x00FF);
        
        mess += (uint8_t)((ADC_0_get_conversion(ADC_MUXPOS_AIN7_gc)) >> 8);
        
        mess++;
    } while (mess == 0 || mess == 0xFF);

    return mess;
}
