/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           lamp_reference_system_power.h
 *
 * About:
 *  Lamp reference system power definitions.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013   Implement the dummy reference system
 *                                      power mechanism for the demo application.
 ******************************************************************************/


#ifndef LAMP_REFERENCE_SYSTEM_POWER_H
#define	LAMP_REFERENCE_SYSTEM_POWER_H


#include <stdint.h>
#include "dali_cg.h"


#ifdef DALI_USE_DEVICE_TYPE_6

//// The reference system power module needs to store this many bytes in
//// non-volatile memory. The demo application just uses one byte to store whether
//// the measurement has been run successfully or not. Full implementations might
//// need to store power curves for different lamp settings.
//#define REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED        1
//
//// Since just one non-volatile byte is used, just one such location is defined,
//// at address 0. This is used as the location parameter for functions
//// nvmem_refSysPowerWriteByt() and nvmem_refSysPowerReadByte().
//#define REFERENCE_SYSTEM_POWER_RUN_STATUS               0
//
//
///** \brief Initialise reference system power subsystem.
// *
// * This should make the subsystem able to answer queries regarding its status.
// * The current implementation reads its most recently stored status from
// * non-volatile memory.
// */
//void lampRefSysPower_init();
//
//
///** \brief 1ms tick provided to the subsystem.
// *
// * This implementation of the reference system power subsystem requires a 1ms
// * tick. During a reference system power measurement it pretends to be doing
// * something for 10 seconds, and as such, needs to be informed on the passage
// * of time.
// */
//void lampRefSysPower_tick1ms();
//
//
///** \brief Start a reference system power measurement.
// *
// * This example implementation has a "dummy" reference system power mechanism.
// * It pretends to be doing something for 10 seconds and then signals success
// * (if not disturbed).
// */
//void lampRefSysPower_start();
//
//
///** \brief Abort the reference system power measurement.
// *
// * The DALI state machine may require us to stop the current measurement.
// */
//void lampRefSysPower_stop();
//
//
///** \brief Query the status of the reference system power measurement.
// *
// * The DALI state machine needs to know the status of the reference system
// * power. This function needs to provide it such that the application can use
// * this value to update the copy of the status in the library.
// *
// * @return 
// *  - REFERENCE_SYSTEM_POWER_FAILED -- Reference system power failed \n
// *  - REFERENCE_SYSTEM_POWER_RUN_OK -- Reference system power has been run
// *    successfully \n
// *  - REFERENCE_SYSTEM_POWER_RUNNING -- Reference system power running
// */
//uint8_t lampRefSysPower_getStatus();

#endif /* DALI_USE_DEVICE_TYPE_6 */


#endif	/* LAMP_REFERENCE_SYSTEM_POWER_H */

