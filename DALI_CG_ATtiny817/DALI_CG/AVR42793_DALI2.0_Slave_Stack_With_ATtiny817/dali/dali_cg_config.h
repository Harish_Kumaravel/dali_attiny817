/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg_config.h
 *
 * About:
 *  Configuration file for the DALI library.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/


#ifndef DALI_CG_CONFIG_H
#define	DALI_CG_CONFIG_H


// This file contains DALI Library configuration. These defines are used in the
// library to enable specific functionality. They may also be used in the
// application code such that the application doesn't provide functionality not
// needed since the underlying support for that functionality has been disabled.


// This symbol enables the usage of memory banks > 0. Memory bank 0 is only
// implemented as read-only by this DALI stack, but further memory banks can be
// written to (if allowed by the application). This way, disabling the usage
// of the non-mandatory memory banks ~200 words can be saved (on an Enhanced
// Mid-Range PIC), while retaining full compatibility with the DALI standard.
//#define DALI_USE_EXTRA_MEMORY_BANKS


// The usage of this define enables the LED specific functionality of the DALI
// stack. Removing this saves ~700 words (on an Enhanced Mid-Range PIC).
#define DALI_USE_DEVICE_TYPE_6

// Constants used by the DALI machine
#define PHYSICAL_MINIMUM_LEVEL          1       // Lamp physical minimum power level
#define DAPC_SEQUENCE_FADE_TIME_MS      200     // Fast fade time (in ms) used by DAPC sequence

// Constants used by the DALI machine for Device Type 6 (LED) devices
#ifdef DALI_USE_DEVICE_TYPE_6
#define GEAR_TYPE                       0x0B    // See IEC 62386-207, 11.3.4.2, Command 237 (page 15)
#define MIN_FAST_FADE_TIME              1       // Minimum fast fade time
#define POSSIBLE_OPERATING_MODES        0x04    // See IEC 62386-207, 11.3.4.2, Command 239 (page 15)
#define FEATURES                        0xFF    // See IEC 62386-207, 11.3.4.2, Command 240 (page 16)
#endif /* DALI_USE_DEVICE_TYPE_6 */


#endif	/* DALI_CG_CONFIG_H */

