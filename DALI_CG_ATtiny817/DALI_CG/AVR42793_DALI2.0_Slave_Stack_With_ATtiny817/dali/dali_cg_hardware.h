/**
 * \file
 *
 * \brief Decode and encode dali transmission bits
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#ifndef DALI_CG_HARDWARE_H
#define DALI_CG_HARDWARE_H



/*Must select clock source by MCU FUSE*/
#ifndef F_CPU
#define F_CPU 16000000UL
#endif


/*System 1mS tick*/
#define SYS_TICK_TC_TOP  (F_CPU/DALI_PWM_CLOCK_HZ)
#define ONE_MS_TICK		 1  // 1mS tick
#define DALI_PWM_CLOCK_HZ   1000


/*DALI bit timing*/
#define DALI_BIT_TC_TOP   (F_CPU/2400/TE_VALUE)  // DALI bard rate 2400


/*Tick count per half bit*/
#define TE_VALUE              13 


/* PWM timer*/    
#define DALI_PWM_TC_TOP     0x0FFF  // 12bits resolution 


//// The timer used to generate the DALI delays is often being reloaded with
//// values based on its overflow value and the TE value defined further below.
//// Here we use a 16-bit timer, thus its overflow value is 2^16.
//#define TE_TIMER_OVERFLOW_VAL           65536
//
//
//// The number of timer clocks that make up a 416us delay. The timer is
//// configured to run at 2MHz, thus our delay will be 2MHz * 416.(6)us = 833.
//#define TE                              833


// There's a layer of inversion in the optocouplers' transistors on the board
// and we define these two symbols such that other hardware configurations can
// easily be adopted.
#define DALI_HI                         1
#define DALI_LO                         0


//
//#include <xc.h>
#include <stdint.h>
#include "dali_cg.h"


/** \brief Initialise Te Timer.
 *
 * In this application Timer 1 is used as the Te Timer. This function configures
 * it to run at 2MHz and enables its interrupt.
 */
inline void dalihw_teTimerInit();


/** \brief Turn on Te Timer.
 *
 */
inline void dalihw_teTimerOn()
{
    /*Enable TCB*/
    TCB0.CTRLA = 0x01;
//    TMR1ON = 1;
}

//inline void dalihw_teTimerOn();


/** \brief Turn off Te Timer.
 *
 */
inline void dalihw_teTimerOff()
{
    /*Disable TCB*/
    TCB0.CTRLA = 0x00;
//    TMR1ON = 0;
}

//inline void dalihw_teTimerOff();


/** \brief Clear Te Timer interrupt flag.
 *
 */
//inline void dalihw_teTimerClearInterrupt()
//{
//    /*Clear interrupt flag*/
//    TCB0.INTFLAGS = 0x01;
////    TMR1IF = 0;
//}

//inline void dalihw_teTimerClearInterrupt();


/** \brief Read Te Timer value.
 *
 * @return Te Timer value.
 */
inline uint16_t dalihw_teTimerGetVal();


/** \brief Set Te Timer value.
 *
 * @param val Value to reload Te Timer with.
 */
inline void dalihw_teTimerSetVal(uint16_t val);


/** \brief Set the External Interrupt to fire on the falling edge of the DALI
 * bus.
 *
 * In this application, the "rising" and "falling" in the function names refer
 * to the DALI bus, but there's a layer of inversion in the optocouplers'
 * transistors on the board. Thus, what is called in the function names "rising"
 * is a falling edge on the PIC's pin.
 */
inline void dalihw_extInterruptSetFallingDALI();


/** \brief Set the External Interrupt to fire on the rising edge of the DALI
 * bus.
 *
 * In this application, the "rising" and "falling" in the function names refer
 * to the DALI bus, but there's a layer of inversion in the optocouplers'
 * transistors on the board. Thus, what is called in the function names "rising"
 * is a falling edge on the PIC's pin.
 */
inline void dalihw_extInterruptSetRisingDALI();


/** \brief Toggle the interrupt edge on which the External Interrupt fires.
 *
 */
inline void dalihw_extInterruptToggleEdge();


/** \brief Check whether the DALI line is low or not.
 *
 * This is used to detect cable disconnection (in order to set the lamp power
 * level to system failure level)
 *
 * @return 1 if line is low, 0 otherwise.
 */

inline uint8_t dalihw_isDALILineLow() // Used to detect whether a forward frame is being received
{
    return ((PORTB_IN & (0x01 << 3)) == DALI_LO);
//    return (DALI_RX == DALI_LO);
}

//inline uint8_t dalihw_isDALILineLow();


/** \brief Get the value of DALI RX line.
 *
 * @return DALI_RX value.
 */
inline uint8_t dalihw_getDALIRXLineValue(); 

/** \brief Pull DALI bus to the low level.
 *
 */
inline void dalihw_setDALILineLow() // SEND_BACKWARD_DATA in timer interrupt
{
    PORTB_OUTCLR = 0x01 << 4;  // Low level
//    DALI_TX = DALI_LO;
}
//inline void dalihw_setDALILineLow();


/** \brief Release DALI bus such that the IPS can pull it to the high level.
 *
 */
inline void dalihw_setDALILineHigh() // SEND_BACKWARD_DATA in timer interrupt
{
    PORTB_OUTSET = 0x01 << 4;  // High level
//    DALI_TX = DALI_HI;
}
//inline void dalihw_setDALILineHigh();



/** \brief Get the value of DALI TX line.
 *
 * @return DALI_TX value.
 */
inline uint8_t dalihw_getDALITXLineValue(); 

#endif /* DALI_CG_HARDWARE_H */
