/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           lamp_reference_system_power.c
 *
 * About:
 *  Implementation of a lamp reference system power measurement system. This is
 *  defined for Device Type 6 (LED) devices and is hardware-specific. Based on
 *  these reference measurements, the lamp status can be updated by periodically
 *  measuring its power. The current implementation just pretends to be doing
 *  something for 10 seconds, as no measurements on the lamp can be made using
 *  the Microchip Lighting Communications Main Board.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013   Implement the dummy reference system
 *                                      power mechanism for the demo application.
 ******************************************************************************/

#include "lamp_reference_system_power.h"

#ifdef DALI_USE_DEVICE_TYPE_6

#include "dali_cg_nvmemory.h"


/**********************Reference system power variables************************/


///** \brief Shadow variable of EEreferenceSystemPower. */
//static uint8_t referenceSystemPower;
//
//
///** \brief Reference system power time-out.
// *
// * The reference system power measurement is simulated in this application
// * example, and is implemented as a time-out.
// */
//static uint16_t referenceSystemPowerTimeout_ms = 0;


/**********************Reference system power variables*(END)******************/


/**********************Function implementations********************************/

//
//void lampRefSysPower_init()
//{
//    // Read reference system power from EEPROM, if this is implemented
//    referenceSystemPower = nvmem_refSysPowerReadByte(REFERENCE_SYSTEM_POWER_RUN_STATUS);
//}


//void lampRefSysPower_tick1ms()
//{
//    if (referenceSystemPowerTimeout_ms > 0)
//    {
//        referenceSystemPowerTimeout_ms--;
//        if (referenceSystemPowerTimeout_ms == 0)
//        {
//            referenceSystemPower = 0xAA;
//        }
//    }
//
//    // The user can signal a disturbance in the reference system power by
//    // pressing the following button combination on the board
//    // S5 + S4 = Disturb reference system power
//    if ((PORTDbits.RD2 == 0) && (PORTDbits.RD4 == 0))
//    {
//        lampRefSysPower_stop();
//    }
//
//    // If the shadow variable has been changed, save the change to EEPROM as
//    // well
//    if (referenceSystemPower != nvmem_refSysPowerReadByte(REFERENCE_SYSTEM_POWER_RUN_STATUS))
//    {
//        nvmem_refSysPowerWriteByte(REFERENCE_SYSTEM_POWER_RUN_STATUS, referenceSystemPower);
//    }
//}

//void lampRefSysPower_start()
//{
//    // Pretend we're doing something for 10 seconds
//    referenceSystemPowerTimeout_ms = 10000;
//}

//void lampRefSysPower_stop()
//{
//    // Measurement stopped, store 0 in EEPROM such that we know no reference
//    // system power was done
//    referenceSystemPower = 0;
//    referenceSystemPowerTimeout_ms = 0;
//}


//uint8_t lampRefSysPower_getStatus()
//{
//    if (referenceSystemPowerTimeout_ms != 0)
//    {
//        // Still running
//        return REFERENCE_SYSTEM_POWER_RUNNING;
//    }
//
//    if (referenceSystemPower == 0)
//    {
//        // Not running, not successfully run
//        return REFERENCE_SYSTEM_POWER_FAILED;
//    }
//
//    // Reference system power has been successfully run
//    return REFERENCE_SYSTEM_POWER_RUN_OK;
//}


/**********************Function implementations*(END)**************************/

#endif /* DALI_USE_DEVICE_TYPE_6 */

