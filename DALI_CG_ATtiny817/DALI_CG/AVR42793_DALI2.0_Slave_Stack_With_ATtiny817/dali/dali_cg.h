/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           dali_cg.h
 *
 * About:
 *  Definitions for the DALI API.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date		Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Shaima Husain     15 February 2012
 * Michael Pearce    09 May 2012         Update to use the newer API
 * Mihai Cuciuc      28 November 2013    Changed application to use the new
 *                                       DALI Control Gear Library
 ******************************************************************************/

#ifndef DALI_CG_H
#define	DALI_CG_H


#include <stdint.h>
#include "dali_top.h"

// Needed to be in tune with library configuration. DALI_USE_DEVICE_TYPE_6 is
// defined (if needed) in this file.
#include "dali_cg_config.h"


// The function dali_setOperatingMode should be called with one of these
// parameters:
#define OPERATING_MODE_PWM_MODE_ACTIVE          0x01
#define OPERATING_MODE_AM_MODE_ACTIVE           0x02
#define OPERATING_MODE_CURRENT_CONTROLLED       0x04
#define OPERATING_MODE_HIGH_CURRENT_PULSE       0x08


// The function dali_setStatus() should be called by ORing together all the
// following values that apply:
#define LAMP_STATUS_OK                          0x00
#define LAMP_STATUS_OPEN_CIRCUIT                0x01
#define LAMP_STATUS_SHORT_CIRCUIT               0x02
#define LAMP_STATUS_LOAD_INCREASE               0x04
#define LAMP_STATUS_LOAD_DECREASE               0x08
#define LAMP_STATUS_THERMAL_OVEROAD             0x10
#define LAMP_STATUS_THERMAL_SHUTDOWN            0x20

//// The dali_setReferenceSystemPowerStatus() function should be called with one
//// of the following parameters:
//#define REFERENCE_SYSTEM_POWER_FAILED           0x00
//#define REFERENCE_SYSTEM_POWER_RUN_OK           0x01
//#define REFERENCE_SYSTEM_POWER_RUNNING          0x02


// The library holds these flags in order to pass single-bit data to the
// application.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned tick1ms                            : 1;    // 1ms has passed
        unsigned updatedLampPower                   : 1;    // the lamp power needs to be updated
        unsigned nonLogDimming                      : 1;    // the linear dimming needs to be used for the lamp power
        unsigned startIdentificationProcedure       : 1;    // the Control Gear identification procedure needs to be started
        unsigned startReferenceSystemPower          : 1;    // a reference system power measurement needs to be started
        unsigned stopReferenceSystemPower           : 1;    // any running reference system power measurement needs to be stopped
        unsigned                                    : 2;
    };
}tdali_flags_cg;


/** \brief DALI communication hardware configuration.
 *
 * This function configures DALI pin tristate registers, their initial values,
 * configures the Te Timer and sets up the required interrupt enables.
 */
void dalihw_init();


/** \brief Clear Te Timer interrupt flag.
 *
 */
inline void dalihw_teTimerClearInterrupt()
{
    /*Clear interrupt flag*/
    TCB0.INTFLAGS = 0x01;
//    TMR1IF = 0;
}

//inline void dalihw_teTimerClearInterrupt();


/** \brief Enable Te Timer interrupt flag.
 *
 */
inline void dalihw_teTimerDisableInterrupt()
{
    /*Disable TCB interrupt*/
    TCB0.INTCTRL = 0x00;
}


/** \brief Enable Te Timer interrupt flag.
 *
 */
inline void dalihw_teTimerEnableInterrupt()
{
    /*Enable TCB interrupt*/
    TCB0.INTCTRL = 0x01;
}


/** \brief Clear external interrupt flag.
 *
 */
inline void dalihw_extInterruptClearInterrupt()
{
    /*Clear interrupt flag*/
    PORTB.INTFLAGS = 0x01 << 3;
}

///** \brief Check if the Te Timer interrupt triggered.
// *
// * This function just checks if the IF and IE bits for the Te Timer are 1.
// *
// * @return 1 if Te Timer triggered, 0 otherwise.
// */
//inline uint8_t dalihw_teTimerInterruptTriggered();


/** \brief API Call: The application should call this function for every Te
 * Timer interrupt.
 *
 * This function, together with the one that handles the external interrupt run
 * the mechanism that handle the DALI data transmission and reception.
 */
void dali_interruptTeTimer();


/** \brief API Call: The application should call this function for every
 * external interrupt.
 *
 * This function, together with the one that handles the Te Timer run the
 * mechanism that handle the DALI data transmission and reception.
 */
void dali_interruptExternal();


/** \brief API Call: Initialise the DALI library.
 *
 * This function triggers the initialisation of the DALI library.
 */
void dali_init();


/** \brief API Call: Run one iteration of the DALI state machine which, among
 * others, checks if any new DALI frame has arrived and if so, processes it.
 *
 * This function should be called from the main while(1) loop and should be
 * executed as often as possible. It does the following: \n
 *  - checks for any DALI frame on the bus and if one exists it processes it \n
 *  - checks if the machine is in reset state by checking the relevant values \n
 *  - checks lamp operating mode, by looking at the value provided by the
 *    application \n
 *  - checks the status of reference system power by looking at the value
 *    provided by the application \n
 *  - sets the lamp failure and lamp power on bits according to the lamp power
 *    level and the lamp status provided by the application \n
 *  - sets the various lamp status bits defined by the DALI LED
 *    specifications, if Device Type 6 is enabled \n
 *  - if any memory bank has been modified, recomputes the checksum for one of
 *    them and updates it \n
 *  - if the lamp power needs to be changed, update the value and inform the
 *    application that it should change the lamp power
 */
void dali_tasks();


/** \brief API Call: Library code that should be run every millisecond.
 *
 * Code from main should call this function once every millisecond. It is used
 * to derive a time base for time-outs, to detect cable disconnection and to run
 * the lamp fading mechanism.
 */
inline void dali_tick1ms();
//inline void dali_tick1ms();


/** \brief API Call: Obtain the DALI library flags.
 *
 * The DALI library passes to the application some flags encoding the commands
 * that the application should run. These are packed in a byte whose structure
 * is defined by the tdali_flags_cg type. These flags encode if a millisecond
 * has passed (for mainline code), if the lamp power should be updated,
 * reference system power commands and if the identification procedure should be
 * started.
 *
 * @return Byte representing the flags, as tdali_flags_cg defines them.
 */
uint8_t dali_getFlags();


/** \brief API Call: Obtain the power that the application should command the
 * lamp to go to.
 *
 * Whenever the updateLampPower bit is set in the library flags, the lamp power
 * returned by this function should be used to drive the lamp. The nonLogDimming
 * flag instructs on the usage of the logarithmic or linear dimming curves.
 *
 * @return DALI lamp power [0..254]
 */
uint8_t dali_getPower();


/** \brief API Call: Inform the library of the lamp status.
 *
 * The DALI library needs to have access to a recent status of the lamp. The
 * user application should use this function to update this status according
 * to measurements from the lamp (if such measurements are implemented).
 *
 * @param status Lamp status, as a bitmask. The following constants define
 * possible states: \n
 *  LAMP_STATUS_OK \n
 *  LAMP_STATUS_OPEN_CIRCUIT \n
 *  LAMP_STATUS_SHORT_CIRCUIT \n
 *  LAMP_STATUS_LOAD_INCREASE \n
 *  LAMP_STATUS_LOAD_DECREASE \n
 *  LAMP_STATUS_THERMAL_OVEROAD \n
 *  LAMP_STATUS_THERMAL_SHUTDOWN
 */
void dali_setStatus(uint8_t status);


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief API Call: Inform the library of the lamp operating mode.
 *
 * The DALI library needs to have access to a recent value for the lamp
 * operating mode. The user application should use this function to update this
 * status according to information from the lamp (if available).
 *
 * @param mode Lamp operating mode, as one of the following constants: \n
 *  OPERATING_MODE_PWM_MODE_ACTIVE \n
 *  OPERATING_MODE_AM_MODE_ACTIVE \n
 *  OPERATING_MODE_CURRENT_CONTROLLED \n
 *  OPERATING_MODE_HIGH_CURRENT_PULSE
 */
void dali_setOperatingMode(uint8_t mode);


/** \brief API Call: Inform the library of the non-lograithmic physical minimum
 * level.
 *
 * The DALI library, when implementing Device Type 6 (LED) devices, needs to be
 * able to use either the normal (logarithmic) dimming curve or a linear one.
 * The constraint is that the physical minimum level should be adjusted such
 * that the light output stays the same at physical minimum level regardless of
 * dimming curve choice. Thus, this value needs to be known to the library and
 * the user application needs to use this function to set it.
 *
 * @param level Non-logarithmic physical minimum level.
 */
void dali_setNonLogPhysicalMinimum(uint8_t level);


///** \brief API Call: Inform the library of the reference system power status.
// *
// * The DALI library needs access to the status of the last reference system
// * power measurement. The user application should use this function to set it.
// *
// * @param status Reference system power status, as one of the following
// * constants: \n
// *  REFERENCE_SYSTEM_POWER_FAILED \n
// *  REFERENCE_SYSTEM_POWER_RUN_OK \n
// *  REFERENCE_SYSTEM_POWER_RUNNING
// */
//void dali_setReferenceSystemPowerStatus(uint8_t status);

#endif /* DALI_USE_DEVICE_TYPE_6 */

#endif	/* DALI_CG_H */

