/**
 * \file
 *
 * \brief Decode and encode dali transmission bits
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#include "dali_top.h"
#include "dali_cg_hardware.h"
#include "dali_cg_machine.h"
//#include "dali_cmd.h"



#include "dali_cg_hardware.h"


//#define DALI_RX                         RB0         // RX line input register bit
//#define DALI_RX_TRIS                    TRISB0      // RX line tristate register bit
//#define DALI_TX                         LATC5       // TX line output register bit
//#define DALI_TX_TRIS                    TRISC5      // TX line tristate register bit



/**********************Function implementations********************************/



//inline uint8_t dalihw_teTimerInterruptTriggered()
//{
//    return (TMR1IF != 0) && (TMR1IE != 0);
//}


extern inline void dalihw_teTimerOn();

extern inline void dalihw_teTimerOff();

extern inline void dalihw_setDALILineLow(); // SEND_BACKWARD_DATA in timer interrupt

extern inline void dalihw_setDALILineHigh(); // SEND_BACKWARD_DATA in timer interrupt

extern inline uint8_t dalihw_isDALILineLow(); // Used to detect whether a forward frame is being received

extern inline void dalihw_teTimerClearInterrupt();

extern inline void dalihw_teTimerDisableInterrupt();

extern inline void dalihw_teTimerEnableInterrupt();

extern inline void dalihw_extInterruptClearInterrupt();








inline void dalihw_teTimerInit()
{
      // Timer TCB configuration. This timer is used by the DALI protocol to
      // generate the Te timings and time-outs. It is called "Te Timer" throughout
      // this library and the dalihw_teTimer...() functions refer to this timer.
      // Frequency 31.2KHz
      // TOP value
      TCB0.CCMP = DALI_BIT_TC_TOP;
      // 0bxxxxx000 - Periodic interrupt mode
      TCB0.CTRLB = 0;
      // 0bxxxxxxx1 - Enable TCB
      // 0bxxxxx00x - Clock Select CLK_PER
      TCB0.CTRLA = 0x01;
      
      /*Disable interrupt first*/
      dalihw_teTimerDisableInterrupt();
  //    TCB0.INTCTRL = 0x00;

      // Clear timer interrupt flag
      dalihw_teTimerClearInterrupt();

      // Enable TCB timing interrupt and keep it enabled. Will turn timer TCB 
      // on/off as needed
      dalihw_teTimerEnableInterrupt();
  //    TCB0.INTCTRL = 0x01;
}


void dalihw_init()
{
  
      CCP = 0xD8;
      CLKCTRL.MCLKCTRLB = 0;
      /*As system power up indicator*/
      PORTA_DIRSET = 0x08;
      PORTA_OUTSET = 0x08;
      
      /* Configure PB4 as DALI output */
      // TX line
      PORTB_DIRSET = 0x01 << 4;
      dalihw_setDALILineHigh();
      
      /* Configure PB3 as DALI input */
      // RX line
      PORTB_DIRCLR = 0x01 << 3;
      
      /*Sense both edges*/
      PORTB_PIN3CTRL = 0x01;
      
      // Configure Te Timer
      dalihw_teTimerInit();
      
//      dali_tc_init();
      dali_bod_init();
    
  #if defined(__GNUC__)
      /* TCA used as 1mS tick for system timing */
      TCA0.SINGLE.PER = SYS_TICK_TC_TOP;
      /*Normal mode*/
      TCA0.SINGLE.CTRLB = 0x00;
      /*Enable overflow interrupt*/
      TCA0.SINGLE.INTCTRL = 0x01;
      /*Enable TCA*/
      TCA0.SINGLE.CTRLA = 0x01;
  #elif defined(__ICCAVR__)
      /* TCA used as 1mS tick for system timing */
      TCA0.SINGLE.PER = SYS_TICK_TC_TOP;
      /*Normal mode*/
      TCA0.SINGLE.CTRLB = 0x00;
      /*Enable overflow interrupt*/
      TCA0.SINGLE.INTCTRL = 0x01;
      /*Enable TCA*/
      TCA0.SINGLE.CTRLA = 0x01;
  #endif
      
      // Clear external interrupt flag
      PORTB.INTFLAGS = 0x01 << 3;
}


/**********************Function implementations*(END)**************************/
