/**
 * \file
 *
 * \brief Process dali eeprom
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#ifndef DALI_EEPROM_H
#define DALI_EEPROM_H

#include <stdint.h>
#include "dali_cg_config.h"

// Note that the memory banks as defined by the DALI standard are not related to
// the banks defined by the PIC architecture. For further description of the
// DALI memory banks please refer to IEC62386-102, section 9.8.
// The number of banks in this implementation of the library is currently
// limited to 8 (checksumChange flags size).


// DALI bank 0 values. This bank's structure is defined by the standard up to
// location 0x0E. These first 15 values are read-only, further locations and
// their writability can be freely chosen by the control gear manufacturer.
// However, the present DALI machine does not allow any writing to bank 0. As
// such, in this application example we only implement these read-only locations
// for bank 0 such that we can store these values in program space, saving
// the EEPROM for actual writable data. Note that other implementations might
// choose to use EEPROM for these read-only values as well to save program
// space.
// Bank 0 is the only mandatory memory bank and it is read-only, thus the
// application does not need to be able to store user data to non-volatile
// memory (but it does need to store the DALI variables to non-volatile memory).
// Please refer to the DALI documentation for further information regarding the
// data banks.

#define MEM_BANK0                                   0
#define MEM_BANK1                                   1
#define MEM_BANK2                                   2


#define MEM_BANK0_SIZE_DEFAULT                      0x1B

#define MEM_BANK0_LOCATION_NOT_IMPLEMENTED          0x01

#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// This demo application implements 2 more banks apart from the mandatory
// bank 0.
#define MEM_BANK0_LAST_ACCESSIBLE_BANK_DEFAULT      2
#else
#define MEM_BANK0_LAST_ACCESSIBLE_BANK_DEFAULT      0
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


// Locations in the non-volatile memory where each of these values should be
// stored.
#define MEM_BANK0_GTIN_BYTE_0_DEFAULT               1
#define MEM_BANK0_GTIN_BYTE_1_DEFAULT               2
#define MEM_BANK0_GTIN_BYTE_2_DEFAULT               3
#define MEM_BANK0_GTIN_BYTE_3_DEFAULT               4
#define MEM_BANK0_GTIN_BYTE_4_DEFAULT               5
#define MEM_BANK0_GTIN_BYTE_5_DEFAULT               6
#define MEM_BANK0_CG_FW_VERSION_MAJOR_DEFAULT       1
#define MEM_BANK0_CG_FW_VERSION_MINOR_DEFAULT       1
// Modified according to IEC 62386-102 (ed2.0) Table 9
#define MEM_BANK0_ID_BYTE_0_DEFAULT                 0
#define MEM_BANK0_ID_BYTE_1_DEFAULT                 1
#define MEM_BANK0_ID_BYTE_2_DEFAULT                 2
#define MEM_BANK0_ID_BYTE_3_DEFAULT                 3
#define MEM_BANK0_ID_BYTE_4_DEFAULT                 4
#define MEM_BANK0_ID_BYTE_5_DEFAULT                 5
#define MEM_BANK0_ID_BYTE_6_DEFAULT                 6
#define MEM_BANK0_ID_BYTE_7_DEFAULT                 7
#define MEM_BANK0_HW_VERSION_MAJOR_DEFAULT          1
#define MEM_BANK0_HW_VERSION_MINOR_DEFAULT          1
#define MEM_BANK0_101_VERSION_NUM_DEFAULT           8 // 00001000 (Major version number bits 7 to 2 and minor version number bits 0 to 1)
#define MEM_BANK0_CG_102_VERSION_NUM_DEFAULT        8 // 00001000 (Major version number bits 7 to 2 and minor version number bits 0 to 1)
#define MEM_BANK0_CD_103_VERSION_NUM_DEFAULT        8 // 00001000 (Major version number bits 7 to 2 and minor version number bits 0 to 1)
#define MEM_BANK0_NUM_CD_UNITS_DEFAULT              0
#define MEM_BANK0_NUM_CG_UNITS_DEFAULT              1
#define MEM_BANK0_CG_UNIT_IDX_NUM_DEFAULT           0

#define MEM_BANK0_LAST_ACC_LOCATION_ADDR            0x00
#define MEM_BANK0_LOCATION_NOT_IMPLEMENTED_ADDR     0x01
#define MEM_BANK0_LAST_ACCESSIBLE_BANK_ADDR         0x02
#define MEM_BANK0_GTIN_BYTE_0_ADDR                  0x03
#define MEM_BANK0_GTIN_BYTE_1_ADDR                  0x04
#define MEM_BANK0_GTIN_BYTE_2_ADDR                  0x05
#define MEM_BANK0_GTIN_BYTE_3_ADDR                  0x06
#define MEM_BANK0_GTIN_BYTE_4_ADDR                  0x07
#define MEM_BANK0_GTIN_BYTE_5_ADDR                  0x08
#define MEM_BANK0_CG_FW_VERSION_MAJOR_ADDR          0x09
#define MEM_BANK0_CG_FW_VERSION_MINOR_ADDR          0x0A
// Modified according to IEC 62386-102 (ed2.0) Table 9
#define MEM_BANK0_ID_BYTE_0_ADDR                    0X0B            
#define MEM_BANK0_ID_BYTE_1_ADDR                    0X0C
#define MEM_BANK0_ID_BYTE_2_ADDR                    0X0D
#define MEM_BANK0_ID_BYTE_3_ADDR                    0X0E
#define MEM_BANK0_ID_BYTE_4_ADDR                    0X0F
#define MEM_BANK0_ID_BYTE_5_ADDR                    0X10
#define MEM_BANK0_ID_BYTE_6_ADDR                    0X11
#define MEM_BANK0_ID_BYTE_7_ADDR                    0X12
#define MEM_BANK0_HW_VERSION_MAJOR_ADDR             0X13
#define MEM_BANK0_HW_VERSION_MINOR_ADDR             0X14
#define MEM_BANK0_101_VERSION_NUM_ADDR              0X15 
#define MEM_BANK0_CG_102_VERSION_NUM_ADDR           0X16 
#define MEM_BANK0_CD_103_VERSION_NUM_ADDR           0X17
#define MEM_BANK0_NUM_CD_UNITS_ADDR                 0X18
#define MEM_BANK0_NUM_CG_UNITS_ADDR                 0X19
#define MEM_BANK0_CG_UNIT_IDX_NUM_ADDR              0X1A


#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// DALI bank 1 values. Bank 1 is optional. However, if it is implemented, it
// needs to have a fixed structure with at least 16 locations. Most of these
// locations are writable and some are lockable.
// While the size is not supposed to change, it is also written to EEPROM in
// this example for ease of access -- however, this is not a requirement.
#define MEM_BANK1_SIZE_DEFAULT_AND_RESET            0x11
#define MEM_BANK1_NUM_OF_NVM_VARIABLES              0X0E
#define MEM_BANK1_LOCK_BYTE_UNLOCK_VALUE            0x55

#define MEM_BANK1_LAST_ADDR_DEFAULT_AND_RESET       (MEM_BANK1_SIZE_DEFAULT_AND_RESET - 1)
#define MEM_BANK1_INDICATOR_BYTE_DEFAULT            1
#define MEM_BANK1_LOCK_BYTE_DEFAULT_AND_RESET       0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_0_DEFAULT           0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_1_DEFAULT           0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_2_DEFAULT           0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_3_DEFAULT           0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_4_DEFAULT           0xFF
#define MEM_BANK1_OEM_GTIN_BYTE_5_DEFAULT           0xFF
#define MEM_BANK1_OEM_ID_BYTE_0_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_1_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_2_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_3_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_4_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_5_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_6_DEFAULT             0xFF
#define MEM_BANK1_OEM_ID_BYTE_7_DEFAULT             0xFF


#define MEM_BANK1_SIZE_ADDR                         0x00
#define MEM_BANK1_INDICATOR_BYTE_ADDR               0x01
#define MEM_BANK1_LOCK_BYTE_ADDR                    0x02
#define MEM_BANK1_OEM_GTIN_BYTE_0_ADDR              0x03
#define MEM_BANK1_OEM_GTIN_BYTE_1_ADDR              0x04
#define MEM_BANK1_OEM_GTIN_BYTE_2_ADDR              0x05
#define MEM_BANK1_OEM_GTIN_BYTE_3_ADDR              0x06
#define MEM_BANK1_OEM_GTIN_BYTE_4_ADDR              0x07
#define MEM_BANK1_OEM_GTIN_BYTE_5_ADDR              0x08
#define MEM_BANK1_OEM_ID_BYTE_0_ADDR                0x09
#define MEM_BANK1_OEM_ID_BYTE_1_ADDR                0x0A
#define MEM_BANK1_OEM_ID_BYTE_2_ADDR                0x0B
#define MEM_BANK1_OEM_ID_BYTE_3_ADDR                0x0C
#define MEM_BANK1_OEM_ID_BYTE_4_ADDR                0x0D
#define MEM_BANK1_OEM_ID_BYTE_5_ADDR                0x0E
#define MEM_BANK1_OEM_ID_BYTE_6_ADDR                0x0F
#define MEM_BANK1_OEM_ID_BYTE_7_ADDR                0x10


// DALI bank 2. The structure for banks >= 2 is not as strict as those for the
// first two.
#define MEM_BANK2_SIZE_DEFAULT_AND_RESET            0x04
#define MEM_BANK2_LOCK_BYTE_UNLOCK_VALUE            0x55
#define MEM_BANK2_NUM_OF_ROM_VARIABLES              0x02

#define MEM_BANK2_INDICATOR_BYTE_DEFAULT_AND_RESET  0x00
#define MEM_BANK2_LOCK_BYTE_DEFAULT_AND_RESET       0xFF
#define MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1       0x00

#define MEM_BANK2_SIZE_ADDR                         0x00
#define MEM_BANK2_INDICATOR_BYTE_ADDR               0x01
#define MEM_BANK2_LOCK_BYTE_ADDR                    0x02
#define MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1_ADDR  0x03

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


#ifdef DALI_USE_DEVICE_TYPE_6
///** \brief Read a byte from non-volatile memory for reference system power.
// *
// * The reference system power system needs to be able to read/write to
// * non-volatile memor.
// *
// * @param location Location of the byte, in the range [0..REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED-1].
// * @return Value read from non-volatile memory.
// */
//uint8_t nvmem_refSysPowerReadByte(uint8_t location);
//
//
///** \brief Write a byte to non-volatile memory for reference system power.
// *
// * The reference system power system needs to be able to read/write to
// * non-volatile memory.
// *
// * @param location Location of the byte, in the range [0..REFERENCE_SYSTEM_POWER_NVMEMORY_REQUIRED-1].
// * @param value Value that needs to be written to non-volatile memory.
// */
//void nvmem_refSysPowerWriteByte(uint8_t location, uint8_t value);

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Read a byte from non-volatile memory for the DALI system.
 * 
 * The DALI system needs to store some of its variables to non-volatile memory
 * in order to function according to specifications. Since the example
 * application has access to on-chip EEPROM this is used to implement the
 * non-volatile memory.
 * 
 * @param location Location of the byte, in the range [0..DALI_MACHINE_NVMEMORY_REQUIRED-1].
 * @return Value read from non-volatile memory.
 */
uint8_t nvmem_daliReadByte(uint8_t location);


/** \brief Write a byte to non-volatile memory for the DALI system.
 *
 * The DALI system needs to store some of its variables to non-volatile memory
 * in order to function according to specifications. Since the example
 * application has access to on-chip EEPROM this is used to implement the
 * non-volatile memory.
 * 
 * @param location Location of the byte, in the range [0..DALI_MACHINE_NVMEMORY_REQUIRED-1].
 * @param value Value that needs to be written to non-volatile memory.
 */
void nvmem_daliWriteByte(uint8_t location, uint8_t value);


/** \brief Retrieve the last value written using nvmem_daliWriteLastLevel().
 *
 * In this implementation which uses a circular buffer, this should be the only
 * value in the buffer that differs from 0xFF.
 *
 * @return Last value written using nvmem_daliWriteLastLevel().
 */
uint8_t nvmem_daliReadLastLevel();


/** \brief Write this value to non-volatile memory to be later retrieved using
 * nvmem_daliReadLastLevel().
 *
 * The value written represents the current arc power level and needs to be
 * stored under certain conditions (if POWER ON LEVEL is set to 255). As such,
 * this may be called frequently, thus wearing out the non-volatile memory. To
 * accomodate this a special interface is provided using this pair of functions
 * such that the application developer can implement this in the most efficient
 * way, such as using wear-leveling on multiple locations or only updating the
 * actual level stored to the non-volatile memory after a time-out. \n
 * This implementation uses a circular buffer to store the values, thus
 * distributing EEPROM cell usage over a larger number of cells. We know that
 * the data written to the buffer needs to be a dimming level, and as such this
 * will take values in [0..254]. We can then use the value 255 to mark unused
 * positions in the buffer. Since 255 is an unprogrammed EEPROM cell, this also
 * means we don't do more than one EEPROM programming per write.
 * The function looks for the first programmed value and erases it, moves to the
 * next position (wrapping around at the end of the buffer) and writes the new
 * value there.
 *
 * @param level Value that needs to be written to non-volatile memory.
 */
void nvmem_daliWriteLastLevel(uint8_t level);


/** \brief Read a byte from bank memory.
 *
 * This application example uses 3 memory banks and this function implements the
 * read accesses for these. Given that one bank is in flash and the other two in
 * EEPROM and all have different lenghths, there is an explicit block of code to
 * handle each of the banks.
 * The return type needs to be 16-bit where the upper 8 bits, if different from
 * 0, sigal an error.
 *
 * @param bank Bank from which to read.
 * @param location Memory location in the bank.
 * @return
 *  - If the 8 upper bits are 0, then the return value is the byte read from
 *    the bank memory. \n
 *  - Otherwise, an error is signalled using the value BANK_MEMORY_READ_ERROR
 *    which has the upper 8 bits different from 0. This value represents that
 *    an access past the last location of an implemented bank or of an
 *    unimplemented bank has been attempted.
 */
uint16_t nvmem_daliBankMemoryRead(uint8_t bank, uint8_t location);


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

/** \brief Reset the values stored in RAM and NVM of Memory Bank 1.
 *
 * The Memory Bank 1 locations having memory type RAM and NVM are set to their RESET values.
 *
 * @return Status of Memory Bank 1 RESET operation.
 */
uint8_t resetMemoryBank1();


/** \brief Reset the values stored in RAM and NVM of Memory Bank 2.
 *
 * The Memory Bank 2 locations having memory type RAM and NVM are set to their RESET values.
 *
 * @return Status of Memory Bank 2 RESET operation.
 */
uint8_t resetMemoryBank2();


/** \brief Write a byte to bank memory.
 *
 * This function may should allow any value to be written to EEPROM banks, the
 * DALI system makes sure this memory is used appropriately (memory locks are
 * implemented).
 *
 * @param bank Bank from which to read.
 * @param location Memory location in the bank.
 * @param value Byte that needs to be stored in the memory bank.
 * @return
 *  - BANK_MEMORY_WRITE_OK_NOT_LAST_LOC -- write ok, not last accessible memory
 *    address in bank. \n
 *  - BANK_MEMORY_WRITE_OK_LAST_LOC -- write ok, last accessible memory address
 *    in bank. \n
 *  - BANK_MEMORY_WRITE_CANNOT_WRITE -- cannot write.
 */
uint8_t nvmem_daliBankMemoryWrite(uint8_t bank, uint8_t location, uint8_t value);

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */



#endif /* DALI_EEPROM_H */
