/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           lamp_hardware.h
 *
 * About:
 *  Lamp hardware definitions.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013    Implement isolated lamp hardware
 *                                       functionality.
 ******************************************************************************/

#ifndef LAMP_HARDWARE_H
#define	LAMP_HARDWARE_H


#include <stdint.h>
#include "dali_cg.h"


/** \brief Initialise lamp hardware.
 * 
 * Sets up the PWM output and the button inputs. The buttons are used to
 * simulate various failure conditions.
 */
void lamp_init();


/** \brief Control the lamp power.
 *
 * This function takes care of applying (if required) the logarithmic dimming
 * curve.
 *
 * @param value Lamp brightness, as delivered by the DALI machine [0..254]
 * @param nonLogDimming
 *  - 0: use logarithmic dimming \n
 *  - 1: use linear dimming
 */
void lamp_setPower(uint8_t value, uint8_t nonLogDimming);


/** \brief Obtain lamp status.
 *
 * This function should report any failure of the lamp. Note that the DALI state
 * machine does NOT switch off the lamp in response to these error conditions,
 * although it does report the lamp to be off where required by the standard. It
 * is the application's responsibility to keep the lamp operating safely.
 *
 * @return
 *  - LAMP_STATUS_OK \n
 *  - LAMP_STATUS_OPEN_CIRCUIT \n
 *  - LAMP_STATUS_SHORT_CIRCUIT \n
 *  - LAMP_STATUS_LOAD_INCREASE \n
 *  - LAMP_STATUS_LOAD_DECREASE \n
 *  - LAMP_STATUS_THERMAL_OVEROAD \n
 *  - LAMP_STATUS_THERMAL_SHUTDOWN
 */
uint8_t lamp_getStatus();


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Obtain lamp operating mode.
 *
 * The standard specifies multiple operating modes but no criteria for selecting
 * any of these modes. It is thus the responsibility of the application to use
 * whichever operating mode it needs.
 *
 * @return
 *  - OPERATING_MODE_PWM_MODE_ACTIVE \n
 *  - OPERATING_MODE_AM_MODE_ACTIVE \n
 *  - OPERATING_MODE_CURRENT_CONTROLLED \n
 *  - OPERATING_MODE_HIGH_CURRENT_PULSE
 */
uint8_t lamp_getOperatingMode();


/** \brief Retrieve the actual value that should be sent to the lamp, from 0 to
 * 254, for intensity "index". This is needed to realign the physical minimum
 * level when using the linear dimming scheme.
 *
 * When using the logarithmic dimming curve, this function maps a DALI lamp
 * brightness [0..254] to the actual power level delivered to the lamp within
 * the same [0..254] interval. The DALI machine only uses this information to
 * align the physical minimum level of the lamp when switching to a linear
 * dimming curve.
 *
 * @param index Lamp power value in the DALI [0..254] interval.
 * @return Actual level sent to the lamp for this power.
 */
uint8_t lamp_getDimmingTableValue(uint8_t index);
#endif /* DALI_USE_DEVICE_TYPE_6 */


#endif	/* LAMP_HARDWARE_H */

