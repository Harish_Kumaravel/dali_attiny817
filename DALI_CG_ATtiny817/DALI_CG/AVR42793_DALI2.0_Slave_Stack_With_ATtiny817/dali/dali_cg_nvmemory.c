/**
 * \file
 *
 * \brief Process dali slave emulator EEPROM
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#include "dali_top.h" 
#include "dali_cg_nvmemory.h"
#include "dali_cg_config.h"

#include "dali_cg_machine.h"


// The  nvmem_daliBankMemoryReset() function should return on its
// completion status using the following values:
#define BANK_MEMORY_RESET                               0x00
#define BANK_MEMORY_NOT_RESET                           0x01
    

/** \brief Actual definition of the bank 0 memory space.
 *
 * This array contains memory bank 0 and is implemented in program space to
 * save EEPROM.
 */
PROGMEM uint8_t bank0mem[MEM_BANK0_SIZE_DEFAULT] = 
{
      MEM_BANK0_SIZE_DEFAULT - 1,
      MEM_BANK0_LOCATION_NOT_IMPLEMENTED,
      MEM_BANK0_LAST_ACCESSIBLE_BANK_DEFAULT,
      MEM_BANK0_GTIN_BYTE_0_DEFAULT,
      MEM_BANK0_GTIN_BYTE_1_DEFAULT,
      MEM_BANK0_GTIN_BYTE_2_DEFAULT,
      MEM_BANK0_GTIN_BYTE_3_DEFAULT,
      MEM_BANK0_GTIN_BYTE_4_DEFAULT,
      MEM_BANK0_GTIN_BYTE_5_DEFAULT,
      MEM_BANK0_CG_FW_VERSION_MAJOR_DEFAULT,
      MEM_BANK0_CG_FW_VERSION_MINOR_DEFAULT,
      MEM_BANK0_ID_BYTE_0_DEFAULT,
      MEM_BANK0_ID_BYTE_1_DEFAULT,
      MEM_BANK0_ID_BYTE_2_DEFAULT,
      MEM_BANK0_ID_BYTE_3_DEFAULT,
      MEM_BANK0_ID_BYTE_4_DEFAULT,
      MEM_BANK0_ID_BYTE_5_DEFAULT,
      MEM_BANK0_ID_BYTE_6_DEFAULT,
      MEM_BANK0_ID_BYTE_7_DEFAULT,
      MEM_BANK0_HW_VERSION_MAJOR_DEFAULT,
      MEM_BANK0_HW_VERSION_MINOR_DEFAULT, 
      MEM_BANK0_101_VERSION_NUM_DEFAULT,
      MEM_BANK0_CG_102_VERSION_NUM_DEFAULT,
      MEM_BANK0_CD_103_VERSION_NUM_DEFAULT,
      MEM_BANK0_NUM_CD_UNITS_DEFAULT,
      MEM_BANK0_NUM_CG_UNITS_DEFAULT,
      MEM_BANK0_CG_UNIT_IDX_NUM_DEFAULT
};


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

/** \brief Memory bank 1 locations to be stored in ROM. */
PROGMEM uint8_t memBank1LastAddr_DefautAndReset = MEM_BANK1_LAST_ADDR_DEFAULT_AND_RESET;
PROGMEM uint8_t memBank1IndicatorByte_DefautAndReset = MEM_BANK1_INDICATOR_BYTE_DEFAULT;

/** \brief Memory bank 1 lock byte to be stored in RAM. */
static uint8_t memBank1LockByteVal_DefautAndReset = MEM_BANK1_LOCK_BYTE_DEFAULT_AND_RESET;
/** \brief Definition of memory bank 1 for variables stored in EEPROM.
 *
 * This array contains memory bank 1 variables implemented in EEPROM.
 */
__eeprom uint8_t EEbank1mem[MEM_BANK1_NUM_OF_NVM_VARIABLES] = 
{
      MEM_BANK1_OEM_GTIN_BYTE_0_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_1_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_2_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_3_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_4_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_5_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_0_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_1_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_2_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_3_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_4_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_5_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_6_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_7_DEFAULT
};

/** \brief Definition of memory bank 1 reset values.
 *
 * This array contains memory bank 1 reset values and is implemented in ROM.
 */
PROGMEM uint8_t bank1EEmemResetTable[MEM_BANK1_NUM_OF_NVM_VARIABLES] = 
{
      MEM_BANK1_OEM_GTIN_BYTE_0_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_1_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_2_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_3_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_4_DEFAULT,
      MEM_BANK1_OEM_GTIN_BYTE_5_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_0_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_1_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_2_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_3_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_4_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_5_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_6_DEFAULT,
      MEM_BANK1_OEM_ID_BYTE_7_DEFAULT
};


/** \brief Memory bank 2 locations to be stored in RAM. */
static uint8_t memBank2LockByteVal_DefautAndReset = MEM_BANK2_LOCK_BYTE_DEFAULT_AND_RESET;
static uint8_t memBank2ManufacturerSpecificByte1 = MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1;

/** \brief Definition of memory bank 2 for ROM variables.
 *
 * This array contains memory bank 2 locations implemented in ROM.
 */
PROGMEM uint8_t bank2mem[MEM_BANK2_NUM_OF_ROM_VARIABLES] = 
{
      MEM_BANK2_SIZE_DEFAULT_AND_RESET - 1,
      MEM_BANK2_INDICATOR_BYTE_DEFAULT_AND_RESET
};
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */


/** \brief DALI state machine non-volatile memory.
 *
 * The DALI state machine needs some non-volatile memory for it to function
 * according to specifications. It uses nvmem_daliReadByte() and
 * nvmem_daliWriteByte() to read from and write to this memory. The
 * daliMachineEeprom variable implements the memory locations needed for this.
 * DALI_MACHINE_NVMEMORY_REQUIRED represents the number of bytes required by the
 * machine and is defined in dali_cg_machine.h
 */
__eeprom uint8_t daliMachineEeprom[DALI_MACHINE_NVMEMORY_REQUIRED] =
{
      NVMEMORY_DEFAULT_POWER_ON_LEVEL,
      NVMEMORY_DEFAULT_SYSTEM_FAILURE_LEVEL,
      NVMEMORY_DEFAULT_MINIMUM_LEVEL,
      NVMEMORY_DEFAULT_MAXIMUM_LEVEL,
      NVMEMORY_DEFAULT_FADE_RATE,
      NVMEMORY_DEFAULT_FADE_TIME,
      NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_BASE,
      NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_MULTIPLIER,
      NVMEMORY_DEFAULT_OPERATING_MODE,
      NVMEMORY_DEFAULT_SHORT_ADDRESS,
      NVMEMORY_DEFAULT_RANDOM_ADDRESS_H,
      NVMEMORY_DEFAULT_RANDOM_ADDRESS_M,
      NVMEMORY_DEFAULT_RANDOM_ADDRESS_L,
      NVMEMORY_DEFAULT_GROUP_LOW,
      NVMEMORY_DEFAULT_GROUP_HIGH,
      NVMEMORY_DEFAULT_SCENES,              // 16 locations

#ifdef DALI_USE_DEVICE_TYPE_6
      NVMEMORY_DEFAULT_FAST_FADE_TIME,
      NVMEMORY_DEFAULT_DIMMING_CURVE,
      NVMEMORY_DEFAULT_MULTIPLE_BITS
#endif /* DALI_USE_DEVICE_TYPE_6 */
};



// The DALI state machine may need to store the received power level to
// non-volatile memory. As this can be done quite frequently, the DALI state
// machine provides a separate interface, just for this purpose, using the
// functions nvmem_daliReadLastLevel() and nvmem_daliWriteLastLevel(). This
// application example implements a circular buffer 16 bytes long to use wear
// leveling on the EEPROM locations that store these values.
// This value is the circular buffer length.
#define EEPROM_CIRCULAR_BUFFER_SIZE                 0x10


/** \brief Circular buffer that stores the arc power levels in EEPROM.
 *
 */
__eeprom uint8_t EElastArcValues[EEPROM_CIRCULAR_BUFFER_SIZE];


//#ifdef DALI_USE_DEVICE_TYPE_6
//
///** \brief Memory storage for reference system power.
// *
// * The reference system power mechanism may need to store values in non-volatile
// * memory. This array can be accessed using the nvmem_refSysPowerReadByte()
// * and nvmem_refSysPowerWriteByte() functions.
// */
//__eeprom uint8_t EEreferenceSystemPower[1]; /* Replace with a macro */
//
//#endif /* DALI_USE_DEVICE_TYPE_6 */



//#ifdef DALI_USE_DEVICE_TYPE_6
//
//uint8_t nvmem_refSysPowerReadByte(uint8_t location)
//{
//    return EEreferenceSystemPower[location];
//}
//
//
//void nvmem_refSysPowerWriteByte(uint8_t location, uint8_t value)
//{
//    EEreferenceSystemPower[location] = value;
//}
//
//#endif /* DALI_USE_DEVICE_TYPE_6 */


uint8_t nvmem_daliReadByte(uint8_t location)
{
      return daliMachineEeprom[location];
}


void nvmem_daliWriteByte(uint8_t location, uint8_t value)
{
      daliMachineEeprom[location] = value;
}


uint8_t nvmem_daliReadLastLevel()
{
      uint8_t i, readVal;
    
      for (i = 0; i < EEPROM_CIRCULAR_BUFFER_SIZE; i++)
      {
          readVal = EElastArcValues[i];
          if (readVal != 0xFF)
          {
              break;
          }
      }
      
      return readVal;
}


void nvmem_daliWriteLastLevel(uint8_t level)
{
      uint8_t i, lastVal;
      
      // Use wear leveling for this task, as it may be updated often
      for (i = 0; i < EEPROM_CIRCULAR_BUFFER_SIZE; i++)
      {
          lastVal = EElastArcValues[i];
          if (lastVal != 0xFF)
          {
              if (lastVal != level)
              {
                  // Erase current position
                  EElastArcValues[i] = 0xFF;
                  // Move to next position in circular buffer
                  i++;
                  if (i >= EEPROM_CIRCULAR_BUFFER_SIZE)
                  {
                      i = 0;
                  }
                  // Write current arc value to this position
                  EElastArcValues[i] = level;
              }
              break;
          }
      }
      // This final case deals with the possibility that the buffer is empty, as
      // it should leave the factory. In that case, just use the first memory
      // location
      if (lastVal == 0xFF)
      {
          EElastArcValues[0] = level;
      }
}


uint16_t nvmem_daliBankMemoryRead(uint8_t bank, uint8_t location)
{
      if (bank == 0)
      {
          if ((location < MEM_BANK0_SIZE_DEFAULT) && (location != MEM_BANK0_LOCATION_NOT_IMPLEMENTED_ADDR))
          {
              return bank0mem[location];
          }
          else
          {
              return BANK_MEMORY_READ_ERROR;
          }
      }
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
      else if (bank == 1)
      {
          if(location < MEM_BANK1_SIZE_DEFAULT_AND_RESET)
          {
              if(location == MEM_BANK1_SIZE_ADDR)
              {
                  return memBank1LastAddr_DefautAndReset;
              }
              else if(location == MEM_BANK1_INDICATOR_BYTE_ADDR)
              {
                  return memBank1IndicatorByte_DefautAndReset;
              }
              else if(location == MEM_BANK1_LOCK_BYTE_ADDR)
              {
                  return memBank1LockByteVal_DefautAndReset;
              }
              else
              {
                  return EEbank1mem[location-(MEM_BANK1_SIZE_DEFAULT_AND_RESET - MEM_BANK1_NUM_OF_NVM_VARIABLES)];
              }
          }
          else
          {
              return BANK_MEMORY_READ_ERROR;
          }
      }
      else if (bank == 2)
      {
          if (location < MEM_BANK2_SIZE_DEFAULT_AND_RESET)
          {
              if(location < MEM_BANK2_NUM_OF_ROM_VARIABLES)
              {
                  return bank2mem[location];
              }
              else if(location == MEM_BANK2_LOCK_BYTE_ADDR)
              {
                  return memBank2LockByteVal_DefautAndReset;
              }
              else if(location == MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1_ADDR)
              {
                  return memBank2ManufacturerSpecificByte1;
              }
          }
          else
          {
              return BANK_MEMORY_READ_ERROR;
          }
      }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
    // In case we're trying to access memory from a non-existent bank
    // we need to tell this to the DALI State Machine.
      return BANK_MEMORY_READ_ERROR;
}


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

uint8_t resetMemoryBank1()
{
      uint8_t i;
      
      if(memBank1LockByteVal_DefautAndReset == MEM_BANK1_LOCK_BYTE_UNLOCK_VALUE)
      {
              for(i = 0; i < MEM_BANK1_NUM_OF_NVM_VARIABLES; i++)
              {
                  EEbank1mem[i] = bank1EEmemResetTable[i];
              }
              memBank1LockByteVal_DefautAndReset = MEM_BANK1_LOCK_BYTE_DEFAULT_AND_RESET;
              return BANK_MEMORY_RESET;
      }
      else
      {
          return BANK_MEMORY_NOT_RESET;
      }
}


uint8_t resetMemoryBank2()
{
      if(memBank2LockByteVal_DefautAndReset == MEM_BANK2_LOCK_BYTE_UNLOCK_VALUE)
      {
              memBank2LockByteVal_DefautAndReset = MEM_BANK2_LOCK_BYTE_DEFAULT_AND_RESET;
              memBank2ManufacturerSpecificByte1 = MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1;
              return BANK_MEMORY_RESET;
      }
      else
      {
          return BANK_MEMORY_NOT_RESET;
      
}


uint8_t nvmem_daliBankMemoryWrite(uint8_t bank, uint8_t location, uint8_t value)
{
      if (bank == 0)
      {
          return BANK_MEMORY_WRITE_CANNOT_WRITE;
      }
      else if (bank == 1)
      {
          if(location < MEM_BANK1_SIZE_DEFAULT_AND_RESET)
          {
              if(location == MEM_BANK1_LOCK_BYTE_ADDR)
              {
                  memBank1LockByteVal_DefautAndReset = value;
                  return BANK_MEMORY_WRITE_OK_NOT_LAST_LOC;
              }
              else if((location >= (MEM_BANK1_SIZE_DEFAULT_AND_RESET - MEM_BANK1_NUM_OF_NVM_VARIABLES)) && (memBank1LockByteVal_DefautAndReset == MEM_BANK1_LOCK_BYTE_UNLOCK_VALUE))
              {
                  EEbank1mem[(location - (MEM_BANK1_SIZE_DEFAULT_AND_RESET - MEM_BANK1_NUM_OF_NVM_VARIABLES))] = value;
                  if (location == (MEM_BANK1_SIZE_DEFAULT_AND_RESET - 1))
                  {
                      return BANK_MEMORY_WRITE_OK_LAST_LOC;
                  }
              }
              return BANK_MEMORY_WRITE_CANNOT_WRITE;
          }
      }
      else if (bank == 2)
      {
          if (location < MEM_BANK2_SIZE_DEFAULT_AND_RESET)
          {
              // Write only to the locations which are writable
              if(location == MEM_BANK2_LOCK_BYTE_ADDR)
              {
                  memBank2LockByteVal_DefautAndReset = value;
              }
              else if(location == MEM_BANK2_MANUFACTURER_SPECIFIC_BYTE1_ADDR)
              {
                  memBank2ManufacturerSpecificByte1 = value;
              }
              if (location == (MEM_BANK2_SIZE_DEFAULT_AND_RESET - 1))
              {
                  return BANK_MEMORY_WRITE_OK_LAST_LOC;
              }
              return BANK_MEMORY_WRITE_OK_NOT_LAST_LOC;
          }
          else
          {
              return BANK_MEMORY_WRITE_CANNOT_WRITE;
          }
      }
      // If we haven't returned by now, bank unimplemented
      return BANK_MEMORY_WRITE_CANNOT_WRITE;
}

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */



