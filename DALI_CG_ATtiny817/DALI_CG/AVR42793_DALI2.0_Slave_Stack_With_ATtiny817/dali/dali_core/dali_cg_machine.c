/**
 * \file
 *
 * \brief Process commands dali slave received
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#include "dali_top.h"
#include "dali_cg_machine.h"

#include "dali_cg.h"
#include "dali_cg_layer.h"
#include "dali_cg_protocol.h"
#include "dali_cg_nvmemory.h"

#include "random.h"


// First commit by Harish

/**********************DALI machine definitions********************************/

#ifdef DALI_USE_DEVICE_TYPE_6
// Device type, according to DALI standard. This file implements the LED command
// set of the DALI system as well as the basic commands. This makes it comply
// with DALI Device Type 6.
#define DEVICE_TYPE                                 0x06
#else
// If the library doesn't support the LED command set, we still need to respond
// to the device type query. Value 255 tells the control device that multiple
// device types are supported.
#define DEVICE_TYPE                                 255
#endif /* DALI_USE_DEVICE_TYPE_6 */


#ifdef DALI_USE_DEVICE_TYPE_6
// Light source type, according to DALI standard. This file implements the LED 
// command set of the DALI system as well as the basic commands. This makes it 
// comply with DALI Device Type 6.
#define LIGHT_SOURCE_TYPE                           0x06
#else
// If the library doesn't support the LED command set, we still need to respond
// to the light source type query. Value 255 tells the control device that 
// multiple light source types are supported.
#define LIGHT_SOURCE_TYPE                           255
#endif /* DALI_USE_DEVICE_TYPE_6 */


// The lamp can either be commanded to go to a new power level directly, or
// using either fade time or fade rate. These are being given as parameters to
// the idali_setArcPower() function such that it can properly configure the lamp
// power and the fading mechanism.
#define FADE_NONE                                   0x00
#define FADE_FADE_TIME                              0x01
#define FADE_FADE_RATE                              0x02


// The DALI state machine uses a set of flags to store its state. They are
// defined here.
typedef union
{
      uint16_t All;
      struct
      {
          unsigned fadeUp                             : 1;    // Flag telling the fader that it should fade up
          unsigned withdrawFromCompare                : 1;    // During initialisation, we may be told to withdraw from the comparison process
          unsigned initialisationStateEnabled         : 1;    // Initialisation state is a temporary state which is entered with the command. Power cycle or TERMINATE causes Control Gear to leave initialisationState immediately.
          unsigned initialisationStateDisabled        : 1;    // Initialisation state is a temporary state which is entered with the command. Power cycle or TERMINATE causes Control Gear to leave initialisationState immediately.
          unsigned initialisationStateWithdrawn       : 1;    // Initialisation state is a temporary state which is entered with the command. Power cycle or TERMINATE causes Control Gear to leave initialisationState immediately.
          unsigned receivedPacket                     : 1;    // A packet has been received and awaits processing
          unsigned writelastLightLevelToNVMAtPowerUp  : 1;    // This variable is indicates whether after initial powerup the actualArcPower needs to be saved in NVM
          unsigned minLevelChanged                    : 1;    // This variable is indicates whether the limitError bit should be set as a result of change in min level
          unsigned maxLevelChanged                    : 1;    // This variable is indicates whether the limitError bit should be set as a result of change in max level
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
          unsigned writeEnabled                       : 1;    // The memory is write enabled
          unsigned writeEnabledOld                    : 1;    // Previous state of write enable state, used to form a latch
#else
          unsigned                                    : 2;    // Unused bits if bank memory writing is not implemented
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
#ifdef DALI_USE_DEVICE_TYPE_6
          unsigned enabledDeviceType                  : 1;    // Device Type 6 specific commands need to be enabled first. This flag indicates that such enabling has taken place
          unsigned enabledDeviceTypeOld               : 1;    // Previous state of device type enable, used to form a latch
          unsigned                                    : 3;
#else
          unsigned                                    : 5;    // Unused bits if Device Type 6 is not enabled
#endif /* DALI_USE_DEVICE_TYPE_6 */
    };
}tdali_flags_state_machine;


#ifdef DALI_USE_DEVICE_TYPE_6
//// LED specific machine flags
//typedef union
//{
//    uint8_t All;
//    struct
//    {
//        unsigned currentProtectorEnabled            : 1;    // The current protector state
//        unsigned                                    : 7;    // Unused bits
//    };
//}tdali_flags_state_machine_led;
#endif /* DALI_USE_DEVICE_TYPE_6 */


// When processing a command, various addressing modes are being stored as flags
// in a variable that uses this structure.
typedef union
{
      uint8_t All;
      struct
      {
          unsigned deviceAddressed                    : 1;    // The device is being addressed
          unsigned publicAddress                      : 1;    // Device is addressed using a "public" address
          unsigned individualAddress                  : 1;    // Device is addressed using an individual address
          unsigned commandRepeated                    : 1;    // Command has been received a second time within 100ms
          unsigned previousCommand                    : 1;    // Some other command is required to be sent first in order to send the current command
          unsigned                                    : 3;    // Unused locations
      };
}tdali_flags_command;


// This union is modified according to IEC 62386-102 (ed2.0) Table 12 Page 45
typedef union
{
      uint8_t All;
      struct
      {
          unsigned controlGearFailure                 : 1;    // controlGearFailure is TRUE? "1" = "YES"
          unsigned lampFailure                        : 1;    // lampFailure is TRUE? "1" = "YES"
          unsigned lampOn                             : 1;    // lampOn is TRUE? "1" = "YES"
          unsigned limitError                         : 1;    // limitError is TRUE? "1" = "YES"
          unsigned fadeRunning                        : 1;    // fadeRunning is TRUE? "1" = "YES"
          unsigned resetState                         : 1;    // resetState is TRUE? "1" = "YES"
          unsigned shortAddress                       : 1;    // shortAddress is MASK? "1" = "YES"
          unsigned powerCycleSeen                     : 1;    // powerCycleSeen is TRUE? "1" = "YES"
      };
}tdali_flags_status;

#ifdef DALI_USE_DEVICE_TYPE_6


// The LED specifications for the DALI protocol defines these failure status
// flags.
typedef union
{
      uint8_t All;
      struct
      {
          unsigned shortCircuit                       : 1;    // "0" = No
          unsigned openCircuit                        : 1;    // "0" = No
          unsigned loadDecrease                       : 1;    // "0" = No
          unsigned loadIncrease                       : 1;    // "0" = No
          unsigned currentProtectorActive             : 1;    // "0" = No
          unsigned thermalShutDown                    : 1;    // "0" = No
          unsigned thermalOverload                    : 1;    // "0" = No
          unsigned referenceMeasurementFailed         : 1;    // "0" = No
      };
}tdali_flags_failure_status;


// The LED specifications for the DALI protocol defines this structure for the
// operating mode.
typedef union
{
      uint8_t All;
      struct
      {
          unsigned pwm                                : 1;    // "0" = No
          unsigned am                                 : 1;    // "0" = No
          unsigned outputCurrentControlled            : 1;    // "0" = No
          unsigned highCurrentPulseMode               : 1;    // "0" = No
          unsigned nonLogDimmingCurve                 : 1;    // "0" = No
          unsigned                                    : 3;    // Unused locations
      };
}tdali_flags_led_operation_mode;

#endif /* DALI_USE_DEVICE_TYPE_6 */


// The fader uses a 32bit counter with overflow/underflow detection. It also
// needs access to its individual bytes as well. The structure of this counter
// is as follows:
// byte[3] byte[2] byte[1] byte[0]
//   |        |      |       |
//   |        |      |       |
//   |        |      |       \-- Lower 8 bits of the 32bit timer, being
//   |        |      |           interpreted as the fractional part of the power
//   |        |      |           level. It is required since some fades need to
//   |        |      |           run much slower than 1 power level increment
//   |        |      |           every 5ms
//   |        |      |
//   |        |      \----- Middle 8 bits of the 32bit timer. 
//   |        |  
//   |        |  
//   |        |
//   |        |
//   |        |
//   |        |
//   |        \------ Highest 8 bits of the 32 bit timer.
//   |
//   \----------- This is exactly the power level that needs to be sent to the 
//                lamp
typedef union
{
      uint32_t counter;
      struct
      {
          uint8_t byte[4];
      };
}tdali_fade_counter;

/**********************DALI Machine definitions*(END)**************************/



/**********************DALI Machine variables**********************************/

/** \brief Array which aids in the decoding of the groups.
 *
 * Using this method gives better results than using "1<<i" does.
 */
static const uint8_t groupMask[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};


/** \brief Fade time table, as defined by the DALI standard.
 *
 * The fader is run every 5ms, thus these values represent the number of fader
 * steps that make up the given fade time. (0s, 0.7s, 1s,...)
 */
//static const uint16_t fadeTimeTable[16] =
//{
//    0,350,500,700,1000,1400,2000,2850,4000,
//    5650,8000,11300,16000,22650,32000,45250
//};

static const uint16_t fadeTimeTable[16] =
{
      0,140,200,280,400,560,800,1040,1600,
      2260,3200,4520,6400,9060,12800,18100
};


/** \brief Fade rate table, as defined by the DALI standard.
 *
 * The fader uses a 32bit counter of which byte 2 is the lamp power
 * level, and the low 16 bits are used to increase dimming accuracy. As such,
 * these values are used to make byte 2 change at the fade rate
 * requested by the standard. The values from the table in IEC 62386-102 are
 * multiplied by 65536 (shift left 16 bits) and divided by 200 (fader frequency,
 * its period being 5ms).
 */
static const uint32_t fadeRateTable[16] =
{
      165888,30031217,21223178,15015608,10653532,7499416,
      5309989,3749708,2650800,1879048,1325400,939524,662700,469762,335544,234881
};


/** \brief Extended fade time table, as defined by the DALI standard.
 *
 * If fadeTime equals 0, and the fast fade time as defined in IEC 62386 Part 
 * 207 is implemented and equals 0, the extended fade time shall be used.
 * The fader is run every 5ms, thus these values represent the number of fader
 * steps that make up the given fade time. (0s, 100ms, 1s, 10s, ....)
 */
static const uint16_t extendedFadeTimeMulTable[] = {0, 20,  200,  2000,  12000};
//static const uint32_t extendedFadeTimeTable[16][8] =
//{
//    {0, 20,  200,  2000,  12000}, /*  initializers for row indexed by 0 */
//    {0, 40,  400,  4000,  24000}, /*  initializers for row indexed by 1 */
//    {0, 60,  600,  6000,  36000}, /*  initializers for row indexed by 2 */
//    {0, 80,  800,  8000,  48000}, /*  initializers for row indexed by 3 */
//    {0, 100, 1000, 10000, 60000}, /*  initializers for row indexed by 4 */
//    {0, 120, 1200, 12000, 72000}, /*  initializers for row indexed by 5 */
//    {0, 140, 1400, 14000, 84000}, /*  initializers for row indexed by 6 */
//    {0, 160, 1600, 16000, 96000}, /*  initializers for row indexed by 7 */
//    {0, 180, 1800, 18000, 108000}, /*  initializers for row indexed by 8 */
//    {0, 200, 2000, 20000, 120000}, /*  initializers for row indexed by 9 */
//    {0, 220, 2200, 22000, 132000}, /*  initializers for row indexed by 10 */
//    {0, 240, 2400, 24000, 144000}, /*  initializers for row indexed by 11 */
//    {0, 260, 2600, 26000, 156000}, /*  initializers for row indexed by 12 */
//    {0, 280, 2800, 28000, 168000}, /*  initializers for row indexed by 13 */
//    {0, 300, 3000, 30000, 180000}, /*  initializers for row indexed by 14 */
//    {0, 320, 3200, 32000, 192000}  /*  initializers for row indexed by 15 */
//};


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Fast fade time table, as defined by the DALI standard for Device Type
 * 6 devices.
 *
 * The fader is run every 5ms, thus these values represent the number of fader
 * steps that make up the given fast fade time. (0ms, 25ms, 50ms,...)
 */
static const uint8_t fastFadeTimeTable[28] =
{
      0,5,10,15,20,25,30,35,
      40,45,50,55,60,65,70,75,
      80,85,90,95,100,105,110,115,
      120,125,130,135
};


#endif /* DALI_USE_DEVICE_TYPE_6 */


// Non-Volatile Memory data. These values are the locations that the
// NVMemoryDALIReadByte() and NVMemoryDALIWriteByte() will read from/write to.
// Default values are provided in dali_cg_machine.h since
// those are the ones the control gear should leave the factory with.
#define NVMEMORY_POWER_ON_LEVEL                 0
#define NVMEMORY_SYSTEM_FAILURE_LEVEL           1
#define NVMEMORY_MINIMUM_LEVEL                  2
#define NVMEMORY_MAXIMUM_LEVEL                  3
#define NVMEMORY_FADE_RATE                      4
#define NVMEMORY_FADE_TIME                      5
#define NVMEMORY_EXTENDED_FADE_TIME_BASE        6
#define NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER  7
#define NVMEMORY_OPERATING_MODE                 8
#define NVMEMORY_SHORT_ADDRESS                  9
#define NVMEMORY_RANDOM_ADDRESS_H               10
#define NVMEMORY_RANDOM_ADDRESS_M               11
#define NVMEMORY_RANDOM_ADDRESS_L               12
#define NVMEMORY_GROUP_LOW                      13
#define NVMEMORY_GROUP_HIGH                     14
//// The variable lastLightLevel is added according to IEC 62386-102 (ed2.0) Table 14
//#define NVMEMORY_LAST_LIGHT_LEVEL               15
#define NVMEMORY_SCENES_BASE                    15  // 16 values

#ifdef DALI_USE_DEVICE_TYPE_6
// And LED specific ones
#define NVMEMORY_FAST_FADE_TIME                 NVMEMORY_SCENES_BASE + 16
#define NVMEMORY_DIMMING_CURVE                  NVMEMORY_SCENES_BASE + 17
#define NVMEMORY_MULTIPLE_BITS                  NVMEMORY_SCENES_BASE + 18


// The NVMEMORY_MULTIPLE_BITS location contains 3 meaningful bits:
//   bit 7: reference measurement failed
//   bit 4: linear dimming curve selected
//   bit 0: current protector enabled state
// We define masks for each of these
//#define NVMEMORY_MULTIPLE_BITS_REF_FAIL         0x80
#define NVMEMORY_MULTIPLE_BITS_LIN_DIM          0x08
//#define NVMEMORY_MULTIPLE_BITS_CP_EN            0x01

//// Extended fade tiem base and multiplier masks
//#define EXTENDED_FADE_TIME_BASE_MASK            0b00001111
//#define EXTENDED_FADE_TIME_MULTIPLIER_MASK     0b01110000

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Power on level (shadow of Non-Volatile Memory value). */
static uint8_t powerOnLevel;


/** \brief System failure level (shadow of Non-Volatile Memory value). */
static uint8_t systemFailureLevel;


/** \brief Minimum level (shadow of Non-Volatile Memory value). */
static uint8_t minLevel;


/** \brief Maximum level (shadow of Non-Volatile Memory value). */
static uint8_t maxLevel;


/** \brief Fade rate (shadow of Non-Volatile Memory value). */
static uint8_t fadeRate;


/** \brief Fade time (shadow of Non-Volatile Memory value). */
static uint8_t fadeTime;

/** \brief Helper for extended fade time (shadow of Non-Volatile Memory value). */
static uint8_t extendedFadeTimeBase;


/** \brief Helper for extended fade time (shadow of Non-Volatile Memory value). */
static uint8_t extendedFadeTimeMultiplier;


/** \brief Operating mode (shadow of Non-Volatile Memory value). */
static uint8_t opMode;

/** \brief Short address (shadow of Non-Volatile Memory value).
 *
 * In 0AAA AAA1 format.
 */
static uint8_t shortAddress;


/** \brief Random address high byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressH;


/** \brief Random address medium byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressM;


/** \brief Random address low byte (shadow of Non-Volatile Memory value). */
static uint8_t randomAddressL;


/** \brief Group low byte (shadow of Non-Volatile Memory value). */
static uint8_t groupLow;


/** \brief Group high byte (shadow of Non-Volatile Memory value). */
static uint8_t groupHigh;


// This variable is added in IEC 62386-102 (ed2.0) 
/** \brief Last light level (shadow of Non-Volatile Memory value). 
 * Used during power-up, if the device needs to restore the last used power
 * level.
 * */
static uint8_t lastLightLevel;


/** \brief Scene levels (shadow of Non-Volatile Memory value). */
static uint8_t scenes[16];


#ifdef DALI_USE_DEVICE_TYPE_6

/** \brief Fast fade time (shadow of Non-Volatile Memory value). */
static uint8_t fastFadeTime;

#endif /* DALI_USE_DEVICE_TYPE_6 */


// This variable is added in IEC 62386-102 (ed2.0) 
/** \brief Last active level. */
static uint8_t lastActiveLevel;


/** \brief Actual arc power level. */
// This is the actualLevel variable in IEC 62386-102 (ed2.0) Table 14
static volatile uint8_t actualArcPower;

//// This variable is indicates whether after initial powerup the actualArcPower
//// needs to be saved in NVM
///** \brief Save actualArcPower in NVM. */
//static uint8_t writelastLightLevelToNVMAtPowerUp = 1;
//
//// This variable is indicates whether the limitError bit should be set
//// as a result of change in min level
///** \brief Change in min level. */
//static uint8_t minLevelChanged;
//
//// This variable is indicates whether the limitError bit should be set
//// as a result of change in max level
///** \brief Change in max level. */
//static uint8_t maxLevelChanged;

/** \brief Search address high byte. */
static uint8_t searchAddressH;


/** \brief Search address medium byte. */
static uint8_t searchAddressM;


/** \brief Search address low byte. */
static uint8_t searchAddressL;


/** \brief Data transfer register. */
static uint8_t DTR;


/** \brief Data transfer register 1. */
static uint8_t DTR1;


/** \brief Data transfer register 2. */
static uint8_t DTR2;


/** \brief DALI status flags. */
static volatile tdali_flags_status status;


#ifdef DALI_USE_DEVICE_TYPE_6

// LED specific DALI variables

/** \brief Physical minimum level.
 *
 * When implementing a DALI Device Type 6 (LED) device, the physical minimum
 * level needs to be adjusted according to the selected dimming curve (linear or
 * logarithmic).
 */
static uint8_t physicalMinimumLevel;


/** \brief DALI failure status flags. */
static tdali_flags_failure_status failureStatus;


/** \brief DALI operating mode. */
static tdali_flags_led_operation_mode operatingMode;


///** \brief DALI variable which keeps the enable status of the current
// * protector.
// */
//static tdali_flags_state_machine_led daliLedStateFlags;


#else

#define physicalMinimumLevel        PHYSICAL_MINIMUM_LEVEL

#endif /* DALI_USE_DEVICE_TYPE_6 */


/** \brief Command time-out variable.
 *
 * Value measured in milliseconds. Used for commands that need to be received
 * twice within 100ms to be executed.
 */
volatile uint8_t commandTimeout_ms = 0;


/** \brief DAPC sequence time-out variable.
 *
 * Value measured in milliseconds. If no DAPC command is received within 200ms,
 * the sequence needs to be aborted.
 */
static volatile uint8_t dapcTimeout_ms = 0;


/** \brief Power-up time-out variable.
 *
 * Value measured in milliseconds. The device should wait 0.6s after powerup
 * before sending the lamp to the power on level.
 */
static volatile uint8_t powerUpTimeout_100ms = 6;


/** \brief Initialise time-out variable.
 *
 * Value measured in tenths of a second. After receiving an initialise command,
 * this timer will run for 15 minutes or until a terminate command is received.
 */
static volatile uint16_t initialiseTimeout_100ms = 0;


/** \brief Identify Device time-out variable.
 *
 * Value measured in tenths of a second. After receiving an initialise command,
 * this timer will run for 15 minutes or until a terminate command is received.
 */
static volatile uint8_t identifyDeviceTimeout_100ms = 0;


/** \brief Cable disconnection time-out variable.
 *
 * Value measured in milliseconds. If the cable is disconnected for 500ms, the
 * device should react by going to system failure level.
 */
static volatile uint16_t cableDisconnectTimeout_ms = 550;

/** \brief Last command received (high byte).
 * 
 * The command processing mechanism needs to know what the last command was in
 * order to be able to tell if the command has been received twice.
 */
static uint8_t lastAddress;


/** \brief Last command received (low byte).
 * 
 * The command processing mechanism needs to know what the last command was in
 * order to be able to tell if the command has been received twice.
 */
static uint8_t lastCommand;


/** \brief The DALI state machine stores its state in these flags. */
static tdali_flags_state_machine daliStateFlags;


/** \brief Fade counter variable. */
static volatile tdali_fade_counter fadeAccumulator;


/** \brief Value that should be added to/subtracted from the counter every
 * 5ms. */
static uint32_t fadeDelta;


/** \brief Value that should be set to adjust fadeDelta for first and last  
 * fade steps (using FADE_FADE_TIME). */
static volatile uint8_t fadeStart;


/** \brief Number of 5ms steps that the fader should run. */
static volatile uint32_t fadeCount;


/** \brief Target level for the fading mechanism.
 * 
 * If the target level is known (which it is, unless we're using fade rate),
 * store the target level in this variable. The fader may or may not hit this
 * exact level since there is limited precision. At the very last step of the
 * fader, this value is given to the lamp, thus ending up at the exact value
 * the Control Device asked us to go to.
 */
static uint8_t fadeTimeTargetLevel;


/** \brief Data received from DALI bus.
 * 
 * If the DALI protocol level detects a forward frame, it will call
 * idali_receiveForwardFrame() which will set this variable. Being set from the
 * ISR, it needs to be declared volatile.
 */
static volatile uint16_t daliData;


/**********************DALI Machine variables*(END)****************************/



/**********************Local function definitions******************************/


/** \brief Configure the lamp power level and the fader.
 *
 * This function checks whether the level is within the allowable range and sets
 * the limit error bit if it is needed. It computes the fader parameters and
 * starts it, if fading is required.
 *
 * @param level
 *  - if fade == FADE_NONE, go directly to this level (applying the
 *    minLevel/maxLevel bounds) \n
 *  - if fade == FADE_FADE_TIME, set the fader to run for as many steps as
 *    needed such that the lamp will be at this level after the selected fade
 *    time has elapsed \n
 *  - if fade == FADE_FADE_RATE, set the fader to run for 200ms. If level == 0
 *    fade down, otherwise fade up
 * @param fade Type of fade, one of FADE_NONE, FADE_FADE_TIME or FADE_FADE_RATE.
 * The meaning of the "level" parameter depends on this value
 */
static void idali_setArcPower(uint8_t level, uint8_t fade);


/** \brief Set a new short address.
 *
 * The function checks the parameter for the correct format and if it is ok,
 * sets it as the new short address. If it differs from the previous one,
 * updates the non-volatile memory as well. Updates the missing short address
 * bit. Can remove the short address when given the parameter 255.
 *
 * @param newAddress New address that should be set.
 */
static void idali_setShortAddress(uint8_t newAddress);


/** \brief Check if the given parameter represents a group that this Control
 * Gear belongs to.
 *
 * The address should be a valid group address, of the form 100x xxxS. Otherwise
 * (we don't belong to the group or the address is not a group address) this
 * function returns 0.
 *
 * @param address Address that should be checked.
 * @return
 *  - 1 if we belong to the group specified by the "address" parameter.\n
 *  - 0 otherwise (either wrong address type or we don't belong to the specified
 *    group)
 */
static inline uint8_t idali_doesGroupMatch(uint8_t address);


/** \brief Perform the DALI "RESET" command.
 *
 * This reinitialises most of the variables to known (reset) values.
 */
static void idali_reset();


/** \brief Perform the DALI "SAVE PERSISTENT VARIABLES" command.
 *
 * This physically stores all the variables identified as NVM in IEC 62386-102 
 * (ed2.0) Table 14 and applicable parts of 2xx.
 */
static void idali_savePersistentVariables();

/** \brief Run the DALI fading mechanism.
 *
 * This function is called every 5ms to update the lamp power in a continuous
 * fashion, according to the fade mechanism requested.
 */
static inline void idali_fade();


/** \brief DALI command decoder.
 *
 * If a DALI forward frame has been received by the protocol layer this function
 * is called by the mainline code to process it. This implements most of the
 * DALI subsystem, it processes every DALI forward frame, checks if the command
 * is addressed to this device and executes it if all conditions are met (some
 * commands require being sent twice, others need an activation command to
 * precede them).
 *
 * @param address First byte of DALI forward frame payload (address byte).
 * @param command Second byte of DALI forward frame payload (data byte).
 */
static inline void idali_processCommand(uint8_t address, uint8_t command);

/**********************Local function definitions*(END)************************/



/**********************Function implementations********************************/


void idali_initMachine()
{
      uint8_t i;

      // Call the DALI protocol initialiser
      idali_initProtocol();

      // Load last lamp power level
      lastLightLevel = nvmem_daliReadLastLevel();
      
  #ifdef DALI_USE_DEVICE_TYPE_6
      // Clear LED specific flags
      failureStatus.All = 0;
      operatingMode.All = 0;
      // Load bits from non-volatile memory
      i = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
  //    if ((i & NVMEMORY_MULTIPLE_BITS_CP_EN) != 0)
  //    {
  //        daliLedStateFlags.currentProtectorEnabled = 1;
  //    }
      if ((i & NVMEMORY_MULTIPLE_BITS_LIN_DIM) != 0)
      {
          operatingMode.nonLogDimmingCurve = 1;
      }
  //    if ((i & NVMEMORY_MULTIPLE_BITS_REF_FAIL) != 0)
  //    {
  //        failureStatus.referenceMeasurementFailed = 1;
  //    }

      // Read fast fade time
      fastFadeTime = nvmem_daliReadByte(NVMEMORY_FAST_FADE_TIME);

      // The physical minimum level needs to be adjusted based on the dimming
      // curve selected, such that the light output at the physical minimum level
      // be the same regardless of dimming curve choice.
      if (operatingMode.nonLogDimmingCurve == 0)
      {
          physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
      }
      else
      {
          physicalMinimumLevel = idali_getLampNonLogPhysicalMinimum();
      }
  #endif /* DALI_USE_DEVICE_TYPE_6 */

      // Load all other variables from the non-volatile memory
      powerOnLevel = nvmem_daliReadByte(NVMEMORY_POWER_ON_LEVEL);
      systemFailureLevel = nvmem_daliReadByte(NVMEMORY_SYSTEM_FAILURE_LEVEL);
      minLevel = nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL);
      maxLevel = nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL);
      fadeRate = nvmem_daliReadByte(NVMEMORY_FADE_RATE);
      fadeTime = nvmem_daliReadByte(NVMEMORY_FADE_TIME);
      extendedFadeTimeBase = nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_BASE);
      extendedFadeTimeMultiplier = nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER);
      opMode = nvmem_daliReadByte(NVMEMORY_OPERATING_MODE);
      shortAddress = nvmem_daliReadByte(NVMEMORY_SHORT_ADDRESS);
      randomAddressH = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H);
      randomAddressM = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M);
      randomAddressL = nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L);
      groupLow = nvmem_daliReadByte(NVMEMORY_GROUP_LOW);
      groupHigh = nvmem_daliReadByte(NVMEMORY_GROUP_HIGH);
      
      // Initialise the status byte with the PowerCycleSeen bit, 
      // the factory default value of resetState bit and, if we 
      // don't have a short address, the shortAddress bit as well.
      if (shortAddress == 255)
      {
          // PowerCycleSeen, shortAddress and resetState
          status.All = 0xE0;
      }
      else
      {
          // PowerCycleSeen, resetState
          status.All = 0xA0;
      }

      // Read scenes from the non-volatile memory.
      for (i = 0; i < 16; i++)
      {
          scenes[i] = nvmem_daliReadByte(NVMEMORY_SCENES_BASE + i);
      }

      // Set the searchAddress to the power on value
      searchAddressH = 0xFF;
      searchAddressM = 0xFF;
      searchAddressL = 0xFF;
   
      // Set initialisationStateDisabled and writelastLightLevelToNVMAtPowerUp
      daliStateFlags.All = 0x0048;
  //    daliStateFlags.All = 0;
  //    daliStateFlags.initialisationStateDisabled = 1;
}


void dali_tasks()
{
      static uint8_t i, address, command;
      static uint8_t oldArcPower = 0;

      // Check for new data and if we have some, handle it
      if (daliStateFlags.receivedPacket == 1)
      {
          address = (uint8_t)(daliData >> 8);
          command = (uint8_t)(daliData & 0xFF);
          idali_processCommand(address, command);
//          idali_processCommand(daliData >> 8, daliData & 0xFF);
          daliStateFlags.receivedPacket = 0;
      }

#ifdef DALI_USE_DEVICE_TYPE_6
      // Start with the failure status cleared, bits will be populated as
      // necessary
      failureStatus.All = 0;
      // Update operating mode
      operatingMode.All &= ~0x0F;
      operatingMode.All |= idali_getLampOperatingMode();

  //    // Check if we have had a failure in the reference system power measurement.
  //    if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_FAILED)
  //    {
  //        // Reference system power failed. Set corresponding bit
  //        failureStatus.referenceMeasurementFailed = 1;
  //        // Also update bit in the non-volatile memory
  //        i = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
  //        if ((i & NVMEMORY_MULTIPLE_BITS_REF_FAIL) == 0)
  //        {
  //            i |= NVMEMORY_MULTIPLE_BITS_REF_FAIL;
  //            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, i);
  //        }
  //    }
#endif /* DALI_USE_DEVICE_TYPE_6 */
      
      // Check if we are in the reset state, and if so, set the corresponding bit.
      status.resetState = 0;
      if (
          (powerOnLevel == 254) &&
          (systemFailureLevel == 254) &&
          (minLevel == physicalMinimumLevel) &&
          (maxLevel == 254) &&
          (fadeRate == 7) &&
          (fadeTime == 0) &&
          (extendedFadeTimeBase == 0) &&
          (extendedFadeTimeMultiplier == 0) &&
          (randomAddressH == 0xFF) &&
          (randomAddressM == 0xFF) &&
          (randomAddressL == 0xFF) &&
          (groupLow == 0) &&
          (groupHigh == 0))
      {
          status.resetState = 1;
      }

      // If any of the scenes are programmed, this Control Gear is not in the
      // reset state.
      for (i = 0; i < 16; i++)
      {
          if (scenes[i] != 255)
          {
              status.resetState = 0;
          }
      }
      
      if(status.powerCycleSeen == 1)
      {
          lastActiveLevel = maxLevel;
      }
      
      if(status.resetState == 1)
      {
          status.limitError = 0;
      }
      
      
      // Initially set lampArcPowerOn bit according to the requested lamp power.
      // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
      if (actualArcPower > 0)
      {
          status.lampOn = 1;
      }
      else
      {
          status.lampOn = 0;
      }

      // Obtain the lamp status from the application layer and set the failure
      // bits accordingly.
      i = idali_getLampStatus();
//#ifdef DALI_USE_DEVICE_TYPE_6
  //    if ((i & LAMP_STATUS_OPEN_CIRCUIT) != 0)
  //    {
  //        failureStatus.openCircuit = 1;
  //    }
  //    if ((i & LAMP_STATUS_SHORT_CIRCUIT) != 0)
  //    {
  //        failureStatus.shortCircuit = 1;
  //    }
  //    if ((i & LAMP_STATUS_LOAD_INCREASE) != 0)
  //    {
  //        failureStatus.loadIncrease = 1;
  //    }
  //    if ((i & LAMP_STATUS_LOAD_DECREASE) != 0)
  //    {
  //        failureStatus.loadDecrease = 1;
  //    }
  //    if ((i & LAMP_STATUS_THERMAL_OVEROAD) != 0)
  //    {
  //        failureStatus.thermalOverload = 1;
  //    }
  //    if ((i & LAMP_STATUS_THERMAL_SHUTDOWN) != 0)
  //    {
  //        failureStatus.thermalShutDown = 1;
  //    }
  //
  //    // Set the lamp failure bit in the status byte if any sort of failure is
  //    // detected.
  //    if (i == 0)
  //    {
  //        status.lampFailure = 0;
  //        failureStatus.currentProtectorActive = 0;
  //    }
  //
  //    if ((failureStatus.loadIncrease == 1) || (failureStatus.loadDecrease == 1))
  //    {
  //        // Current protector can become active, if needed
  //        if ((daliLedStateFlags.currentProtectorEnabled == 1) && (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUN_OK) && (actualArcPower > 0))
  //        {
  //            // If the current protector is enabled and there has been a
  //            // successful reference system power measurement, the current
  //            // protector becomes active, unless the lamp power level is 0.
  //            // This state also should clear the lampPowerOn bit and signal a
  //            // lamp failure.
  //            failureStatus.currentProtectorActive = 1;
  //            // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
  //            status.lampOn = 0;
  //            status.lampFailure = 1;
  //        }
  //    }
  //    
  //    if ((failureStatus.openCircuit == 1) || (failureStatus.shortCircuit == 1))
  //    {
  //        // Open or short circuits are considered lamp failures.
  //        status.lampFailure = 1;
  //        // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
  //        status.lampOn = 0;
  //    }
  //
  //    if (failureStatus.thermalShutDown == 1)
  //    {
  //        // During thermal shutdown, this bit should be 0.
  //        // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
  //        status.lampOn = 0;
  //    }
//#else
      if (i == 0)
      {
          status.lampFailure = 0;
      }
      else
      {
          status.lampFailure = 1;
          status.lampOn = 0;
      }
//#endif /* DALI_USE_DEVICE_TYPE_6 */

      // This mechanism of updating the lamp power values is used since
      // idali_setLampPower() will set a flag signaling the application that a
      // new power level needs to be set. This may need to access slow memory to
      // load values from tables or to compute the value on the fly. This method
      // limits these redundant operations.
      if ((actualArcPower != oldArcPower) || (daliStateFlags.writelastLightLevelToNVMAtPowerUp == 1))
  //    if ((actualArcPower != oldArcPower) || (writelastLightLevelToNVMAtPowerUp == 1))
      {
#ifdef DALI_USE_DEVICE_TYPE_6
          idali_setLampPower(actualArcPower, operatingMode.nonLogDimmingCurve);
#else
          idali_setLampPower(actualArcPower, 0);
#endif /* DALI_USE_DEVICE_TYPE_6 */
          oldArcPower = actualArcPower;
          // If powerOnLevel == 255 write to the non-volatile memory the value of
          // the set power level, such that at power up lastLightLevel can be retrieved
          if (powerOnLevel == 255)
          {
              daliStateFlags.writelastLightLevelToNVMAtPowerUp = 0;
  //            writelastLightLevelToNVMAtPowerUp = 0;
              nvmem_daliWriteLastLevel(actualArcPower);
          }
      }
    
}


void idali_tick1msMachine()
{
      static uint8_t divisor100 = 0;  // We need to derive a 100ms tick
      static uint8_t divisor5 = 0;    // ..and a 5ms one for fading

      // Divide 1kHz tick by 5 and run fader
      divisor5++;
      if (divisor5 == 5)
      {
          idali_fade();
          divisor5 = 0;
      }

      // This timer is used by the commands that need to be received twice within
      // 100ms.
      if (commandTimeout_ms != 0)
      {
          commandTimeout_ms--;
      }

      // This timer is used for the DAPC sequence
      if (dapcTimeout_ms != 0)
      {
          dapcTimeout_ms--;
      }

      // Interface failure. See IEC 62386-102, 9.3 (page 21).
      // Check the DALI line and if it's low keep decrementing a counter until it
      // hits 0. When it is 0, we are in the system failure state. If the DALI
      // line is not low, reload this counter to 550ms.
      if((PORTB_IN & (0x01 << 3)) == LOW)
  //    if (dalihw_isDALILineLow())
      {
          if (cableDisconnectTimeout_ms > 0)
          {
              cableDisconnectTimeout_ms--;
          }
      }
      else
      {
          cableDisconnectTimeout_ms = 550;
      }

      if (cableDisconnectTimeout_ms == 0)
      {
          if (systemFailureLevel != 255)
          {
  //            fade = FADE_NONE;
              idali_setArcPower(systemFailureLevel, FADE_NONE);
          }
      }

  //    // Divide 1kHz tick by 5 and run fader
  //    divisor5++;
  //    if (divisor5 == 5)
  //    {
  //        idali_fade();
  //        divisor5 = 0;
  //    }

      // Divite 1kHz tick by 100
      divisor100++;
      if (divisor100 == 100)
      {
          // 0.1s tick

          // After the INITIALISE command has been received there is a 15 minute
          // timer that needs to run. This is that timer.
          if (initialiseTimeout_100ms != 0)
          {
              initialiseTimeout_100ms--;
          }

          // After the IDENTIFY DEVICE command has been received there is a 10 seconds
          // timer that needs to run. This is that timer.
          if (identifyDeviceTimeout_100ms != 0)
          {
              identifyDeviceTimeout_100ms--;
          }

          if(identifyDeviceTimeout_100ms == 0)
          {
              idali_stopIdentificationProcedure();
          }
          
          // See IEC 62386-102, 9.2 (page 21).
          // This timer is reset by commands affecting power level.
          if (powerUpTimeout_100ms != 0)
          {
              powerUpTimeout_100ms--;
              if (powerUpTimeout_100ms == 0)
              {
                  // Go to powerOnLevel or if MASK, to the last power level used.
                  // Since the divisor needs to be reset at the end of this loop
                  // anyway, use divisor100 as a temporary variable to hold the
                  // power level we need to go to.
                  if (powerOnLevel == 255)
                  {
                      // If powerOnLevel == 255 we need to retrieve the last arc
                      // power level which is lastLightLevel used
                      divisor100 = lastLightLevel;
                  }
                  else
                  {
                      divisor100 = powerOnLevel;
                  }
                  // If divisor still == 255, this means that we started with
                  // powerOnLevel == 255 (and needed to retrieve the value from
                  // the buffer), but the buffer was empty
                  if (divisor100 == 255)
                  {
                      divisor100 = 254;
                  }
                  
                  idali_setArcPower(divisor100, FADE_NONE);
                  // The powerFailure bit is renamed powerCycleSeen according to IEC 62386-102 (ed2.0) Table 12 Page 45
                  status.powerCycleSeen = 1;
              }
          }

          // We may have used the divisor as a temporary variable inside this if,
          // so we need to set it to 0 at the end of this if block.
          divisor100 = 0;
      }

}


void idali_receiveForwardFrame(uint16_t data)
{
      // If data is in the buffer already and has not been read by the machine,
      // drop the current frame.
      if (daliStateFlags.receivedPacket == 0)
      {
          daliData = data;
          daliStateFlags.receivedPacket = 1;
      }
}


static void idali_setArcPower(uint8_t level, uint8_t fade)
{
      // level == 255 means "stop fading"
      // This function checks MIN and MAX levels and sets the limit error bit if
      // these are violated.

      // Disable the power-up timer. This timer is only run at power-up and runs
      // for 600ms. If SetArcPower is not called within these 600ms, the value
      // stored in the powerOnLevel variable will be the initial power level.
      powerUpTimeout_100ms = 0;

      // Clear power failure bit. This should be set if the Control Gear has not
      // received any RESET or arc power control command since power-on. The RESET
      // command ends up calling this function so it will clear this bit as well.
      // The powerFailure bit is renamed powerCycleSeen according to IEC 62386-102 (ed2.0) Table 12 Page 45
      status.powerCycleSeen = 0;

      if (fade != FADE_FADE_RATE)
      {
          // Check that the required level is within the acceptable range and if
          // it isn't, bring it within minLevel and maxLevel
          if (level == 0)
          {
              // level == 0 is acceptable either way.
              status.limitError = 0;
          }
          else if (level == 255)
          {
              // 255 means "STOP FADING. See IEC 62386-102, 11.1.1 (page 27).
              level = actualArcPower;
              fade = FADE_NONE;
          }
          else if ((level > maxLevel) || (daliStateFlags.maxLevelChanged == 1))
  //        else if ((level > maxLevel) || (maxLevelChanged == 1))
          {
              level = maxLevel;
              status.limitError = 1;
              daliStateFlags.maxLevelChanged = 0;
  //            maxLevelChanged = 0;
          }
          else if ((level < minLevel) || (daliStateFlags.minLevelChanged == 1))
  //        else if ((level < minLevel) || (minLevelChanged == 1))
          {
              level = minLevel;
              status.limitError = 1;
              daliStateFlags.minLevelChanged = 0;
  //            minLevelChanged = 0;
          }
          else
          {
              status.limitError = 0;
          }

      }
      
      // idali_fade() runs from the ISR and can disturb these values. Disabling
      // interrupts for this section protects against this. The interrupts stay
      // disabled for max. 55us (PRO), 70us (Free) on an Enhanced Mid-Range
      // core @ 32MHz at the end of a successful forward frame reception (during
      // the forbidden period after a frame reception).
      cli();

      if(level != 0)
      {       
          lastActiveLevel = level;     
      }
      
      // Based on the fade settings set the power level or start fading
      if (fade == FADE_NONE)
      {
          // No fading, go straight to the power level requested
          fadeCount = 0;
          actualArcPower = level;
      }
      else if (fade == FADE_FADE_RATE)
      {
          fadeStart = 1;
          
          // Fade rate is used with commands "UP" and "DOWN". These should dim up
          // or down 200ms using the selected fade rate. Since we run the fader
          // every 5ms, 200ms makes 40 steps.
          fadeCount = 40;

          // Tell the fading loop that we aren't using fade time (there is no
          // target level defined, just stop wherever we get after 200ms.
          fadeTimeTargetLevel = 255;

          // When using the fade rate, the level parameter gives the fading
          // direction: 0 to fade down, 1 to fade up.
          if (level == 0)
          {
              daliStateFlags.fadeUp = 0;
              actualArcPower -= 1;
          }
          else
          {
              daliStateFlags.fadeUp = 1;
              actualArcPower += 1;
          }
   
          // Load the step delta from the (precomputed) table
          fadeDelta = fadeRateTable[fadeRate];

          // If the fader is not running, reinitialise the counter. Otherwise
          // don't do any initialisation. This allows us to use the full precision
          // of the fader and make a smoother transition.
          if (status.fadeRunning == 0)
          {
              fadeAccumulator.byte[0] = 0;
              fadeAccumulator.byte[1] = 0;
              fadeAccumulator.byte[2] = 0;
              fadeAccumulator.byte[3] = actualArcPower;
          }
      }
      else if (fade == FADE_FADE_TIME)
      {
          // Use fade time. If we are in a DAPC sequence, we should use a
          // "special" fade time that is loosely defined in the standard.
          // Otherwise, use either the programmed fade time or the programmed fast
          // fade time or the extended fade time.
          if (dapcTimeout_ms == 0)
          {
              if (level == actualArcPower)
              {
                  // We're trying to fade to the same level, which is a NOP. This
                  // can cause problems in the fader for
                  // level == actualArcPower == 0.
                  fadeCount = 0;
              }
              else
              {
                  // fadeStart is used to indicate the idali_fade() function that 
                  // the control is entering the idali_fade() function for the first 
                  // time from idali_setArcPower(). This is required since the fadeDelta 
                  // for first and last step of fading should be twice the fadeDelta 
                  // required for the intermediate steps. This variables is cleared after 
                  // the first change of actualArcPower, i.e. at actualArcPower - 1. See  
                  // IEC 62386-102 (ed2.0) Figure 4 Page 26
                  fadeStart = 1;
                  
                  // We are not in a DAPC sequence. Load number of steps we should
                  // run the fader from the (precomputed) table.
                  fadeCount = fadeTimeTable[fadeTime];

#ifdef DALI_USE_DEVICE_TYPE_6
                  // If this value turns out to be 0, we should use the fast fade time
                  // defined for LED devices.
                  if (fadeCount == 0)
                  {
                      // Load number of steps we should run the fader from the
                      // other (precomputed) table.
                      fadeCount = fastFadeTimeTable[fastFadeTime];
  //                    // If this value turns out to be 0, we should use the extended
  //                    // fade time if fast fade time is defined for LED devices.
  //                    if(fadeCount == 0)
  //                    {
  //                        if((extendedFadeTimeBase == 0) && (extendedFadeTimeMultiplier == 0))
  //                        {
  //                            fadeCount = 0;
  //                        }
  //                        else if(extendedFadeTimeBase <= 16)
  //                        {
  //                            fadeCount = extendedFadeTimeTable[extendedFadeTimeBase][extendedFadeTimeMultiplier];
  //                        }
  //                    }
                  }
#endif /* DALI_USE_DEVICE_TYPE_6 */
                  // If the value of fadeCount and fastFadeCount (if fast fade time  
                  // is defined for LED devices) turns out to be 0, we should use  
                  // the extended fade time.
                  if(fadeCount == 0)
                  {
                      if(extendedFadeTimeMultiplier == 0)
  //                    if((extendedFadeTimeBase == 0) && (extendedFadeTimeMultiplier == 0))
                      {
                          fadeCount = 0;
                      }
                      else if(extendedFadeTimeBase <= 16)
                      {
                          fadeCount = (extendedFadeTimeBase + 1) * extendedFadeTimeMulTable[extendedFadeTimeMultiplier];
  //                        fadeCount = extendedFadeTimeTable[extendedFadeTimeBase][extendedFadeTimeMultiplier];
                      }
                  }
              }
          }
          else
          {
              // We are in a DAPC sequence, use "special" fade time and also
              // reload timer such that the sequence can be as long as the Control
              // Device wants it.
              dapcTimeout_ms = 200;
  //            fadeCount = DAPC_SEQUENCE_FADE_TIME_MS / 5;
              fadeCount = 40;
          }

          if (fadeCount == 0)
          {
              // If it turns out that the fade time is actually 0, go straight to
              // that power level and don't start the fader.
              actualArcPower = level;
          }
          else
          {
              // At the end of fadeCount, if fadeTimeTargetLevel is not 255, it
              // will be set as the arc power level. This is required because the
              // approximations we use here may not bring us (after fading) to the
              // exact level the Control Device required.
              fadeTimeTargetLevel = level;

              if ((actualArcPower == 0) && (level != 0))
              {
                  // Lamp needs to be switched on. Do this now.
                  actualArcPower = minLevel;
              }

              if ((actualArcPower != 0) && (level == 0))
              {
                  // The lamp should be switched off. However, we need to take a
                  // single step from minLevel to off.
                  level = minLevel;
                  fadeCount++;
              }

              if (level > actualArcPower)
              {
                  // We should be fading up, as the target is higher than the
                  // current level
                  daliStateFlags.fadeUp = 1;
                  fadeDelta = level - actualArcPower;
              }
              else
              {
                  // We should be fading down
                  daliStateFlags.fadeUp = 0;
                  fadeDelta = actualArcPower - level;
              }
              fadeDelta <<= 24;
              fadeDelta /= fadeCount;

              // Load accumulator values
              fadeAccumulator.byte[0] = 0;
              fadeAccumulator.byte[1] = 0;
              fadeAccumulator.byte[2] = 0;
              fadeAccumulator.byte[3] = actualArcPower;
          }
      }
      sei();
}


static void idali_setShortAddress(uint8_t newAddress)
{
      // This function should be used to update the short address, as it will
      // check for the correct format and will also erase the short address when
      // given 0xFF as a parameter.
      uint8_t oldAddress;
      oldAddress = shortAddress;

      if (newAddress == 0xFF)
      {
          // Delete short address
          shortAddress = 0xFF;
          // The missingShortaddress bit is renamed shortAddress according to IEC 62386-102 (ed2.0) Table 12 Page 45
          status.shortAddress = 1;
      }
      else if ((newAddress & 0x81) == 0x01)
      {
          // If the address has the right format, i.e. 0AAAAAA1, program
          // short address 00AAAAAA according to IEC 62386-102 (ed2.0)
          shortAddress = (newAddress >> 1);
          // The missingShortaddress bit is renamed shortAddress according to IEC 62386-102 (ed2.0) Table 12 Page 45
          status.shortAddress = 0;
      }

      if (newAddress != oldAddress)
      {
          // If our new address is different from the previously stored one,
          // update the non-volatile memory as well
          nvmem_daliWriteByte(NVMEMORY_SHORT_ADDRESS, shortAddress);
      }
}


static inline uint8_t idali_doesGroupMatch(uint8_t address)
{
      uint8_t val;
      
      val = 0;
      
      // Firstly, check if the parameter given has the correct format to be a
      // group address. Return 0 if it doesn't
      if ((address & 0xE0) == 0x80) // address is 100x xxxx
      {
          // Strip off extra bits; val = group number
          val = (address >> 1) & 0x0F;
          if (val < 8)
          {
              // We should check the groupLow variable
              if ((groupLow & groupMask[val]) != 0)
              {
                  // We do belong to this group, return 1
                  return 1;
              }
              else
              {
                  return 0;
              }
          }
          else
          {
              // We should check the groupHigh variable
              if ((groupHigh & groupMask[val - 8]) != 0)
              {
                  // We do belong to this group, return 1
                  return 1;
              }
              else
              {
                  return 0;
              }
          }
      }
      else
      {
          return 0;
      }
}



static void idali_reset()
{
      uint8_t i;

      // This function loads the reset values into the variables. If the newly
      // loaded values differ from the ones that were saved to the non-volatile
      // memory, update the non-volatile memory as well.

#ifdef DALI_USE_DEVICE_TYPE_6
      operatingMode.nonLogDimmingCurve = 0;
      physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
      
      fastFadeTime = 0;
      if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != fastFadeTime)
      {
          nvmem_daliWriteByte(NVMEMORY_FADE_TIME, 0);
      }
#endif /* DALI_USE_DEVICE_TYPE_6 */

      if (shortAddress == 255)
      {
          // Reset state AND Missing short address
          status.All = 0x60;
      }
      else
      {
          // Reset state
          status.All = 0x20;
      }

      minLevel = physicalMinimumLevel;
      if (nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL) != physicalMinimumLevel)
      {
          nvmem_daliWriteByte(NVMEMORY_MINIMUM_LEVEL, physicalMinimumLevel);
      }

      maxLevel = 0xFE;
      if (nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL) != 0xFE)
      {
          nvmem_daliWriteByte(NVMEMORY_MAXIMUM_LEVEL, 0xFE);
      }

      actualArcPower = 0xFE;
      idali_setArcPower(actualArcPower, FADE_NONE);
      
      lastActiveLevel = 0xFE;
      
      lastLightLevel = 0xFE;
      if (nvmem_daliReadLastLevel() != 0xFE)
      {
          nvmem_daliWriteLastLevel(lastLightLevel);
      }
      
      powerOnLevel = 0xFE;
      if (nvmem_daliReadByte(NVMEMORY_POWER_ON_LEVEL) != 0xFE)
      {
          nvmem_daliWriteByte(NVMEMORY_POWER_ON_LEVEL, 0xFE);
      }

      systemFailureLevel = 0xFE;
      if (nvmem_daliReadByte(NVMEMORY_SYSTEM_FAILURE_LEVEL) != 0xFE)
      {
          nvmem_daliWriteByte(NVMEMORY_SYSTEM_FAILURE_LEVEL, 0xFE);
      }

      fadeRate = 7;
      if (nvmem_daliReadByte(NVMEMORY_FADE_RATE) != 7)
      {
          nvmem_daliWriteByte(NVMEMORY_FADE_RATE, 7);
      }

      fadeTime = 0;
      if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != 0)
      {
          nvmem_daliWriteByte(NVMEMORY_FADE_TIME, 0);
      }
      
      extendedFadeTimeBase = 0;
      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_BASE) != 0)
      {
          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_BASE, 0);
      }
      
      extendedFadeTimeMultiplier = 0;
      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER) != 0)
      {
          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER, 0);
      }

      searchAddressH = 0xFF;
      searchAddressM = 0xFF;
      searchAddressL = 0xFF;

      randomAddressH = 0xFF;
      randomAddressM = 0xFF;
      randomAddressL = 0xFF;
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H) != 0xFF)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_H, 0xFF);
      }
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M) != 0xFF)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_M, 0xFF);
      }
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L) != 0xFF)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_L, 0xFF);
      }

      groupLow = 0;
      if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != 0)
      {
          nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, 0);
      }

      groupHigh = 0;
      if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != 0)
      {
          nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, 0);
      }


      for (i = 0; i < 16; i++)
      {
          scenes[i] = 255;
          if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + i) != 255)
          {
              nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + i, 255);
          }
      }

}


static void idali_savePersistentVariables()
{
      // This function forces the control gear to physically write all variables of 
      // type NVM in Table 14 to memory (NVM). This includes all application extended  
      // NVM variables defined in the applicable parts 2xx.

#ifdef DALI_USE_DEVICE_TYPE_6
      
      // Store fastFadeTime in NVM
      if (nvmem_daliReadByte(NVMEMORY_FAST_FADE_TIME) != fastFadeTime)
      {
          nvmem_daliWriteByte(NVMEMORY_FAST_FADE_TIME, fastFadeTime);
      } 
      
      // NVMEMORY_DIMMING_CURVE is not being written to anywhere in the application 
      // and NVMEMORY_MULTIPLE_BITS is being handled by directly accessing the NVM 
      // memory location (without using a global variable for it).
          
#endif /* DALI_USE_DEVICE_TYPE_6 */

      
      // Store lastLightLevel in NVM
      if (nvmem_daliReadLastLevel() != lastLightLevel)
      {
          nvmem_daliWriteLastLevel(lastLightLevel);
      }

      // Store powerOnLevel in NVM
      if (nvmem_daliReadByte(NVMEMORY_POWER_ON_LEVEL) != powerOnLevel)
      {
          nvmem_daliWriteByte(NVMEMORY_POWER_ON_LEVEL, powerOnLevel);
      }

      // Store systemFailureLevel in NVM
      if (nvmem_daliReadByte(NVMEMORY_SYSTEM_FAILURE_LEVEL) != systemFailureLevel)
      {
          nvmem_daliWriteByte(NVMEMORY_SYSTEM_FAILURE_LEVEL, systemFailureLevel);
      }

      // Store minLevel in NVM
      if (nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL) != minLevel)
      {
          nvmem_daliWriteByte(NVMEMORY_MINIMUM_LEVEL, minLevel);
      }

      // Store minLevel in NVM
      if (nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL) != maxLevel)
      {
          nvmem_daliWriteByte(NVMEMORY_MAXIMUM_LEVEL, maxLevel);
      }

      // Store fadeRate in NVM
      if (nvmem_daliReadByte(NVMEMORY_FADE_RATE) != fadeRate)
      {
          nvmem_daliWriteByte(NVMEMORY_FADE_RATE, fadeRate);
      }

      // Store fadeTime in NVM
      if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != fadeTime)
      {
          nvmem_daliWriteByte(NVMEMORY_FADE_TIME, fadeTime);
      }
      
      // Store extendedFadeTimeBase in NVM
      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_BASE) != extendedFadeTimeBase)
      {
          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_BASE, extendedFadeTimeBase);
      }
      
      // Store extendedFadeTimeMultiplier in NVM
      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER) != extendedFadeTimeMultiplier)
      {
          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER, extendedFadeTimeMultiplier);
      }
      
      // Store shortAddress in NVM
      if (nvmem_daliReadByte(NVMEMORY_SHORT_ADDRESS) != shortAddress)
      {
          nvmem_daliWriteByte(NVMEMORY_SHORT_ADDRESS, shortAddress);
      }
      
      // Store randomAddress in NVM
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H) != randomAddressH)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_H, randomAddressH);
      }
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M) != randomAddressM)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_M, randomAddressM);
      }
      if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L) != randomAddressL)
      {
          nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_L, randomAddressL);
      }

      // Store opMode in NVM
      if (nvmem_daliReadByte(NVMEMORY_OPERATING_MODE) != opMode)
      {
          nvmem_daliWriteByte(NVMEMORY_OPERATING_MODE, opMode);
      }

      // Store group information in NVM
      if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != groupLow)
      {
          nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, groupLow);
      }

      if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != groupHigh)
      {
          nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, groupHigh);
      }

      // Store sceneX in NVM
      for (uint8_t i = 0; i < 16; i++)
      {
          if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + i) != scenes[i])
          {
              nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + i, scenes[i]);
          }
      }

      // ToDo Modify the code to set the fading to the transition that was active
      idali_setArcPower(actualArcPower, FADE_NONE);
}


static inline void idali_fade()
{
      static uint8_t actualArcPowerTemp;
      static uint32_t fadeDeltaTemp;
      
      // Check if fader should be running. If it shouldn't, clear the fade running
      // bit and return
      if (fadeCount != 0)
      {
          // Decrement counter and signal that the fader is running
          fadeCount--;
          status.fadeRunning = 1;

          if ((fadeCount == 0) && (fadeTimeTargetLevel != 255))
          {
              // This is the last step of a fade with fade time. This should bring
              // us EXACTLY at the requested target level, even if the math that
              // uses approximations didn't.
              // fadeTimeTargetLevel is set by idali_setArcPower() to:
              //   - 255 if we are using the fade rate (where no target level is
              //     defined, fade rate means we should fade for 200ms and then
              //     stop wherever it lands us)
              //   - a target power level (i.e. different from 255) when using any
              //     form of fade time (fade time, fast fade time, extended fade time
              //     or the special fade time used in the DAPC sequence)
              actualArcPower = fadeTimeTargetLevel;
          }
          else
          {
              // Not the last step of a fade. We should just update
              // the fade counter by applying the increment/decrement
              if (daliStateFlags.fadeUp == 0)
              {
                  // Using fade rate
                  if(fadeTimeTargetLevel == 255)
                  {
                      if(fadeStart == 1) 
                      {
  //                        fadeDeltaTemp = fadeDelta *2;
                          fadeDeltaTemp = fadeDelta << 1;
                      }
                      
                      fadeAccumulator.counter -= fadeDeltaTemp;
  //                    fadeAccumulator.counter -= fadeDelta;
  //                    if (fadeAccumulator.byte[3] != 0)
  //                    {
  //                        // Underflow. We faded down too much. See the description
  //                        //  offade_counter for details
  //                        fadeAccumulator.counter = 0x00000000;
  //                    }
                      if (fadeAccumulator.byte[3] < minLevel)
                      {
                          fadeAccumulator.byte[3] = minLevel;
                      }
                      // Set the lamp power to the relevant part of the accumulator.
                      // See the description of fade_counter for details
  //                    if(((fadeAccumulator.byte[2]) <= 0xA0) && (fadeAccumulator.byte[3] <= (actualArcPower - 1)))
  //                    {
  //                        fadeStart = 0;
  //                        fadeDeltaTemp = fadeDelta;
  //                        actualArcPower = fadeAccumulator.byte[3];
  //                    }
                      actualArcPowerTemp = actualArcPower;
                      if((fadeAccumulator.byte[3] <= (actualArcPowerTemp - 1)))
                      {
                          fadeStart = 0;
                          fadeDeltaTemp = fadeDelta;
                          actualArcPower = fadeAccumulator.byte[3];
                      }
                  }
                  // Using fade time
                  else
                  {
                      if(fadeStart == 1) {
  //                        fadeDeltaTemp = fadeDelta *2;
                          fadeDeltaTemp = fadeDelta << 1;
                      }
                      fadeAccumulator.counter -= fadeDeltaTemp;
  //                    if (fadeAccumulator.byte[3] != 0)
  //                    {
  //                        // Underflow. We faded down too much. See the description
  //                        // of fade_counter for details
  //                        fadeAccumulator.counter = 0x00000000;
  //                    }
                      if (fadeAccumulator.byte[3] < minLevel)
                      {
                          fadeAccumulator.byte[3] = minLevel;
                      }
                      // Set the lamp power to the relevant part of the accumulator.
                      // See the description of fade_counter for details
                      actualArcPowerTemp = actualArcPower;
                      if(((fadeAccumulator.byte[2]) <= 0x0F) && (fadeAccumulator.byte[3] <= (actualArcPowerTemp - 1)))
//                      if((fadeAccumulator.byte[3] == (actualArcPower - 1)))
                      {
                          fadeStart = 0;
                          fadeDeltaTemp = fadeDelta;
                          actualArcPower = fadeAccumulator.byte[3];
                      }
  //                    if(((fadeAccumulator.counter & 0xFFFF) > (0xFFFF - (uint16_t)fadeDeltaTemp)) && (fadeAccumulator.byte[2] <= (actualArcPower - 1)))
  ////                    if((fadeAccumulator.byte[2] == (actualArcPower - 1)))
  //                    {
  //                        fadeStart = 0;
  //                        fadeDeltaTemp = fadeDelta;
  //                        actualArcPower = fadeAccumulator.byte[2];
  //                    }
                  }
              }
              else
              {
                  // Using fade rate
                  if(fadeTimeTargetLevel == 255) 
                  {
                      if(fadeStart == 1) 
                      {
  //                        fadeDeltaTemp = fadeDelta *2;
                          fadeDeltaTemp = fadeDelta << 1;
                      }
                      
                      fadeAccumulator.counter += fadeDeltaTemp;
  //                    fadeAccumulator.counter += fadeDelta;
  //                    if (fadeAccumulator.byte[3] != 0)
  //                    {
  //                        // Overflow. We faded up too much. See the description of
  //                        // fade_counter for details
  //                        fadeAccumulator.counter = 0x00ffffff;
  //                    }
                      if (fadeAccumulator.byte[3] > maxLevel)
                      {
                          fadeAccumulator.byte[3] = maxLevel;
                      }
                      // Set the lamp power to the relevant part of the accumulator. See
                      // the description of fade_counter for details
  //                    actualArcPower = fadeAccumulator.byte[2];
                      actualArcPowerTemp = actualArcPower;
                      if(fadeAccumulator.byte[3] >= (actualArcPowerTemp + 1))
                      {
                          fadeStart = 0;
                          fadeDeltaTemp = fadeDelta;
                          actualArcPower = fadeAccumulator.byte[3];
                      }
                  }
                  // Using fade time
                  else
                  {
                      if(fadeStart == 1) {
  //                        fadeDeltaTemp = fadeDelta *2;
                          fadeDeltaTemp = fadeDelta << 1;
                      }
                      fadeAccumulator.counter += fadeDeltaTemp;
  //                    if (fadeAccumulator.byte[3] != 0)
  //                    {
  //                        // Overflow. We faded up too much. See the description of
  //                        // fade_counter for details
  //                        fadeAccumulator.counter = 0x00ffffff;
  //                    }
                      if (fadeAccumulator.byte[3] > maxLevel)
                      {
                          fadeAccumulator.byte[3] = maxLevel;
                      }
                      // Set the lamp power to the relevant part of the accumulator. See
                      // the description of fade_counter for details
                      actualArcPowerTemp = actualArcPower;
                      if(fadeAccumulator.byte[3] == (actualArcPowerTemp + 1))
                      {
                          fadeStart = 0;
                          fadeDeltaTemp = fadeDelta;
                          actualArcPower = fadeAccumulator.byte[3];
                      }
                  }
              }
          }
      }
      else
      {
          // No fade running
          status.fadeRunning = 0;
      }
}


static inline void idali_processCommand(uint8_t address, uint8_t command)
{
      // The command may be addressed to this Control Gear, to every Control
      // Gear or to some other Control Gear. This bunch of flags will keep this
      // sort of information.
      tdali_flags_command f;

      // Some commands are of the form
      // [1 start bit][8 bit address][4 bit command][4 bit parameter]
      // For these, this variable will hold the parameter
      uint8_t parameter;

      // The NVMemoryDALIBankMemoryRead() and NVMemoryDALIBankMemoryWrite() return
      // values end up in this variable for status checks
      uint16_t data;

      // Clear all addressing flags
      f.All = 0;

      // Firstly check if this device is addressed, since we need to know this
      // in case we were expecting the 2nd command of a "send twice" group.
      if ((address > 0xA0) && ((address & 0xFE) != 0xFE) && ((address & 0xFC) != 0xFC))
      {
          // This is one of the commands that does not include an address field
          // and should be interpreted by all Control Gears, irrespective of their
          // addresses.
          f.publicAddress = 1;
          f.deviceAddressed = 1;
      }
      else if (((address & 0xFE) == 0xFE) ||                                 // Broadcast command
  //             ((address & 0xFE) == (shortAddress & 0xFE)) ||              // Short address match
               ((address & 0xFE) == (((shortAddress << 1) + 1) & 0xFE)) ||   // Short address match
               (idali_doesGroupMatch(address) != 0) ||                       // Group address match
              (((address & 0xFC) == 0xFC) && (status.shortAddress == 1)))    // Broadcast unaddressed         
      {
          // This is either a broadcast command, a command sent to our short
          // address, broadcast unaddressed command (should only be processed 
          // when the device does not have a short address) or to a group that 
          // we belong to. Either way, we are being targeted with it.
          f.individualAddress = 1;
          f.deviceAddressed = 1;
      }

  //#ifdef DALI_USE_DEVICE_TYPE_6
  //    // During Reference system power all other commands except query commands
  //    // and TERMINATE should be ignored. Although not mentioned in the
  //    // standard, the "ENABLE DEVICE TYPE 6" command also needs to be allowed
  //    // since it gives access to LED specific queries.
  //    if ((f.deviceAddressed == 1) && (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING))
  //    {
  //        if (!(((address == 0xA1) && (command == 0x00)) ||                               // TERMINATE should still be allowed
  //              ((address == 0xC1) && (command == 0x06)) ||                               // ENABLE DEVICE TYPE 6 should still be allowed
  //              ((f.individualAddress == 1) && (command >= 0xED)) ||                      // General query commands
  //              ((f.individualAddress == 1) && (command >= 0x90) && (command < 0xB0))))   // DEV TYPE 6 query commands
  //        {
  //            // If the command we received is NOT one of the allowed ones, abort.
  //            return;
  //        }
  //    }
  //#endif /* DALI_USE_DEVICE_TYPE_6 */


#ifdef DALI_USE_EXTRA_MEMORY_BANKS

      // Create a latch for the write enabled state. Since the device is addressed
      // most commands would now clear the write enabled state. However, the
      // "WRITE MEMORY LOCATION" and "WRITE MEMORY LOCATION - NO REPLY" commands 
      // need to know the status of the writeenabled bit. Using this mechanism we 
      // can clear the write enabled state for the following commands (and if the 
      // current command is one that reenables it, it can do so) but when decoding
      //  the command we can use the old value of the write enabled state (as set 
      // by the previous command).
      daliStateFlags.writeEnabledOld = daliStateFlags.writeEnabled;
      daliStateFlags.writeEnabled = 0;

#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

      if ((lastCommand == command) && (lastAddress == address) && (commandTimeout_ms != 0))
      {
          // Signal that this is a repeated command and also clear timer such that
          // we can't send a command 3 times and have it executed twice.
          f.commandRepeated = 1;
          commandTimeout_ms = 0;
      }
      else if (((lastCommand != command) || ((lastAddress != address))) && (commandTimeout_ms != 0))
      {
          // Signal that this is not a repeated command and still needs to be processed
          // and also clear timer such that we can't send a command 3 times and
          // have it executed twice.
          f.commandRepeated = 0;
          commandTimeout_ms = 0;
      }
      
      // Identification shall be stopped immediately upon reception of any instruction 
      // other than IDENTIFY DEVICE, INITIALISE (device), RECALL MIN LEVEL, RECALL MAX 
      // LEVEL   
      if ((command != 0x25) && (address != 0xA5) && (command != 0x05) && (command != 0x06))
      {
          identifyDeviceTimeout_100ms = 0;
      }

#ifdef DALI_USE_DEVICE_TYPE_6
      if (f.deviceAddressed == 1)
      {
          // Create a latch for the enable device type bit, working similarly to
          // the one for memory write enable.
          daliStateFlags.enabledDeviceTypeOld = daliStateFlags.enabledDeviceType;
          daliStateFlags.enabledDeviceType = 0;
      }
#endif /* DALI_USE_DEVICE_TYPE_6 */

      if ((f.commandRepeated == 1) || (commandTimeout_ms == 0))
      {
          // We end up here if we should pay attention to this command, i.e. if
          // either this is the 2nd one of a "send twice" sequence that has been
          // addressed to us, or if no command that started the 100ms timer was
          // sent previously.

          // Check if this is one of those unaddressed commands that should be
          // listened to by everyone.
          // The full 16bit command is address:command, but in this case the type
          // of command to be executed is encoded in the address field and any
          // parameter is in the command field.
          if (f.publicAddress == 1)
          {
              switch (address)
              {
                  case TERMINATE:      // TERMINATE
                      if (command == 0x00)
                      {
                          // Cancel the initialise 15 minute timer
                          initialiseTimeout_100ms = 0;
                          // Cancel the identify device 10 seconds timer
                          identifyDeviceTimeout_100ms = 0;
                          daliStateFlags.initialisationStateDisabled = 1;
                          daliStateFlags.initialisationStateEnabled = 0;
                          daliStateFlags.initialisationStateWithdrawn = 0;
  //#ifdef DALI_USE_DEVICE_TYPE_6
  //                        // And stop the reference system power measurement if
  //                        // it's running.
  //                        if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING)
  //                        {
  //                            idali_stopReferenceSystemPower();
  //                        }
  //#endif /* DALI_USE_DEVICE_TYPE_6 */
                      }
                      break;
                  case DTR0_DATA:      // DTR0 (data)
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                      // writeEnabled should be set to 1 according to 
                      // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
  //                    daliStateFlags.writeEnabled = 1;
                      if (daliStateFlags.writeEnabledOld == 1)
                      {
                          // Keep write enabled state. Set the "new" value to 1
                          daliStateFlags.writeEnabled = 1;
                      }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                         
                      // Load value into the DTR
                      DTR = command;
                      break;
                  case INITIALISE:      // INITIALISE
                      if (f.commandRepeated == 1)
                      {
                          // Command received twice within 100ms, so we need to
                          // start the 15 minute timer and set some default
                          // values.
                          initialiseTimeout_100ms = 15 * 60 * 10;
  //                        daliStateFlags.withdrawFromCompare = 0;
                          if(daliStateFlags.initialisationStateDisabled == 1)
                          {
                              daliStateFlags.initialisationStateDisabled = 0;
                              daliStateFlags.initialisationStateWithdrawn = 0;
                              daliStateFlags.initialisationStateEnabled = 1;
                          }
                          // We also need to start an identification procedure
                          // that can be defined by the manufacturer.
                          idali_startIdentificationProcedure();
                      }
                      else if ((command == 0) ||
                              ((command == 0xFF) && (shortAddress == 0xFF)) ||
                              (command == ((shortAddress << 1) + 1)))
                      {
                          // Command has not been received twice within 100ms, so
                          // we start the timer and wait for the next one.
                          commandTimeout_ms = 100;
                      }
                      break;
                  case RANDOMISE:      // RANDOMISE
                      if(daliStateFlags.initialisationStateDisabled != 1)
                      {
                          if (f.commandRepeated == 1)
                          {
                              // Command received twice within 100ms, so we need to
                              // pull random data from the application layer and store
                              // it as our random address. Also store it to
                              // non-volatile memory.
                              randomAddressH = random_byte();
                              if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_H) != randomAddressH)
                              {
                                  nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_H, randomAddressH);
                              }
                              randomAddressM = random_byte();
                              if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_M) != randomAddressM)
                              {
                                  nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_M, randomAddressM);
                              }
                              randomAddressL = random_byte();
                              if (nvmem_daliReadByte(NVMEMORY_RANDOM_ADDRESS_L) != randomAddressL)
                              {
                                  nvmem_daliWriteByte(NVMEMORY_RANDOM_ADDRESS_L, randomAddressL);
                              }
                              daliStateFlags.initialisationStateWithdrawn = 0;
                          }
                          else if (command == 0x00)
                          {
                              // This command is only valid in the form 0xA700. It may
                              // seem strange that the check for the lower 8 bits is
                              // only done here and not when the command is repeated,
                              // but whenever f.commandRepeated == 1 we know for sure
                              // that the command received is identical to the
                              // previous one (where this check has been made).
                              if (initialiseTimeout_100ms != 0)
                              {
                                  // Command has not been received twice within 100ms,
                                  // so we start the timer and wait for the next one.
                                  commandTimeout_ms = 100;
                              }
                          }
                      }
                      break;
                  case COMPARE:      // COMPARE
                      // See IEC 62386-102, 11.7.6 (page 69).
                      if (command == 0x00)
                      {
                          // Command is actually 0xA900, thus we should execute it
                          if (initialiseTimeout_100ms != 0)
                          {
                              if ((daliStateFlags.initialisationStateEnabled == 1))
                              {
                                  // If compare is enabled, check our random
                                  // address against the search address
                                  if ((randomAddressH < searchAddressH) ||
                                      ((randomAddressH == searchAddressH) && (randomAddressM < searchAddressM)) ||
                                      ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL <= searchAddressL)))
                                  {
                                      // Random address is less than or equal to
                                      // the search address.
                                      idali_sendBackwardFrame(0xFF);
                                  }
                              }
                          }
                      }
                      break;
                  case WITHDRAW:      // WITHDRAW
                      if (command == 0x00)
                      {
                          if (initialiseTimeout_100ms != 0)
                          {
                              if ((daliStateFlags.initialisationStateEnabled == 1) && (randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL))
                              {
                                  // We are being excluded from the compare
                                  // process, but NOT from the initialisation
                                  // process
                                  daliStateFlags.withdrawFromCompare = 1;
                                  daliStateFlags.initialisationStateWithdrawn = 1;
                                  daliStateFlags.initialisationStateEnabled = 0;
                                  daliStateFlags.initialisationStateDisabled = 0;
                              }
                          }
                      }
                      break;
                  case PING:      // PING
                      // This command is added in IEC 62386-102 (ed2.0) 
                      // The ping command shall be ignored by control gear
                      break;
                  case SEARCHADDRH:      // SEARCHADDRH (data)
                      if (initialiseTimeout_100ms != 0)
                      {
                          // The instruction shall be ignored if initialisationState is equal to DISABLED.
                          if((daliStateFlags.initialisationStateDisabled != 1))
                          {
                              searchAddressH = command;
                          }
                      }
                      break;
                  case SEARCHADDRM:      // SEARCHADDRM (data)
                      // Modified according to IEC 62386-102 (ed2.0)
                      if (initialiseTimeout_100ms != 0)
                      {
                          // The instruction shall be ignored if initialisationState is equal to DISABLED.
                          if(daliStateFlags.initialisationStateDisabled != 1)
                          {
                              searchAddressM = command;
                          }
                      }
                      break;
                  case SEARCHADDRL:      // SEARCHADDRL (data)
                      // Modified according to IEC 62386-102 (ed2.0)
                      if (initialiseTimeout_100ms != 0)
                      {
                          // The instruction shall be ignored if initialisationState is equal to DISABLED.
                          if(daliStateFlags.initialisationStateDisabled != 1)
                          {
                              searchAddressL = command;
                          }
                      }
                      break;
                  case PROGRAM_SHORT_ADDRESS:      // PROGRAM SHORT ADDRESS
                      // Modified according to IEC 62386-102 (ed2.0)
                      if (initialiseTimeout_100ms != 0)
                      {
                          // We should store this short address if physical
                          // selection has been enabled and we are being
                          // physically selected or if the random address
                          // equals the search address
                          if(((daliStateFlags.initialisationStateEnabled == 1) || (daliStateFlags.initialisationStateWithdrawn == 1)) && 
                              (randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL))
                          {
                              // If data = MASK: MASK (effectively deleting the short address)
                              if(command == MASK)
                              {
                                  idali_setShortAddress(command);
                              }
                              // If data = 1xxxxxxxb or xxxxxxx0b: no change
//                              else if(((command & 0x80) == 0x80) || ((command | 0xFE) == 0xFE))
//                              {
//                                  // no change
//                              }
                              // In all other cases (0AAAAAA1b): 00AAAAAAb
                              else if(((command | 0x7F) == 0x7F) && ((command & 0x01) == 0x01))
                              {
                                  
                                  idali_setShortAddress(command);
                              }
                          }
                      }
                      break;
                  case VERIFY_SHORT_ADDRESS:      // VERIFY SHORT ADDRESS
                      // Modified according to IEC 62386-102 (ed2.0)
                      if (initialiseTimeout_100ms != 0)
                      {
                          if(daliStateFlags.initialisationStateDisabled == 0)
                          {
                              if ((command) == ((shortAddress << 1) + 1))
                              {
                                  idali_sendBackwardFrame(0xFF);
                              }
                          }
                      }
                      break;
                  case QUERY_SHORT_ADDRESS:      // QUERY SHORT ADDRESS
                      // Modified according to IEC 62386-102 (ed2.0)
                      if (initialiseTimeout_100ms != 0)
                      {
                          if ((daliStateFlags.initialisationStateDisabled == 0) && ((randomAddressH == searchAddressH) && (randomAddressM == searchAddressM) && (randomAddressL == searchAddressL)))
                          {
                              idali_sendBackwardFrame(((shortAddress << 1) + 1));
                          }
                      }
                      break;
                  case ENABLE_DEVICE_TYPE:      // ENABLE DEVICE TYPE X
#ifdef DALI_USE_DEVICE_TYPE_6
                      if (command == 6)
                      {
                          daliStateFlags.enabledDeviceType = 1;
                      }
#endif /* DALI_USE_DEVICE_TYPE_6 */
                      break;

                  case DTR1_DATA:      // DTR1 (data)
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                      // writeEnabled should be set to 1 according to 
                      // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
                      if (daliStateFlags.writeEnabledOld == 1)
                      {
                          // Keep write enabled state. Set the "new" value to 1
                          daliStateFlags.writeEnabled = 1;
                      }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                      // Load value into the DTR1
                      DTR1 = command;
                      break;
                  case DTR2_DATA:      // DTR2 (data)
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                      // writeEnabled should be set to 1 according to 
                      // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
                      if (daliStateFlags.writeEnabledOld == 1)
                      {
                          // Keep write enabled state. Set the "new" value to 1
                          daliStateFlags.writeEnabled = 1;
                      }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                      // Load value into the DTR2
                      DTR2 = command;
                      break;
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                  case WRITE_MEMORY_LOCATION:      // WRITE MEMORY LOCATION (DTR1, DTR0, data)
                      // See IEC 62386-102, 11.4.4 (page 38).
                      // We now check the previous state of the write enabled
                      // flag, since at the beginning of this function we've reset
                      // the "new" value to 0.
                      if (daliStateFlags.writeEnabledOld == 1)
                      {
                          // Keep write enabled state. Set the "new" value to 1
                          daliStateFlags.writeEnabled = 1;
                          
                          // Writing to bank 0 is not allowed
                          if (DTR1 > 0)
                          {
                              // Determine if the (supposed) memory location is
                              // locked.
                              if ((DTR == 0x02) ||                    // This location is the lock byte
                                  ((DTR > 2) && (nvmem_daliBankMemoryRead(DTR1, 0x02) == 0x0055)))  // This memory is unlocked
                              {
                                  // Try to write memory. If the address is out of
                                  // range we'll know that from the return value.
                                  data = nvmem_daliBankMemoryWrite(DTR1, DTR, command);
                                  
                                  if (data != BANK_MEMORY_WRITE_CANNOT_WRITE)
                                  {
                                      // Write succeeded. Return the value which was written
                                      idali_sendBackwardFrame(command);

                                      if (data == BANK_MEMORY_WRITE_OK_LAST_LOC)
                                      {
                                          // This is the last accessible location
                                          // in the bank. We should thus disable
                                          // the write state.
                                          daliStateFlags.writeEnabled = 0;
                                      }
                                  }
                              }
                          }
                          if(DTR < 0xFF && (0 < DTR1 <= (nvmem_daliBankMemoryRead(MEM_BANK0, MEM_BANK0_LAST_ACCESSIBLE_BANK_ADDR))))
                          {
                              DTR++;
                          }
                      }
                      break;
                  case WRITE_MEMORY_LOCATION_NO_REPLY:      // WRITE MEMORY LOCATION - NO REPLY (DTR1, DTR0, data)
                      // This command is added in IEC 62386-102 (ed2.0) 
                      // This instruction is identical to the WRITE MEMORY LOCATION (DTR1, DTR0, data)
                      // command except that the receiving control gear shall not reply to the command.
                      // We now check the previous state of the write enabled
                      // flag, since at the beginning of this function we've reset
                      // the "new" value to 0.
                      if (daliStateFlags.writeEnabledOld == 1)
                      {
                          // Keep write enabled state. Set the "new" value to 1
                          daliStateFlags.writeEnabled = 1;
                          
                          // Writing to bank 0 is not allowed
                          if (DTR1 > 0)
                          {
                              // Determine if the (supposed) memory location is
                              // locked.
                              if ((DTR == 0x02) ||                    // This location is the lock byte
                                  ((DTR > 2) && (nvmem_daliBankMemoryRead(DTR1, 0x02) == 0x0055)))  // This memory is unlocked
                              {
                                  // Try to write memory. If the address is out of
                                  // range we'll know that from the return value.
                                  data = nvmem_daliBankMemoryWrite(DTR1, DTR, command);
  //                                if(DTR < 0xFF)
  //                                {
  //                                    DTR++;
  //                                }
                                  if (data != BANK_MEMORY_WRITE_CANNOT_WRITE)
                                  {
                                      // Write succeeded. Do not reply

                                      if (data == BANK_MEMORY_WRITE_OK_LAST_LOC)
                                      {
                                          // This is the last accessible location
                                          // in the bank. We should thus disable
                                          // the write state.
                                          daliStateFlags.writeEnabled = 0;
                                      }
                                  }
                              }
                          }
                              if(DTR < 0xFF && (0 < DTR1 <= (nvmem_daliBankMemoryRead(MEM_BANK0, MEM_BANK0_LAST_ACCESSIBLE_BANK_ADDR))))
                              {
                                  DTR++;
                              }
                      }
                      break;
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
              }
          }
          else if (f.individualAddress == 1)
          {
              // These commands are sent by address. This could be a broadcast
              // address, broadcast unaddressed, an individual address or a 
              // group which we belong to.
              if ((address & 0x01) == 0x00)
              {
                  // DIRECT ARC POWER CONTROL
                  // See IEC 62386-102, 11.1.1 (page 27).
                  idali_setArcPower(command, FADE_FADE_TIME);
              }
              else
              {
                  // This first switch handles commands that span ranges, such as
                  // YAAA AAA1 0001 XXXX -- GO TO SCENE XXXX. If the command in
                  // question is not one of these we'll fall through the "default"
                  // case of the switch statement and there we have a new switch
                  // with the individual commands that are left.

                  // Just in case the command includes a parameter, obtain it now
                  // such that it won't need to be recomputed for every case
                  parameter = command & 0x0F;

                  switch (command & 0xF0)
                  {
                      case GO_TO_SCENE:  // GO TO SCENE
                          // Check if we belong to the scene. This check is being
                          // made since idali_setArcPower(255) using FADE_FADE_TIME
                          // would mean "stop fading" and this may not be what we
                          // are trying to do.
                          // The powerCycleSeen bit should be set to FALSE once 
                          // GO TO SCENE command has been received. This is done 
                          // inside idali_setArcPower(scenes[parameter]).
                          if (scenes[parameter] != 255)
                          {
                              idali_setArcPower(scenes[parameter], FADE_FADE_TIME);
                          }
                          else
                          {
                              // The powerCycleSeen bit should be set to FALSE once 
                              // GO TO SCENE command has been received.
                              status.powerCycleSeen = 0;
                          }
                          break;
                      case SET_SCENE:  // SET SCENE (DTR0, sceneX)
                          // According to IEC 62386-102 (ed2.0) sceneNumber = opcode ? 0x40
                          // This is taken care of when the parameter is extracted
                          if (f.commandRepeated == 1)
                          {
                              if (DTR != scenes[parameter])
                              {
                                  scenes[parameter] = DTR;
                                  if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + parameter) != DTR)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + parameter, DTR);
                                  }
                              }
                          }
                          else
                          {
                              commandTimeout_ms = 100;
                          }
                          break;
                      case REMOVE_FROM_SCENE:  // REMOVE FROM SCENE
                          if (f.commandRepeated == 1)
                          {
                              if (scenes[parameter] != 255)
                              {
                                  scenes[parameter] = MASK;
                                  if (nvmem_daliReadByte(NVMEMORY_SCENES_BASE + parameter) != 255)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_SCENES_BASE + parameter, 255);
                                  }
                              }
                          }
                          else
                          {
                              commandTimeout_ms = 100;
                          }
                          break;
                      case ADD_TO_GROUP:  // ADD TO GROUP
                          if (f.commandRepeated == 1)
                          {
                              // Set the bit corresponding the the "parameter"
                              // group in groupHigh:groupLow
                              if (parameter < 8)
                              {
                                  groupLow |= groupMask[parameter];
                                  if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != groupLow)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, groupLow);
                                  }
                              }
                              else
                              {
                                  groupHigh |= groupMask[parameter - 8];
                                  if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != groupHigh)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, groupHigh);
                                  }
                              }
                          }
                          else
                          {
                              commandTimeout_ms = 100;
                          }
                          break;
                      case REMOVE_FROM_GROUP:  // REMOVE FROM GROUP
                          if (f.commandRepeated == 1)
                          {
                              // Clear the bit corresponding the the "parameter"
                              // group in groupHigh:groupLow
                              if (parameter < 8)
                              {
                                  groupLow &= ~groupMask[parameter];
                                  if (nvmem_daliReadByte(NVMEMORY_GROUP_LOW) != groupLow)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_GROUP_LOW, groupLow);
                                  }
                              }
                              else
                              {
                                  groupHigh &= ~groupMask[parameter - 8];
                                  if (nvmem_daliReadByte(NVMEMORY_GROUP_HIGH) != groupHigh)
                                  {
                                      nvmem_daliWriteByte(NVMEMORY_GROUP_HIGH, groupHigh);
                                  }
                              }
                          }
                          else
                          {
                              commandTimeout_ms = 100;
                          }
                          break;
                      case QUERY_SCENE_LEVEL:  // QUERY SCENE LEVEL (SCENES 0 - 15)
                          // Reply with the value stored for scene number
                          // "parameter"
                          idali_sendBackwardFrame(scenes[parameter]);
                          break;
                      default:
                          // This is not a command that spans ranges. We now need
                          // to check every command and act accordingly.
                          switch (command)
                          {
                              case OFF:  // OFF
                                  // See IEC 62386-102 (ed2.0), 11.3.2
                                  // (page 29).
                                  dapcTimeout_ms = 0;
                                  idali_setArcPower(0, FADE_NONE);
                                  break;
                              case UP:  // UP
                                  dapcTimeout_ms = 0;
                                  if  ((actualArcPower > 0) && (actualArcPower != maxLevel))
                                  {
                                      // Do not allow "UP" to turn lamp on
                                      idali_setArcPower(1, FADE_FADE_RATE);
                                  }
                                  else if(actualArcPower == maxLevel)
                                  {
  //                                  // The level will be set to maxLevel in
                                      // idali_setArcPower() since level being
                                      // sent is greater than maxLevel. This needs 
                                      // done to ensure that there is a reaction
                                      // to this command
                                      idali_setArcPower((actualArcPower + 1), FADE_NONE);
                                      
                                  }
                                  break;
                              case DOWN:  // DOWN
                                  dapcTimeout_ms = 0;
                                  if  ((actualArcPower > minLevel) && (actualArcPower != 0))
                                  {
                                      // Do not allow "DOWN" to turn lamp off
                                      idali_setArcPower(0, FADE_FADE_RATE);
                                  }
                                  else
                                  {
                                      if((actualArcPower - 1) != 0)
                                      {
                                          // Do not allow "DOWN" to turn lamp off
                                          // The level will be set to minLevel in
                                          // idali_setArcPower() since level being
                                          // sent is less than minLevel. This needs 
                                          // done to ensure that there is a reaction
                                          // to this command
                                          idali_setArcPower((actualArcPower - 1), FADE_NONE);
                                      }
                                  }
                                  break;
                              case STEP_UP:  // STEP UP
  //                                // This command is modified according to IEC 62386-102 (ed2.0) 11.3.5
                                  dapcTimeout_ms = 0; 
                                  if(actualArcPower == 0)
                                  {
                                      idali_setArcPower(0,FADE_NONE);
                                  }
                                  else if (minLevel <= actualArcPower < maxLevel)
                                  {
                                      // Do not allow "STEP UP" to turn lamp on
                                      if (actualArcPower != 0)
                                      {
                                          idali_setArcPower(actualArcPower + 1, FADE_NONE);
                                      }
                                  }
                                  else if(actualArcPower == maxLevel)
                                  {
                                      idali_setArcPower(maxLevel, FADE_NONE);
                                  }
                                  break;
                              case STEP_DOWN:  // STEP DOWN
  //                                // This command is modified according to IEC 62386-102 (ed2.0) 11.3.6
                                  dapcTimeout_ms = 0;
                                  if(actualArcPower == 0)
                                  {
                                      idali_setArcPower(0, FADE_NONE);
                                  }
                                  else if (minLevel < actualArcPower <= maxLevel)
                                  {
                                      if (actualArcPower > 1)
                                      {
                                          // Do not allow "STEP DOWN" to turn lamp off
                                          idali_setArcPower(actualArcPower - 1, FADE_NONE);
                                      }
                                  }
                                  else if(actualArcPower == minLevel)
                                  {
                                       idali_setArcPower(minLevel, FADE_NONE);
                                  }
                                  break;
                              case RECALL_MAX_LEVEL:  // RECALL MAX LEVEL
  //                                // This command is modified according to IEC 62386-102 (ed2.0) 11.3.7
                                  dapcTimeout_ms = 0;
                                  if(daliStateFlags.initialisationStateDisabled == 1)
                                  {
                                      idali_setArcPower(maxLevel, FADE_NONE);
                                  }
                                  else if(daliStateFlags.initialisationStateDisabled != 1)
                                  {
                                      // While initialisationState is not DISABLED, 
                                      // the control gear shall set actualLevel and 
                                      // targetLevel to maxLevel, and then adjust
                                      // the light output as quickly as possible to 
                                      // 100 % (this is achieved by idali_startIdentificationProcedure())
                                      // IEC 62386-102 (ed2.0) 9.14.3.3
                                      idali_setArcPower(maxLevel, FADE_NONE);
  //                                    idali_startIdentificationProcedure();
                                  }
                                  break;
                              case RECALL_MIN_LEVEL:  // RECALL MIN LEVEL
                                  dapcTimeout_ms = 0;
                                  if(daliStateFlags.initialisationStateDisabled == 1)
                                  {
                                      idali_setArcPower(minLevel, FADE_NONE);
                                  }
                                  else if(daliStateFlags.initialisationStateDisabled != 1)
                                  {
                                      idali_setArcPower(physicalMinimumLevel, FADE_NONE);
  //                                    idali_startIdentificationProcedure();
                                  }
                                  break;
                              case STEP_DOWN_AND_OFF:  // STEP DOWN AND OFF
                                  dapcTimeout_ms = 0;
                                  if (actualArcPower == 0)
                                  {
                                      idali_setArcPower(0, FADE_NONE);
                                  }
                                  else if ((actualArcPower > minLevel) && (actualArcPower <= maxLevel))
                                  {
                                      idali_setArcPower(actualArcPower - 1, FADE_NONE);
                                  }
                                  else if(actualArcPower == minLevel)
                                  {
                                      idali_setArcPower(0, FADE_NONE);
                                  }
                                  break;
                              case ON_AND_STEP_UP:  // ON AND STEP UP
                                  dapcTimeout_ms = 0;
                                  if (actualArcPower == 0)
                                  {
                                      idali_setArcPower(minLevel, FADE_NONE);
                                  }
                                  else if(minLevel <= actualArcPower < maxLevel)
                                  {
                                      idali_setArcPower(actualArcPower + 1, FADE_NONE);
                                  }
                                  else if(actualArcPower >= maxLevel)
                                  {
                                      idali_setArcPower(maxLevel, FADE_NONE);
                                  }
                                  break;
                              case ENABLE_DAPC_SEQUENCE:  // ENABLE DAPC SEQUENCE
                                  dapcTimeout_ms = 200;
                                  break;
                              case GO_TO_LAST_ACTIVE_LEVEL:  // GO TO LAST ACTIVE LEVEL
                                  idali_setArcPower(lastActiveLevel, FADE_FADE_TIME);  
                                  break;
                              case DALI_RESET:  // RESET
                                  if (f.commandRepeated == 1)
                                  {
                                      // Perform a DALI reset
                                      idali_reset();
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case STORE_ACTUAL_LEVEL_IN_DTR0:  // STORE ACTUAL LEVEL IN THE DTR0
                                  if (f.commandRepeated == 1)
                                  {
                                      DTR = actualArcPower;
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SAVE_PERSISTENT_VARIABLES:  // SAVE PERSISTENT VARIABLES
                                  if (f.commandRepeated == 1)
                                  {
                                      // Reception of SAVE PERSISTENT VARIABLES
                                      // command shall stop a running fade
                                      // IEC 62386-102 (ed2.0) 9.5.9 page 30 
                                      idali_setArcPower(255, FADE_NONE);
                                      
                                      // Physically store all variables 
                                      // identified in IEC 62386-102 (ed2.0) 
                                      // Table 14 (page 49) as non-volatile
                                      // memory (NVM).
                                      idali_savePersistentVariables();
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_OPERATING_MODE_DTR0:  // SET OPERATING MODE (DTR0) 
                                  if (f.commandRepeated == 1)
                                  {
                                      if(DTR == nvmem_daliReadByte(NVMEMORY_OPERATING_MODE))
                                      {
                                          opMode = DTR;
                                      }
                                      if(nvmem_daliReadByte(NVMEMORY_OPERATING_MODE) != opMode)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_OPERATING_MODE, opMode); 
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case RESET_MEMORY_BANK_DTR0:  // RESET MEMORY BANK (DTR0) 
                                  if (f.commandRepeated == 1)
                                  {
                                      if(DTR == 0)
                                      {
                                        #ifdef DALI_USE_EXTRA_MEMORY_BANKS
                                          resetMemoryBank1();
                                          resetMemoryBank2();
                                        #endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                                      }
                                      else
                                      {
                                          switch(DTR)
                                          {
                                              #ifdef DALI_USE_EXTRA_MEMORY_BANKS
                                              case MEM_BANK1:
                                                  resetMemoryBank1();
                                                  break;
                                              case MEM_BANK2:
                                                  resetMemoryBank2();
                                                  break;
                                              #endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                                          }
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case IDENTIFY_DEVICE: // IDENTIFY DEVICE
                                  //ToDo Write the command sequence
                                  if (f.commandRepeated == 1)
                                  {
                                      // Reception of IDENTIFY DEVICE
                                      // command shall stop a running fade
                                      // IEC 62386-102 (ed2.0) 9.5.9 page 30 
                                      fadeCount = 0;
                                      
                                      // Command received twice within 100ms, so we need to
                                      // start the 10 s timer 
                                      identifyDeviceTimeout_100ms = 10 * 10;
                                      
                                      idali_startIdentificationProcedure();
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }  // IDENTIFY DEVICE 
                                  break;
                              case SET_MAX_LEVEL_DTR0:  // SET MAX LEVEL (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      fadeCount = 0;
                                      // Make sure the level we set is a legal one
                                      if (DTR < minLevel)
                                      {
                                          maxLevel = minLevel;
                                      }
                                      else if (DTR == MASK)
                                      {
                                          maxLevel = 0xFE;
                                      }
                                      else
                                      {
                                          maxLevel = DTR;
                                      }

                                      // If it changed, update the non-volatile
                                      // memory as well
                                      if (nvmem_daliReadByte(NVMEMORY_MAXIMUM_LEVEL) != maxLevel)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_MAXIMUM_LEVEL, maxLevel);
                                      }

                                      // See IEC 62386-102, 11.2.2, (page 31)
                                      if (actualArcPower > maxLevel)
                                      {
                                          daliStateFlags.maxLevelChanged = 1;
                                          idali_setArcPower(maxLevel, FADE_NONE);
                                      }
                                      else
                                      {
                                          idali_setArcPower(actualArcPower, FADE_NONE);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_MIN_LEVEL_DTR0:  // SET MIN LEVEL (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      fadeCount = 0;
                                      // Make sure the level we set is a legal one
                                      if (DTR <= physicalMinimumLevel)
                                      {
                                          minLevel = physicalMinimumLevel;
                                      }
                                      else if ((DTR >= maxLevel) || (DTR == MASK))
                                      {
                                          minLevel = maxLevel;
                                      }
                                      else
                                      {
                                          minLevel = DTR;
                                      }

                                      // If it changed, update the non-volatile
                                      // memory as well
                                      if (nvmem_daliReadByte(NVMEMORY_MINIMUM_LEVEL) != minLevel)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_MINIMUM_LEVEL, minLevel);
                                      }

                                      // See IEC 62386-102, 11.2.2, (page 31)
                                      if ((actualArcPower < minLevel) && (actualArcPower > 0))
                                      {
                                          daliStateFlags.minLevelChanged = 1;
                                          idali_setArcPower(minLevel, FADE_NONE);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_SYSTEM_FAILURE_LEVEL_DTR0:  // SET SYSTEM FAILURE LEVEL (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      if (systemFailureLevel != DTR)
                                      {
                                          systemFailureLevel = DTR;
                                          nvmem_daliWriteByte(NVMEMORY_SYSTEM_FAILURE_LEVEL, DTR);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_POWER_ON_LEVEL_DTR0:  // SET POWER ON LEVEL (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      if (powerOnLevel != DTR)
                                      {
                                          powerOnLevel = DTR;
                                          nvmem_daliWriteByte(NVMEMORY_POWER_ON_LEVEL, DTR);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_FADE_TIME_DTR0:  // SET FADE TIME (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      // Make sure we store a valid value
                                      if (DTR < 16)
                                      {
                                          fadeTime = DTR;
                                      }
                                      else
                                      {
                                          fadeTime = 15;
                                      }

                                      if (nvmem_daliReadByte(NVMEMORY_FADE_TIME) != fadeTime)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_FADE_TIME, fadeTime);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_FADE_RATE_DTR0:  // SET FADE RATE (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      // Make sure we store a valid value
                                      if (DTR == 0)
                                      {
                                          fadeRate = 1;
                                      }
                                      else if (DTR > 15)
                                      {
                                          fadeRate = 15;
                                      }
                                      else
                                      {
                                          fadeRate = DTR;
                                      }

                                      if (nvmem_daliReadByte(NVMEMORY_FADE_RATE) != fadeRate)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_FADE_RATE, fadeRate);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_EXTENDED_FADE_TIME_DTR0:  // SET EXTENDED FADE TIME (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      // Make sure we store a valid value
                                      if (DTR > 0x4F)
                                      {
                                          extendedFadeTimeBase = 0;
                                          extendedFadeTimeMultiplier = 0;
                                      }
                                      else
                                      {
                                          extendedFadeTimeBase = (DTR & 0x0F);
                                          extendedFadeTimeMultiplier = ((DTR & 0x70) >> 4);
                                      }
                                      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_BASE) != extendedFadeTimeBase)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_BASE, extendedFadeTimeBase);
                                      }
                                      if (nvmem_daliReadByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER) != extendedFadeTimeMultiplier)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_EXTENDED_FADE_TIME_MULTIPLIER, extendedFadeTimeMultiplier);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case SET_SHORT_ADDRESS_DTR0:  // SET SHORT ADDRESS (DTR0)
                                  if (f.commandRepeated == 1)
                                  {
                                      // Let the function check the validity of
                                      // the value stored in DTR
                                      
                                      // If data = MASK: MASK (effectively deleting 
                                      // the short address)
                                      if(DTR == MASK)
                                      {
                                          idali_setShortAddress(DTR);
                                      }
  //                                    // If data = 1xxxxxxxb or xxxxxxx0b: no change
  //                                    else if(((DTR & 0b10000000) == 0b10000000) || ((DTR | 0b11111110) == 0b11111110))
  //                                    {
  //                                        // no change
  //                                    }
                                      // In all other cases (0AAAAAA1b): 00AAAAAAb
                                      else if((DTR & 0x81) == 0x01)
  //                                    else if(((DTR | 0b01111111) == 0b01111111) && ((DTR & 0b00000001) == 0b00000001))
                                      {
                                          
                                          idali_setShortAddress(DTR);
                                      }
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                              case ENABLE_WRITE_MEMORY:  // ENABLE WRITE MEMORY
                                  if (f.commandRepeated == 1)
                                  {
                                      // The next command will have the memory
                                      // writing enabled
                                      daliStateFlags.writeEnabled = 1;
                                  }
                                  else
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                              case QUERY_STATUS:  // QUERY STATUS
                                  idali_sendBackwardFrame(status.All);
                                  break;
                              case QUERY_CONTROL_GEAR_PRESENT:  // QUERY CONTROL GEAR PRESENT
                                  idali_sendBackwardFrame(0xFF);
                                  break;
                              case QUERY_LAMP_FAILURE:  // QUERY LAMP FAILURE
                                  if (status.lampFailure != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_LAMP_POWER_ON:  // QUERY LAMP POWER ON
                              // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
                                  if (status.lampOn != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_LIMIT_ERROR:  // QUERY LIMIT ERROR
                                  if (status.limitError != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_RESET_STATE:  // QUERY RESET STATE
                                  if (status.resetState != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_MISSING_SHORT_ADDRESS:  // QUERY MISSING SHORT ADDRESS
                              // The missingShortAddress bit is renamed shortAddress according to IEC 62386-102 (ed2.0) Table 12 Page 45
                                  if (status.shortAddress != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_VERSION_NUMBER:  // QUERY VERSION NUMBER
                                  data = nvmem_daliBankMemoryRead(MEM_BANK0, MEM_BANK0_CG_102_VERSION_NUM_ADDR);
                                  if ((data & 0xFF00) == 0)
                                  {
                                      // Read OK, no error bits set
                                      idali_sendBackwardFrame(data & 0xFF);
                                  }
                                  break;
                              case QUERY_CONTENT_DTR0:  // QUERY CONTENT DTR0
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                                  // writeEnabled should be set to 1 according to 
                                  // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
  //                                daliStateFlags.writeEnabled = 1;
                                  if (daliStateFlags.writeEnabledOld == 1)
                                  {
                                      // Keep write enabled state. Set the "new" value to 1
                                      daliStateFlags.writeEnabled = 1;
                                  }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */                               
                                  idali_sendBackwardFrame(DTR);
                                  break;
                              case QUERY_DEVICE_TYPE:  // QUERY DEVICE TYPE
                                  // The command QUERY NEXT DEVICE TYPE checks for this flag to check whether  
                                  // the QUERY DEVICE TYPE command has been previously received
                                  f.previousCommand = 1; 
                                  idali_sendBackwardFrame(DEVICE_TYPE);
                                  break;
                              case QUERY_PHYSICAL_MINIMUM:  // QUERY PHYSICAL MINIMUM LEVEL
                                  idali_sendBackwardFrame(physicalMinimumLevel);
                                  break;
                              case QUERY_POWER_FAILURE:  // QUERY POWER FAILURE
                              // The powerFailure bit is renamed powerCycleSeen according to IEC 62386-102 (ed2.0) Table 12 Page 45
                                  if (status.powerCycleSeen != 0)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_CONTENT_DTR1:  // QUERY CONTENT DTR1
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                                  // writeEnabled should be set to 1 according to 
                                  // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
  //                                daliStateFlags.writeEnabled = 1;
                                  if (daliStateFlags.writeEnabledOld == 1)
                                  {
                                      // Keep write enabled state. Set the "new" value to 1
                                      daliStateFlags.writeEnabled = 1;
                                  }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */
                                  idali_sendBackwardFrame(DTR1);
                                  break;
                              case QUERY_CONTENT_DTR2:  // QUERY CONTENT DTR2
#ifdef DALI_USE_EXTRA_MEMORY_BANKS
                                  // writeEnabled should be set to 1 according to 
                                  // 9.10.5 Memory bank writing IEC62368 - 102 (ed2.0)
  //                                daliStateFlags.writeEnabled = 1;
                                  if (daliStateFlags.writeEnabledOld == 1)
                                  {
                                      // Keep write enabled state. Set the "new" value to 1
                                      daliStateFlags.writeEnabled = 1;
                                  }
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */                                
                                  idali_sendBackwardFrame(DTR2);
                                  break;
                              case QUERY_OPERATING_MODE:  // QUERY OPERATING MODE
                                  idali_sendBackwardFrame(opMode);
                                  break;
                              case QUERY_LIGHT_SOURCE_TYPE:  // QUERY LIGHT SOURCE TYPE
                                  idali_sendBackwardFrame(LIGHT_SOURCE_TYPE);
                                  break;
                              case QUERY_ACTUAL_LEVEL:  // QUERY ACTUAL LEVEL
#ifdef DALI_USE_DEVICE_TYPE_6
                                  if ((status.lampFailure == 1) || (failureStatus.thermalOverload == 1) || (failureStatus.thermalShutDown == 1))
#else
                                  if (status.lampFailure == 1)
#endif /* DALI_USE_DEVICE_TYPE_6 */
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
  //                                // Added according to IEC 62386-102, 11.5.20, (page 67)
  //                                // The lampArcPowerOn bit is renamed lampOn according to IEC 62386-102 (ed2.0) Table 12 Page 45
                                  else if(actualArcPower == 0)
                                  {
                                      idali_sendBackwardFrame(0x00);
                                  }
                                  else
                                  {
                                      idali_sendBackwardFrame(actualArcPower);
                                  }
                                  break;
                              case QUERY_MAX_LEVEL:  // QUERY MAX LEVEL
                                  idali_sendBackwardFrame(maxLevel);
                                  break;
                              case QUERY_MIN_LEVEL:  // QUERY MIN LEVEL
                                  idali_sendBackwardFrame(minLevel);
                                  break;
                              case QUERY_POWER_ON_LEVEL:  // QUERY POWER ON LEVEL
                                  idali_sendBackwardFrame(powerOnLevel);
                                  break;
                              case QUERY_SYSTEM_FAILURE_LEVEL:  // QUERY SYSTEM FAILURE LEVEL
                                  idali_sendBackwardFrame(systemFailureLevel);
                                  break;
                              case QUERY_FADE_TIME_FADE_RATE:  // QUERY FADE TIME/FADE RATE
                                  idali_sendBackwardFrame((fadeTime << 4) | fadeRate);
                                  break;
                              case QUERY_MANUFACTURER_SPECIFIC_MODE:  // QUERY MANUFACTURER SPECIFIC MODE
                                  // Added according to IEC 62386-102, 11.5.27, (page 68)
                                  // Comparision fails when the if condition is written as f((0x80 <= opMode <= 0xFF) 
                                  if(0x80 <= opMode)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_NEXT_DEVICE_TYPE:  // QUERY NEXT DEVICE TYPE
                                  // Added according to IEC 62386-102 (ed2.0), 11.5.13, (page 66)
                                  if(f.previousCommand == 1)
                                  {
                                      f.previousCommand = 0;
                                      // If directly preceded by QUERY DEVICE TYPE, and more than one device type/feature 
                                      // is supported: the first and lowest device type/feature number should be sent.
                                      // Since currently DEVICE_TYPE 6 is supported NO REPLY is required. 
                                      if (f.commandRepeated == 1)
                                      {
                                          // If directly preceded by QUERY NEXT DEVICE TYPE, and not all device types have
                                          // been reported: the next lowest device type/feature number should be sent.
                                          // Since currently DEVICE_TYPE 6 is supported NO REPLY is required. 
                                          
                                          // If directly preceded by QUERY NEXT DEVICE TYPE, and all device types have been
                                          // reported: 254
                                          // Since currently DEVICE_TYPE 6 is supported, the device type is already reported 
                                          // during the GET DEVICE TYPE command.
                                          idali_sendBackwardFrame(254);
                                      }
                                  }
                                  break;
                              case QUERY_EXTENDED_FADE_TIME:  // QUERY EXTENDED FADE TIME
                                  // Added according to IEC 62386-102 (ed2.0), 11.5.26, (Page 68)
                                  idali_sendBackwardFrame((extendedFadeTimeMultiplier << 4) | extendedFadeTimeBase);
                                  break;
                              case QUERY_CONTROL_GEAR_FAILURE:  // QUERY CONTROL GEAR FAILURE
                                  if(status.controlGearFailure == 1)
                                  {
                                      idali_sendBackwardFrame(0xFF);
                                  }
                                  break;
                              case QUERY_GROUPS_0_7:  // QUERY GROUPS 0-7
                                  idali_sendBackwardFrame(groupLow);
                                  break;
                              case QUERY_GROUPS_8_15:  // QUERY GROUPS 8-15
                                  idali_sendBackwardFrame(groupHigh);
                                  break;
                              case QUERY_RANDOM_ADDRESS_H:  // QUERY RANDOM ADDRESS (H)
                                  idali_sendBackwardFrame(randomAddressH);
                                  break;
                              case QUERY_RANDOM_ADDRESS_M:  // QUERY RANDOM ADDRESS (M)
                                  idali_sendBackwardFrame(randomAddressM);
                                  break;
                              case QUERY_RANDOM_ADDRESS_L:  // QUERY RANDOM ADDRESS (L)
                                  idali_sendBackwardFrame(randomAddressL);
                                  break;
                              case READ_MEMORY_LOCATION:  // READ MEMORY LOCATION (DTR1, DTR0)
                                  // DTR1 == bank
                                  // DTR == location
                                  data = nvmem_daliBankMemoryRead(DTR1, DTR);
                                  if ((data & 0xFF00) == 0)
                                  {
                                      // Read OK, no error bits set
                                      idali_sendBackwardFrame(data & 0xFF);
                                  }
                                  if(DTR < 0xFF && (DTR1 <= (nvmem_daliBankMemoryRead(MEM_BANK0, MEM_BANK0_LAST_ACCESSIBLE_BANK_ADDR))))
                                  {
                                      DTR++;
                                  }
                                  break;
#ifdef DALI_USE_DEVICE_TYPE_6
                              case REFERENCE_SYSTEM_POWER:  // REFERENCE SYSTEM POWER
  //                                if (f.commandRepeated == 1)
  //                                {
  //                                    // (Mis)use the 8bit variable "parameter" to
  //                                    // store the bits from the non-volatile
  //                                    // memory temporarily in order to flip some
  //                                    // bits (if necessary)
  //                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
  //                                    if (failureStatus.currentProtectorActive == 1)
  //                                    {
  //                                        failureStatus.referenceMeasurementFailed = 1;
  //                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_REF_FAIL) == 0)
  //                                        {
  //                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_REF_FAIL);
  //                                        }
  //                                    }
  //                                    else
  //                                    {
  //                                        failureStatus.referenceMeasurementFailed = 0;
  //                                        if ((parameter & NVMEMORY_MULTIPLE_BITS_REF_FAIL) != 0)
  //                                        {
  //                                            nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_REF_FAIL));
  //                                        }
  //                                        idali_startReferenceSystemPower();
  //                                    }
  //                                }
  //                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    commandTimeout_ms = 100;
  //                                }
                                  break;
                              case ENABLE_CURRENT_PROTECTOR:  // ENABLE CURRENT PROTECTOR
  //                                if (f.commandRepeated == 1)
  //                                {
  //                                    daliLedStateFlags.currentProtectorEnabled = 1;
  //                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
  //                                    if ((parameter & NVMEMORY_MULTIPLE_BITS_CP_EN) == 0)
  //                                    {
  //                                        nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_CP_EN);
  //                                    }
  //                                }
  //                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    commandTimeout_ms = 100;
  //                                }
                                  break;
                              case DISABLE_CURRENT_PROTECTOR:  // DISABLE CURRENT PROTECTOR
  //                                if (f.commandRepeated == 1)
  //                                {
  //                                    daliLedStateFlags.currentProtectorEnabled = 0;
  //                                    parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
  //                                    if ((parameter & NVMEMORY_MULTIPLE_BITS_CP_EN) != 0)
  //                                    {
  //                                        nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_CP_EN));
  //                                    }
  //                                }
  //                                else if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    commandTimeout_ms = 100;
  //                                }
                                  break;
                              case SELECT_DIMMING_CURVE:  // SELECT DIMMING CURVE
                                  if (f.commandRepeated == 1)
                                  {
                                      parameter = nvmem_daliReadByte(NVMEMORY_MULTIPLE_BITS);
                                      if (DTR == 0)
                                      {
                                          operatingMode.nonLogDimmingCurve = 0;
                                          if ((parameter & NVMEMORY_MULTIPLE_BITS_LIN_DIM) != 0)
                                          {
                                              nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter & (~NVMEMORY_MULTIPLE_BITS_LIN_DIM));
                                          }
                                          physicalMinimumLevel = PHYSICAL_MINIMUM_LEVEL;
                                      }
                                      else if (DTR == 1)
                                      {
                                          operatingMode.nonLogDimmingCurve = 1;
                                          if ((parameter & NVMEMORY_MULTIPLE_BITS_LIN_DIM) == 0)
                                          {
                                              nvmem_daliWriteByte(NVMEMORY_MULTIPLE_BITS, parameter | NVMEMORY_MULTIPLE_BITS_LIN_DIM);
                                          }
                                          physicalMinimumLevel = idali_getLampNonLogPhysicalMinimum();
                                      }
                                  }
                                  else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case STORE_DTR_AS_FAST_FADE_TIME:  // STORE DTR AS FAST FADE TIME
                                  if (f.commandRepeated == 1)
                                  {
                                      // Make sure we are using a valid value
                                      if ((DTR == 0) || ((DTR >= MIN_FAST_FADE_TIME) && (DTR <= 27)))
                                      {
                                          fastFadeTime = DTR;
                                      }
                                      else if ((DTR > 0) && (DTR < MIN_FAST_FADE_TIME))
                                      {
                                          fastFadeTime = MIN_FAST_FADE_TIME;
                                      }
                                      else if (DTR > 27)
                                      {
                                          fastFadeTime = 27;
                                      }

                                      if (nvmem_daliReadByte(NVMEMORY_FAST_FADE_TIME) != fastFadeTime)
                                      {
                                          nvmem_daliWriteByte(NVMEMORY_FAST_FADE_TIME, fastFadeTime);
                                      }
                                  }
                                  else if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      commandTimeout_ms = 100;
                                  }
                                  break;
                              case QUERY_GEAR_TYPE:  // QUERY GEAR TYPE
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(GEAR_TYPE);
                                  }
                                  break;
                              case QUERY_DIMMING_CURVE:  // QUERY DIMMING CURVE
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      if (operatingMode.nonLogDimmingCurve == 1)
                                      {
                                          idali_sendBackwardFrame(1);
                                      }
                                      else
                                      {
                                          idali_sendBackwardFrame(0);
                                      }
                                  }
                                  break;
                              case QUERY_POSSIBLE_OPERATING_MODES:  // QUERY POSSIBLE OPERATING MODES
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(POSSIBLE_OPERATING_MODES);
                                  }
                                  break;
                              case QUERY_FEATURES:  // QUERY FEATURES
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(FEATURES);
                                  }
                                  break;
                              case QUERY_FAILURE_STATUS:  // QUERY FAILURE STATUS
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(failureStatus.All);
                                  }
                                  break;
                              case QUERY_SHORT_CIRCUIT:  // QUERY SHORT CIRCUIT
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.shortCircuit == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_OPEN_CIRCUIT:  // QUERY OPEN CIRCUIT
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.openCircuit == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_LOAD_DECREASE:  // QUERY LOAD DECREASE
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.loadDecrease == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_LOAD_INCREASE:  // QUERY LOAD INCREASE
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.loadIncrease == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_CURRENT_PROTECTOR_ACTIVE:  // QUERY CURRENT PROTECTOR ACTIVE
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.currentProtectorActive == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_THERMAL_SHUT_DOWN:  // QUERY THERMAL SHUTDOWN
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.thermalShutDown == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_THERMAL_OVERLOAD:  // QUERY THERMAL OVERLOAD
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.thermalOverload == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_REFERENCE_RUNNING:  // QUERY REFERENCE RUNNING
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (idali_getStatusReferenceSystemPower() == REFERENCE_SYSTEM_POWER_RUNNING)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_REFERENCE_MEASUREMENT_FAILED:  // QUERY REFERENCE MEASUREMENT FAILED
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (failureStatus.referenceMeasurementFailed == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_CURRENT_PROTECTOR_ENABLED:  // QUERY CURRENT PROTECTOR ENABLED
  //                                if (daliStateFlags.enabledDeviceTypeOld == 1)
  //                                {
  //                                    if (daliLedStateFlags.currentProtectorEnabled == 1)
  //                                    {
  //                                        idali_sendBackwardFrame(0xFF);
  //                                    }
  //                                }
                                  break;
                              case QUERY_OPERATING_MODE_207:  // QUERY OPERATING MODE
                                  // Added according to IEC 62386-102 (ed2.0), 11.5.18, (Page 67)
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(operatingMode.All);
                                  }
                                  break;
                              case QUERY_FAST_FADE_TIME:  // QUERY FAST FADE TIME
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(fastFadeTime);
                                  }
                                  break;
                              case QUERY_MIN_FAST_FADE_TIME:  // QUERY MIN FAST FADE TIME
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(MIN_FAST_FADE_TIME);
                                  }
                                  break;
                              case QUERY_EXTENDED_VERSION_NUMBER:  // QUERY EXTENDED VERSION NUMBER
                                  if (daliStateFlags.enabledDeviceTypeOld == 1)
                                  {
                                      idali_sendBackwardFrame(1);
                                  }
                                  break;
#endif /* DALI_USE_DEVICE_TYPE_6 */
                          }
                          break;
                  }
              }
          }
      }
      else if (f.deviceAddressed == 1)
      {
          // This branch gets executed if this command is addressed to us and came
          // < 100ms after the first of a pair of commands that should be sent
          // twice within 100ms.
          // This means we should ignore this command and cancel the timer that
          // was started. See IEC 62386-102, 11.2.1 (page 30).
          commandTimeout_ms = 0;
      }

      if (f.deviceAddressed == 1)
      {
          // Store the last command in order to be able to check for commands
          // being sent twice.
          lastCommand = command;
          lastAddress = address;
      }
}

