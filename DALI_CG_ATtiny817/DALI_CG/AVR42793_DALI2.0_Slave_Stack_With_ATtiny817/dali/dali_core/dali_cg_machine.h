/**
 * \file
 *
 * \brief Process dali slave commands process
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */

#ifndef DALI_CG_MACHINE_H
#define DALI_CG_MACHINE_H

#include <stdint.h>
#include "dali_cg.h"


// Constants according to Terms and definitions IEC 62386-102 (ed2.0)
// See IEC 62386-102 (ed2.0), 3.11 (page 16).
#define MASK 0xFF


// The DALI machine needs to save its variables to non-volatile memory and will
// need this many bytes. The nvmem_daliWriteByte() and nvmem_daliReadByte()
// functions will access locations from 0 to DALI_MACHINE_NVMEMORY_REQUIRED - 1.
// How the application implements these non-volatile memory locations is
// transparent to the DALI state machine.
#ifdef DALI_USE_DEVICE_TYPE_6
#define DALI_MACHINE_NVMEMORY_REQUIRED          35
#else
#define DALI_MACHINE_NVMEMORY_REQUIRED          32
#endif /* DALI_USE_DEVICE_TYPE_6 */


// This block of NVMEMORY_DEFAULT_* symbols is used to tell the application
// layer the default values that should be placed in the
// DALI_MACHINE_NVMEMORY_REQUIRED non-volatile memory locations when programming
// the device, as these are the values the device needs to leave the factory
// with. This order should be preserved, as it corresponds to the NVMEMORY_*
// order defined in dali_cg_machine.c There should be no need to add such
// variables unless the standard changes, any non-volatile memory used by the
// application can be defined and used in dali_cg_nvmemory.c.
    
#define NVMEMORY_DEFAULT_POWER_ON_LEVEL                 0xFE
#define NVMEMORY_DEFAULT_SYSTEM_FAILURE_LEVEL           0xFE
#define NVMEMORY_DEFAULT_MINIMUM_LEVEL                  PHYSICAL_MINIMUM_LEVEL
#define NVMEMORY_DEFAULT_MAXIMUM_LEVEL                  0xFE
#define NVMEMORY_DEFAULT_FADE_RATE                      7
#define NVMEMORY_DEFAULT_FADE_TIME                      0
// The symbols NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_BASE, NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_MULTIPLIER
// and NVMEMORY_DEFAULT_OPERATING_MODE added according to IEC 62386-102 (ed2.0) Table 14
#define NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_BASE        0
#define NVMEMORY_DEFAULT_EXTENDED_FADE_TIME_MULTIPLIER  0
#define NVMEMORY_DEFAULT_OPERATING_MODE                 0
#define NVMEMORY_DEFAULT_SHORT_ADDRESS                  MASK
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_H               0xFF
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_M               0xFF
#define NVMEMORY_DEFAULT_RANDOM_ADDRESS_L               0xFF
#define NVMEMORY_DEFAULT_GROUP_LOW                      0
#define NVMEMORY_DEFAULT_GROUP_HIGH                     0
//// The symbol NVMEMORY_DEFAULT_LAST_LIGHT_LEVEL is added according to IEC 62386-102 (ed2.0) Table 14
//#define NVMEMORY_DEFAULT_LAST_LIGHT_LEVEL               0xFE
#define NVMEMORY_DEFAULT_SCENES                         255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
   
#ifdef DALI_USE_DEVICE_TYPE_6
// And LED specific ones
#define NVMEMORY_DEFAULT_FAST_FADE_TIME                 0
#define NVMEMORY_DEFAULT_DIMMING_CURVE                  0
#define NVMEMORY_DEFAULT_MULTIPLE_BITS                  0x01
#endif /* DALI_USE_DEVICE_TYPE_6 */


// The nvmem_daliBankMemoryRead() function may signal errors using the
// following return value:
#define BANK_MEMORY_READ_ERROR                          0x0100

#ifdef DALI_USE_EXTRA_MEMORY_BANKS
// The nvmem_daliBankMemoryWrite() function should return on its
// completion status using the following values:
#define BANK_MEMORY_WRITE_OK_NOT_LAST_LOC               0x00
#define BANK_MEMORY_WRITE_OK_LAST_LOC                   0x01
#define BANK_MEMORY_WRITE_CANNOT_WRITE                  0x02
#endif /* DALI_USE_EXTRA_MEMORY_BANKS */

// DALI 2.0 commands
// Indirect arc power control
#define OFF                                             0x00
#define UP                                              0x01
#define DOWN                                            0x02
#define STEP_UP                                         0x03
#define STEP_DOWN                                       0x04
#define RECALL_MAX_LEVEL                                0x05
#define RECALL_MIN_LEVEL                                0x06
#define STEP_DOWN_AND_OFF                               0x07
#define ON_AND_STEP_UP                                  0x08
#define ENABLE_DAPC_SEQUENCE                            0x09
#define GO_TO_LAST_ACTIVE_LEVEL                         0x0A

// 11-15 RESERVED
#define GO_TO_SCENE                                     0x10
//#define GO_TO_SCENE_1                                   0x11
//#define GO_TO_SCENE_2                                   0x12
//#define GO_TO_SCENE_3                                   0x13
//#define GO_TO_SCENE_4                                   0x14
//#define GO_TO_SCENE_5                                   0x15
//#define GO_TO_SCENE_6                                   0x16
//#define GO_TO_SCENE_7                                   0x17
//#define GO_TO_SCENE_8                                   0x18
//#define GO_TO_SCENE_9                                   0x19
//#define GO_TO_SCENE_10                                  0x1A
//#define GO_TO_SCENE_11                                  0x1B
//#define GO_TO_SCENE_12                                  0x1C
//#define GO_TO_SCENE_13                                  0x1D
//#define GO_TO_SCENE_14                                  0x1E
//#define GO_TO_SCENE_15                                  0x1F

// Configuration Commands
#define DALI_RESET                                      0x20
#define STORE_ACTUAL_LEVEL_IN_DTR0                      0x21
#define SAVE_PERSISTENT_VARIABLES                       0x22
#define SET_OPERATING_MODE_DTR0                         0x23
#define RESET_MEMORY_BANK_DTR0                          0x24
#define IDENTIFY_DEVICE                                 0x25


// 38 - 41 RESERVED
#define SET_MAX_LEVEL_DTR0                              0x2A
#define SET_MIN_LEVEL_DTR0                              0x2B
#define SET_SYSTEM_FAILURE_LEVEL_DTR0                   0x2C
#define SET_POWER_ON_LEVEL_DTR0                         0x2D
#define SET_FADE_TIME_DTR0                              0x2E
#define SET_FADE_RATE_DTR0                              0x2F
#define SET_EXTENDED_FADE_TIME_DTR0                     0x30

// 49 - 63 RESERVED    
#define SET_SCENE                                       0x40
//#define SET_SCENE_1                                     0x41
//#define SET_SCENE_2                                     0x42
//#define SET_SCENE_3                                     0x43
//#define SET_SCENE_4                                     0x44
//#define SET_SCENE_5                                     0x45
//#define SET_SCENE_6                                     0x46
//#define SET_SCENE_7                                     0x47
//#define SET_SCENE_8                                     0x48
//#define SET_SCENE_9                                     0x49
//#define SET_SCENE_10                                    0x4A
//#define SET_SCENE_11                                    0x4B
//#define SET_SCENE_12                                    0x4C
//#define SET_SCENE_13                                    0x4D
//#define SET_SCENE_14                                    0x4E
//#define SET_SCENE_15                                    0x4F

 
#define REMOVE_FROM_SCENE                               0x50
//#define REMOVE_FROM_SCENE_1                             0x51
//#define REMOVE_FROM_SCENE_2                             0x52
//#define REMOVE_FROM_SCENE_3                             0x53
//#define REMOVE_FROM_SCENE_4                             0x54
//#define REMOVE_FROM_SCENE_5                             0x55
//#define REMOVE_FROM_SCENE_6                             0x56
//#define REMOVE_FROM_SCENE_7                             0x57
//#define REMOVE_FROM_SCENE_8                             0x58
//#define REMOVE_FROM_SCENE_9                             0x59
//#define REMOVE_FROM_SCENE_10                            0x5A
//#define REMOVE_FROM_SCENE_11                            0x5B
//#define REMOVE_FROM_SCENE_12                            0x5C
//#define REMOVE_FROM_SCENE_13                            0x5D
//#define REMOVE_FROM_SCENE_14                            0x5E
//#define REMOVE_FROM_SCENE_15                            0x5F  

 
#define ADD_TO_GROUP                                    0x60
//#define ADD_TO_GROUP_1                                  0x61
//#define ADD_TO_GROUP_2                                  0x62
//#define ADD_TO_GROUP_3                                  0x63
//#define ADD_TO_GROUP_4                                  0x64
//#define ADD_TO_GROUP_5                                  0x65
//#define ADD_TO_GROUP_6                                  0x66
//#define ADD_TO_GROUP_7                                  0x67
//#define ADD_TO_GROUP_8                                  0x68
//#define ADD_TO_GROUP_9                                  0x69
//#define ADD_TO_GROUP_10                                 0x6A
//#define ADD_TO_GROUP_11                                 0x6B
//#define ADD_TO_GROUP_12                                 0x6C
//#define ADD_TO_GROUP_13                                 0x6D
//#define ADD_TO_GROUP_14                                 0x6E
//#define ADD_TO_GROUP_15                                 0x6F


#define REMOVE_FROM_GROUP                               0x70
//#define REMOVE_FROM_GROUP_1                             0x61
//#define REMOVE_FROM_GROUP_2                             0x62
//#define REMOVE_FROM_GROUP_3                             0x63
//#define REMOVE_FROM_GROUP_4                             0x64
//#define REMOVE_FROM_GROUP_5                             0x65
//#define REMOVE_FROM_GROUP_6                             0x66
//#define REMOVE_FROM_GROUP_7                             0x67
//#define REMOVE_FROM_GROUP_8                             0x68
//#define REMOVE_FROM_GROUP_9                             0x69
//#define REMOVE_FROM_GROUP_10                            0x6A
//#define REMOVE_FROM_GROUP_11                            0x6B
//#define REMOVE_FROM_GROUP_12                            0x6C
//#define REMOVE_FROM_GROUP_13                            0x6D
//#define REMOVE_FROM_GROUP_14                            0x6E
//#define REMOVE_FROM_GROUP_15                            0x6F
        
#define SET_SHORT_ADDRESS_DTR0                          0x80
#define ENABLE_WRITE_MEMORY                             0x81
    
// Queries
#define QUERY_STATUS                                    0x90
#define QUERY_CONTROL_GEAR_PRESENT                      0x91
#define QUERY_LAMP_FAILURE                              0x92
#define QUERY_LAMP_POWER_ON                             0x93
#define QUERY_LIMIT_ERROR                               0x94
#define QUERY_RESET_STATE                               0x95
#define QUERY_MISSING_SHORT_ADDRESS                     0x96
#define QUERY_VERSION_NUMBER                            0x97
#define QUERY_CONTENT_DTR0                              0x98
#define QUERY_DEVICE_TYPE                               0x99
#define QUERY_PHYSICAL_MINIMUM                          0x9A
#define QUERY_POWER_FAILURE                             0x9B
#define QUERY_CONTENT_DTR1                              0x9C
#define QUERY_CONTENT_DTR2                              0x9D
#define QUERY_OPERATING_MODE                            0x9E
#define QUERY_LIGHT_SOURCE_TYPE                         0x9F
    
#define QUERY_ACTUAL_LEVEL                              0xA0
#define QUERY_MAX_LEVEL                                 0xA1
#define QUERY_MIN_LEVEL                                 0xA2
#define QUERY_POWER_ON_LEVEL                            0xA3
#define QUERY_SYSTEM_FAILURE_LEVEL                      0xA4
#define QUERY_FADE_TIME_FADE_RATE                       0xA5
#define QUERY_MANUFACTURER_SPECIFIC_MODE                0xA6
#define QUERY_NEXT_DEVICE_TYPE                          0xA7
#define QUERY_EXTENDED_FADE_TIME                        0xA8
#define QUERY_CONTROL_GEAR_FAILURE                      0xAA
    
    
#define QUERY_SCENE_LEVEL                               0xB0
//#define QUERY_SCENE_LEVEL_SCENE_1                       0xB1
//#define QUERY_SCENE_LEVEL_SCENE_2                       0xB2
//#define QUERY_SCENE_LEVEL_SCENE_3                       0xB3
//#define QUERY_SCENE_LEVEL_SCENE_4                       0xB4
//#define QUERY_SCENE_LEVEL_SCENE_5                       0xB5
//#define QUERY_SCENE_LEVEL_SCENE_6                       0xB6
//#define QUERY_SCENE_LEVEL_SCENE_7                       0xB7
//#define QUERY_SCENE_LEVEL_SCENE_8                       0xB8
//#define QUERY_SCENE_LEVEL_SCENE_9                       0xB9
//#define QUERY_SCENE_LEVEL_SCENE_10                      0xBA
//#define QUERY_SCENE_LEVEL_SCENE_11                      0xBB
//#define QUERY_SCENE_LEVEL_SCENE_12                      0xBC
//#define QUERY_SCENE_LEVEL_SCENE_13                      0xBD
//#define QUERY_SCENE_LEVEL_SCENE_14                      0xBE
//#define QUERY_SCENE_LEVEL_SCENE_15                      0xBF
    
    
#define QUERY_GROUPS_0_7                                0xC0
#define QUERY_GROUPS_8_15                               0xC1
#define QUERY_RANDOM_ADDRESS_H                          0xC2
#define QUERY_RANDOM_ADDRESS_M                          0xC3
#define QUERY_RANDOM_ADDRESS_L                          0xC4   
#define READ_MEMORY_LOCATION                            0xC5
    
    
// Special Commands Address Byte
#define TERMINATE                                       0xA1
#define DTR0_DATA                                       0xA3
#define INITIALISE                                      0xA5
#define RANDOMISE                                       0xA7
#define COMPARE                                         0xA9
#define WITHDRAW                                        0xAB
#define PING                                            0xAD
#define SEARCHADDRH                                     0xB1
#define SEARCHADDRM                                     0xB3
#define SEARCHADDRL                                     0xB5
#define PROGRAM_SHORT_ADDRESS                           0xB7
#define VERIFY_SHORT_ADDRESS                            0xB9
#define QUERY_SHORT_ADDRESS                             0xBB
#define ENABLE_DEVICE_TYPE                              0xC1
#define DTR1_DATA                                       0xC3
#define DTR2_DATA                                       0xC5
#define WRITE_MEMORY_LOCATION                           0xC7
#define WRITE_MEMORY_LOCATION_NO_REPLY                  0xC9

// LED Devices IEC 62386-207 commands
     
#ifdef DALI_USE_DEVICE_TYPE_6

#define REFERENCE_SYSTEM_POWER                          0xE0
#define ENABLE_CURRENT_PROTECTOR                        0xE1
#define DISABLE_CURRENT_PROTECTOR                       0xE2
#define SELECT_DIMMING_CURVE                            0xE3
#define STORE_DTR_AS_FAST_FADE_TIME                     0xE4
#define QUERY_GEAR_TYPE                                 0xED
#define QUERY_DIMMING_CURVE                             0xEE
#define QUERY_POSSIBLE_OPERATING_MODES                  0xEF
#define QUERY_FEATURES                                  0xF0
#define QUERY_FAILURE_STATUS                            0xF1
#define QUERY_SHORT_CIRCUIT                             0xF2
#define QUERY_OPEN_CIRCUIT                              0xF3
#define QUERY_LOAD_DECREASE                             0xF4
#define QUERY_LOAD_INCREASE                             0xF5
#define QUERY_CURRENT_PROTECTOR_ACTIVE                  0xF6
#define QUERY_THERMAL_SHUT_DOWN                         0xF7
#define QUERY_THERMAL_OVERLOAD                          0xF8
#define QUERY_REFERENCE_RUNNING                         0xF9
#define QUERY_REFERENCE_MEASUREMENT_FAILED              0xFA
#define QUERY_CURRENT_PROTECTOR_ENABLED                 0xFB
#define QUERY_OPERATING_MODE_207                        0xFC
#define QUERY_FAST_FADE_TIME                            0xFD
#define QUERY_MIN_FAST_FADE_TIME                        0xFE
#define QUERY_EXTENDED_VERSION_NUMBER                   0xFF
     
#endif
    
    

/** \brief Command time-out variable.
 *
 * Value measured in milliseconds. Used for commands that need to be received
 * twice within 100ms to be executed.
 */
extern volatile uint8_t commandTimeout_ms;

    
    
    
/** \brief Initialise DALI state machine from power-up.
 *
 * It calls the DALI protocol initialiser and configures the machine with the
 * values read from non-volatile memory. This function is called by dali_init().
 */
//inline void idali_initMachine();
void idali_initMachine();


/** \brief DALI State machine 1ms tick.
 *
 * This function is used to derive the different software timers of the DALI
 * machine. It also runs the fading mechanism and the detection of the DALI
 * cable disconnection.
 */
void idali_tick1msMachine();
//inline void idali_tick1msMachine();


/** \brief This function is called by the DALI protocol layer upon reception of
 * a valid forward frame.
 *
 * Since it is called from the interrupt service routine, it just copies the
 * data into a variable and sets a bit to signal that a frame has been received
 * for the state machine (which runs from the mainline code) to be able to pick
 * up and process the frame.
 *
 * @param data DALI forward frame payload that has been received.
 */
void idali_receiveForwardFrame(uint16_t data);


/* ToDo Move to a different file later. */
/** \brief API Call: Run one iteration of the DALI state machine which, among
 * others, checks if any new DALI frame has arrived and if so, processes it.
 *
 * This function should be called from the main while(1) loop and should be
 * executed as often as possible. It does the following: \n
 *  - checks for any DALI frame on the bus and if one exists it processes it \n
 *  - checks if the machine is in reset state by checking the relevant values \n
 *  - checks lamp operating mode, by looking at the value provided by the
 *    application \n
 *  - checks the status of reference system power by looking at the value
 *    provided by the application \n
 *  - sets the lamp failure and lamp power on bits according to the lamp power
 *    level and the lamp status provided by the application \n
 *  - sets the various lamp status bits defined by the DALI LED
 *    specifications, if Device Type 6 is enabled \n
 *  - if any memory bank has been modified, recomputes the checksum for one of
 *    them and updates it \n
 *  - if the lamp power needs to be changed, update the value and inform the
 *    application that it should change the lamp power
 */
void dali_tasks();

#endif /* DALI_CG_MACHINE_H */
