/**
 * \file
 *
 * \brief Decode and encode dali transmission bits
 *
 * Copyright (c) 2016 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
#include "dali_top.h"
//#include "dali_cmd.h"
#include "dali_cg_hardware.h"
#include "dali_cg_machine.h"


// Various timer values that need to be loaded into the timer register to
// produce a given delay until the timer overflows. These are based on the TE
// value #defined in dali_cg_hardware.h.
/*One TE is 1/2.4KHz = 416uS*/
// Timer TCB frequency is 31.2KHz (period is 32.051us)
// MIN_SETTLING_TIME_VALUE = 5.5ms. 2403.825us have already elapsed. 
// Difference = 5500us - 2403.825us = 3096.175us. 97 timer ticks * 32.051us = 3096.175us               
// MAX_SETTLING_TIME_VALUE = 10.5ms. 5.5ms have already elapsed. 
// Difference = 10500us - 5500us = 5000us. 156 * 32.051us = 5000us
#define RX_STOP_TIME_VALUE              (75)               // 75 timer ticks * 32.051us = 2403.825us
#define MIN_SETTLING_TIME_VALUE         (97)   
#define MAX_SETTLING_TIME_VALUE         (156)


// Values that the timer will be compared to to check the elapsed time. During a
// DALI frame reception at each bus transition the software arms the Te timer
// to trigger an interrupt after 4 Te. If this happens, it will be considered a
// timeout (and unless this is the last bit reception will be aborted and an
// error will be signaled) since all delays within a frame should be at most 2
// Te. If the external interrupt is triggered during this 4 Te period, the timer
// register is compared to these 4 values and if it falls within
// [TE_MIN, TE_MAX] a single Te is considered to have elapsed, whereas if it
// falls within [TE2_MIN, TE2_MAX], 2 Te is considered to have elapsed.
// Otherwise, a reception error is signaled.
// since these values are used during receiving the half-bits, RX_STOP_TIME_VALUE
// is used.
#define TE_MIN                          (TE_VALUE - 6)       // tolerant low limit
#define TE_MAX                          (TE_VALUE + 4)       // tolerant high limit
#define TEx2_MIN                        (17)
#define TEx2_MAX                        (34)


// DALI protocol state machine states
enum
{
    ST_IDLE,                        // Idle
    ST_SEND_BACKWARD_DATA,          // Sending reply to Control Device
    ST_RECEIVE_FWD_DATA,            // Receiving forward frame
    ST_WAIT_AFTER_CODE_VIOLATION,   // Detected a code violation and waits for 6 Te before becoming ready to receive again (2450us = 5.87 Te ~ 6 Te)
    ST_RECEIVE_FWD_DATA_EXTRA_TE,   // Received forward frame ending in a '1', so we need to wait for one more Te
    ST_WAIT_FOR_SEND,               // Waiting minimum delay between forward and backward frame
    ST_CAN_SEND,                    // Backward frame transmission is allowed
    ST_WAIT_AFTER_FWD_FRAME         // Idle
};


// The state machine sets this flag if the upper layers require a backward frame
// to be sent but we're currently in the ST_WAIT_FOR_SEND state. This tells the
// machine to start sending the frame as soon as we transition to ST_CAN_SEND.
typedef union
{
    uint8_t All;
    struct
    {
        unsigned backwardFrameSendTimeReached       : 1;
        unsigned                                    : 7;
    };
}tdali_flags_protocol;

/**********************DALI Protocol definitions*(END)*************************/




/**********************DALI Protocol variables*********************************/


/** \brief The state the DALI protocol state machine is in.
 *
 * This is one if ST_IDLE, ST_SEND_BACKWARD_DATA, etc.
 */
static volatile uint8_t daliProtocolState;


/** \brief Data that has been received so far. */
static volatile uint16_t rxPacket;

///** \brief Time for which data has been received so far. 
// * This is used to keep track of whether the time difference between 
// * 2 forward frames in 2.4ms */
//static volatile uint8_t rxPacketTimeCounter = 0;
//
///** \brief Flag to indicate whether forward frame reception is allowed. 
// * If the time difference between 2 forward frames in 2.4ms, this flag is set
// * and reception of the next forward frame is allowed */
//static volatile uint8_t fwdFrameReceptionAllowed = 1;


/** \brief Internal variable used in the transmission and reception of DALI
 * data.
 */
static volatile uint8_t halfBitNumber;


/** \brief Internal variable used in the transmission and reception of DALI
 * data.
 */
static volatile uint8_t timerTicks = 0;


/** \brief Shift register that outputs the data that needs to be transmitted. */
static volatile uint8_t txPacket;


/** \brief Data that needs to be transmitted back to the Control Device. */
static volatile uint8_t backwardFrame;


/** \brief Number of bits that have been received so far. */
static volatile uint8_t rxPacketLen;


/** \brief DALI protocol state machine flags variable.
 *
 * Implements just one bit that signals that there's data that needs to be
 * transmitted as soon as the transmission window opens.
 */
static volatile tdali_flags_protocol daliProtocolFlags;

/**********************DALI Protocol variables*(END)***************************/



/**********************Function implementations********************************/


void idali_initProtocol()
{
        /* Configure PB4 as DALI output */
	PORTB_DIRSET = 0x01 << 4;
	PORTB_OUTSET = 0x01 << 4;  // High level
	/* Configure PB3 as DALI input */
	PORTB_DIRCLR = 0x01 << 3;
	/*Sense both edges*/
	PORTB_PIN3CTRL = 0x01;
        
        // Start from ST_IDLE
        daliProtocolState = ST_IDLE;

        // Clear all flags
        daliProtocolFlags.All = 0;
        // Start from ST_IDLE
        daliProtocolState = ST_IDLE;

        // Clear all flags
        daliProtocolFlags.All = 0;
}



ISR(PORTB_PORT_vect)
{
    static uint16_t intValue;
    
    /*Clear interrupt flag firstly*/
    dalihw_teTimerClearInterrupt();
//    PORTB.INTFLAGS = 0x01 << 3;
    TCB0.CNT = 0;
    
    // DALI bus level changed. Based on the state we're currently in, this
    // transition needs to be interpreted.
    if (daliProtocolState == ST_IDLE)
    {
        // Something unexpected happened on the bus.
//        if((PORTB_IN & (0x01 << 3)) == LOW) 
        if (dalihw_isDALILineLow())
        {
            // A falling edge has been detected. Either this is a frame coming,
            // or the cable has been disconnected. The cable is being polled
            // every millisecond by the DALI state machine, so we need to assume
            // that a packet is going to arrive.
            daliProtocolState = ST_RECEIVE_FWD_DATA;
            rxPacket = 0;
            rxPacketLen = 0;
            timerTicks = 0;
            halfBitNumber = 0;
            // clear the exiting flag 
            dalihw_teTimerClearInterrupt();
            // Enable TCB timing
            TCB0.INTCTRL = 0x01;
//            dalihw_teTimerSetVal(RX_STOP_TIME_VALUE);
//            dalihw_teTimerSetVal(TEx6_STOP_TIME_VALUE);
            dalihw_teTimerOn();
        }
        // Note that we don't care if we got here by a rising edge event, since
        // this should normally only happen after the cable has just been
        // connected.

        // Just as a precaution, we should make sure we're not driving our own
        // line low.
        dalihw_setDALILineHigh();
    }
    else if(daliProtocolState == ST_WAIT_FOR_SEND)
    {
        if (dalihw_isDALILineLow())
        {
            // A falling edge has been detected. Either this is a frame coming,
            // or the cable has been disconnected. The cable is being polled
            // every millisecond by the DALI state machine, so we need to assume
            // that a packet is going to arrive.
            dalihw_teTimerOff();
            daliProtocolState = ST_RECEIVE_FWD_DATA;
            rxPacket = 0;
            rxPacketLen = 0;
            timerTicks = 0;
            halfBitNumber = 0;
            // clear the exiting flag 
            dalihw_teTimerClearInterrupt();
            // Enable TCB timing
            TCB0.INTCTRL = 0x01;
//            dalihw_teTimerSetVal(RX_STOP_TIME_VALUE);
//            dalihw_teTimerSetVal(TEx6_STOP_TIME_VALUE);
            dalihw_teTimerOn();
        }
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA)
    {
        intValue = timerTicks;
        timerTicks = 0;
//        intValue = dalihw_teTimerGetVal();
//        dalihw_teTimerSetVal(RX_STOP_TIME_VALUE);
//        dalihw_teTimerSetVal(TEx6_STOP_TIME_VALUE);
        switch (halfBitNumber)
        {
            case 0:
                // halfBitNumber == 0 means "this should be the middle of the
                // start bit". Thus a Te now means we've got it ok, while
                // anything else means an error has occurred.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    halfBitNumber = 2;
                }
                else
                {
//                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 1:
                // halfBitNumber == 1 means "we were in the first part of
                // receiving a 1". Thus a Te now means we've got the 1 ok, while
                // anything else means an error has occurred.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // Middle of a '1' bit
                    rxPacket <<= 1;
                    rxPacket |= 1;
                    rxPacketLen++;
                    halfBitNumber = 2;
                }
                else
                {
//                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 2:
                // halfBitNumber == 2 means "we were in the middle of receiving
                // a 1". Thus a Te now means we're getting another 1 and this is
                // the beginning, a 2 Te means we're getting a 0 and this is
                // the middle.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '1' bit
                    halfBitNumber = 1;
                }
                else if ((intValue >= TEx2_MIN) && (intValue <= TEx2_MAX))
                {
                    // Middle of a '0' bit
                    rxPacket <<= 1;
                    rxPacketLen++;
                    halfBitNumber = 3;
                }
                else
                {
//                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 3:
                // halfBitNumber == 3 means "we were in the middle of receiving
                // a 0". Thus a Te now means we're getting another 0 and this is
                // the beginning, a 2 Te means we're getting a 1 and this is
                // the middle.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '0' bit
                    halfBitNumber = 4;
                }
                else if ((intValue >= TEx2_MIN) && (intValue <= TEx2_MAX))
                {
                    // Middle of a '1' bit
                    rxPacket <<= 1;
                    rxPacket |= 1;
                    rxPacketLen++;
                    halfBitNumber = 2;
                }
                else
                {
//                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;
            case 4:
                // halfBitNumber == 4 means "we were in the first part of
                // receiving a 0". Thus a Te now means we've got the 0 ok, while
                // anything else means an error has occured.
                if ((intValue >= TE_MIN) && (intValue <= TE_MAX))
                {
                    // 1st half of a '0' bit received OK
                    rxPacket <<= 1;
                    rxPacketLen++;
                    halfBitNumber = 3;
                }
                else
                {
//                    dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
                    daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
                }
                break;

        }
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA_EXTRA_TE)
    {
        timerTicks = 0;
        // Transition during the last half of the 2nd stop bit, which is
        // considered an error. Abort reception.
//        dalihw_teTimerSetVal(dalihw_teTimerGetVal() + RX_STOP_TIME_VALUE);
        daliProtocolState = ST_WAIT_AFTER_CODE_VIOLATION;
    }
    
//    // Arm the interrupt such that it will fire on the opposite edge as well.
//    dalihw_extInterruptToggleEdge();
}


ISR(TCB0_INT_vect)
{
    /*Clear interrupt flag firstly*/
    dalihw_teTimerClearInterrupt();
    // Increment the timer tick count. This keeps track of time elapsed.
    timerTicks++;
    
    if (daliProtocolState == ST_SEND_BACKWARD_DATA)
    {
        // We are now sending a backframe to the Control Device. This being the
        // timer overflow interrupt, we need to set new data on the bus.
        if(timerTicks == TE_VALUE)
        {
            switch (halfBitNumber)
            {
                case 1:
                    // START BIT, 2nd half
                    // The timer keeps running, at this current time it couldn't
                    // be too far away from 0, but in order to increase the accuracy
                    // of the baud rate, we add to this value instead of replacing
                    // it. This has the advantage of removing most of the delay from
                    // the ISR vectoring up to this point
                    dalihw_setDALILineHigh();
    //                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                    break;
                case 2:
                    // DATA BIT, 1st half
                    // Now is the time to set the DALI bus to the proper level for
                    // bit 0 of our data.
    //                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                    if ((txPacket & 0x80) != 0)
                    {
                        // Send a '1' bit, which starts with a DALI_LO
                              dalihw_setDALILineLow();
                    }
                    else
                    {
                        // Send a '0' bit, which starts with a DALI_HI
                              dalihw_setDALILineHigh();
                    }                
                    break;
                case 18:
                    // Device is transmitting, STOP condition 2450us
    //                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TX_STOP_TIME_VALUE);
                    dalihw_setDALILineHigh();
                    break;
                case 19:
                    // Transmission finalised OK. Go back to ST_IDLE such that
                    // we can receive the next frame
                    dalihw_teTimerOff();
                    daliProtocolState = ST_IDLE;
                    break;
                default:
                    // This case handles all the in-between (data) bits, with a
                    // distinction between 1st half (even halfBitNumber) and 2nd
                    // half (odd halfBitNumber) of a bit.
    //                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
                        if (((halfBitNumber & 0x01) == 0))
                        {
                            // halfBitNumber is even, we are in the 1st half of the bit.
                            txPacket <<= 1;
                            if ((txPacket & 0x80) != 0)
                            {
                                // We need to send a '1'
                                  dalihw_setDALILineLow();
                            }
                            else
                            {
                                // We need to send a '0'
                                  dalihw_setDALILineHigh();
                            }
                        }
                        else
                        {
                          // halfBitNumber is odd, 2nd half of the bit.
                            if ((txPacket & 0x80) != 0)
                            {
                                // We're currently sending a '1'
                                  dalihw_setDALILineHigh();
                            }
                            else
                            {
                                // We're currently sending a '0'
                                  dalihw_setDALILineLow();
                            }
                        }
                    break;
            }
            if(halfBitNumber < 18)  
//            if(halfBitNumber != 18)  
            {
                halfBitNumber++;
                timerTicks = 0;
            }
        }

        
        // Move on in state ST_SEND_BACKWARD_DATA, by incrementing the halfbit
        // we're transmitting
        // Check first for STOP condition
//        if(timerTicks == TE_VALUE)
//        {
//            if(halfBitNumber < 18)  
////            if(halfBitNumber != 18)  
//            {
////                halfBitNumberOld = halfBitNumber;
//                halfBitNumber++;
//                timerTicks = 0;
//            }
//        }
        if(timerTicks == RX_STOP_TIME_VALUE)
        {
            if(halfBitNumber == 18)
            {
//                halfBitNumberOld = halfBitNumber;
                halfBitNumber++;
                timerTicks = 0;
            }
        }
        
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA)
    {
        if(timerTicks == RX_STOP_TIME_VALUE)
        {
            timerTicks = 0;
            // Time-out. Check first if we're at a final bit (got 16 bits)
            // and the last bit is a 1 or a 0. We need to add an extra delay
            // of 1 Te if this is the final bit of a packet and the last bit was
            // a 1, just to make sure that we exit ST_RECEIVE_FORWARD_DATA after
            // the last stop bit has been fully received
            if ((rxPacketLen == 16) && ((rxPacket & 0x01) != 0x00))
            {
                // The last bit we've received is very likely the last one in the
                // transmission, and it is a '1'. Thus, we need to add a 1 Te
                // delay
                // keep the Te timer on, reload timer with 1 Te
                daliProtocolState = ST_RECEIVE_FWD_DATA_EXTRA_TE;
//                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TE_VALUE);
            }
            else if ((rxPacketLen == 16) && ((halfBitNumber == 2) || (halfBitNumber == 4)))
            {
                // What we've just got is a forward frame, as it has 16 bits.
                // We've made sure we haven't received any stray transitions
                // during or after the last bit, by checking the halfBitNumber
                // sub-state
    //            // Modified according to IEC 62386-101 (ed2.0) Table 17
//                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + MIN_SETTLING_TIME_VALUE);
    //            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx8_SETTLING_TIME_VALUE);
    //            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx7_VALUE);
                daliProtocolState = ST_WAIT_FOR_SEND;
                // Call idali_receiveForwardFrame after state has been set to
                // ST_WAIT_FOR_SEND such that if this function subsequently calls
                // idali_sendBackwardFrame() the state check will pass and load the
                // value for transmission
                idali_receiveForwardFrame(rxPacket);
            }
            else
            {
                // ERROR: received wrong number of bits. Abort.
                // Go directly to ST_IDLE instead of going to
                // ST_WAIT_AFTER_CODE_VIOLATION since this timeout signaled that
                // we've already had 6 Te with no bus activity.
                // Added to pass the test 12.4.4. Re-write this so that abstraction 
                // between the layers is not disturbed.
//                commandTimeout_ms = 0;
                dalihw_teTimerOff();
                daliProtocolState = ST_IDLE;
            }
          
        }
        
    }
    else if (daliProtocolState == ST_RECEIVE_FWD_DATA_EXTRA_TE)
    {
        if(timerTicks == TE_VALUE)
        {
            timerTicks = 0;
            // Time-out. If we didn't have any transition during this last wait
            // (which is safe to assume as such a transition would have stopped
            // the transmission and signaled an error already) we can now check
            // the data received.
            if ((rxPacketLen == 16) && ((halfBitNumber == 2) || (halfBitNumber == 4)))
            {
                // What we've just got is a forward frame, as it has 16 bits.
                // We've made sure we haven't received any stray transitions
                // during or after the last bit, by checking the halfBitNumber
                // sub-state
//                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + MIN_SETTLING_TIME_VALUE);
    //            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx8_SETTLING_TIME_VALUE);
    //            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx7_VALUE);
                daliProtocolState = ST_WAIT_FOR_SEND;
                // Call idali_receiveForwardFrame after state has been set to
                // ST_WAIT_FOR_SEND such that if this function subsequently calls
                // idali_sendBackwardFrame() the state check will pass and load the
                // value for transmission
                idali_receiveForwardFrame(rxPacket);
            }
            else
            {
                // ERROR: received wrong number of bits. Abort.
                // Go directly to ST_IDLE instead of going to
                // ST_WAIT_AFTER_CODE_VIOLATION since this timeout signaled that
                // we've already had 4 Te with no bus activity.
                dalihw_teTimerOff();
                daliProtocolState = ST_IDLE;
            }
        }
      
        
    }
    else if (daliProtocolState == ST_WAIT_FOR_SEND)
    {
        if(timerTicks == MIN_SETTLING_TIME_VALUE)
        {
            // During ST_WAIT_FOR_SEND transmission is not allowed, but we can
            // load a packet for transmission.
            // This time-out means that this state has finished and sending is now
            // allowed. We can either start sending a packet that was already ready,
            // or wait some more for a possible packet from the upper layers.
            if (daliProtocolFlags.backwardFrameSendTimeReached == 1)
            {
                // Transmit frame, i.e. move to the TX state and start putting
                // bits on the line
                daliProtocolFlags.backwardFrameSendTimeReached = 0;
                daliProtocolState = ST_SEND_BACKWARD_DATA;
                timerTicks = 0;
                halfBitNumber = 1;
                txPacket = backwardFrame;
//                dalihw_teTimerSetVal(TE_VALUE);
                dalihw_setDALILineLow();
            }
            else
            {
                // No frame ready for transmission, we can move to the next state
                // which waits for one. This state will last 22 - 7 = 15 Te at most
//                dalihw_teTimerSetVal(dalihw_teTimerGetVal() + MAX_SETTLING_TIME_VALUE);
    //            dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx12SETTLING_TIME_VALUE);
                // dalihw_teTimerSetVal(dalihw_teTimerGetVal() + TEx15_VALUE);
                daliProtocolState = ST_CAN_SEND;
            }
        }
        
        
    }
    else if (daliProtocolState == ST_CAN_SEND)
    {
        if(timerTicks == MAX_SETTLING_TIME_VALUE)
        { 
            timerTicks = 0;
            // During ST_CAN_SEND we can immediately begin transmitting a backward
            // frame.
            // However, since a time-out occurred, we will now move to ST_IDLE
            dalihw_teTimerOff();
            daliProtocolState = ST_IDLE;
        }
        
    }
    else if (daliProtocolState == ST_WAIT_AFTER_CODE_VIOLATION)
    {
        if(timerTicks == RX_STOP_TIME_VALUE)
        {
            timerTicks = 0;
            // This state is used as a safeguard after a code violation has been
            // detected. The standard requires the Control Gear to be ready to
            // receive a new forward frame 1.7ms (4 Te) after a code violation has
            // been detected.
            dalihw_teTimerOff();
            daliProtocolState = ST_IDLE;
        }
        
        
    }

}


void idali_sendBackwardFrame(uint8_t data)
{
    // As the timer interrupt may occur after the state check is made and move
    // us away from the state we thought we were in, we disable all interrupts
    // (which is not a problem given the limited length of this function).
    // One could also stop the timer but this would introduce a slight delay in
    // the duration of the ST_WAIT_FOR_SEND state.
    cli();
    TCB0.CNT = 0;
//    di();
    // If we are not in one of these two states, we can't send the packet any
    // more, since it came too late.
    if ((daliProtocolState == ST_WAIT_FOR_SEND) && (daliProtocolState != ST_RECEIVE_FWD_DATA))
    {
        // We are waiting for the time for sending backframes to come.
        daliProtocolFlags.backwardFrameSendTimeReached = 1;
        backwardFrame = data;
    }
    else if ((daliProtocolState == ST_CAN_SEND)  && (daliProtocolState != ST_RECEIVE_FWD_DATA))
    {
        // Start transmitting frame, i.e. move to the TX state and start putting
        // bits on the line.
        daliProtocolState = ST_SEND_BACKWARD_DATA;
        timerTicks = 0;
        halfBitNumber = 1;
        txPacket = data;
//        dalihw_teTimerSetVal(TE_VALUE);
        dalihw_setDALILineLow();
        // Given that the timer still runs, it may have overflowed until we got
        // to reset its TE_TIMER_VAL. Thus, we clear its flag just to make sure,
        // since this flag set at this point would ruin our start bit.
        dalihw_teTimerClearInterrupt();
    }
    sei();
//    ei();
}

/**********************Function implementations*(END)**************************/

