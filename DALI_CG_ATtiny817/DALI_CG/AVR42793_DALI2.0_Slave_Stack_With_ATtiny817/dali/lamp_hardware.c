/********************************************************************
 *
 * Copyright (C) 2014 Microchip Technology Inc. and its subsidiaries
 * (Microchip).  All rights reserved.
 *
 * You are permitted to use the software and its derivatives with Microchip
 * products. See the license agreement accompanying this software, if any, for
 * more info about your rights and obligations.
 *
 * SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR
 * PURPOSE. IN NO EVENT SHALL MICROCHIP, SMSC, OR ITS LICENSORS BE LIABLE OR
 * OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH
 * OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY FOR ANY DIRECT OR INDIRECT
 * DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL,
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR OTHER SIMILAR COSTS. To the fullest
 * extend allowed by law, Microchip and its licensors liability will not exceed
 * the amount of fees, if any, that you paid directly to Microchip to use this
 * software.
 *************************************************************************
 *
 *                           lamp_hardware.c
 *
 * About:
 *  Lamp hardware interface. This file handles controlling the lamp output and
 *  checking of its status. It also provides the lamp dimming table such that it
 *  behaves according to DALI specifications.
 *
 * Hardware:
 *  Microchip Lighting Communications Main Board (DM160214) +
 *  Microchip DALI Adapter (AC160214-1)
 *
 * Author            Date                Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Mihai Cuciuc      28 November 2013    Implement isolated lamp hardware
 *                                       functionality.
 ******************************************************************************/

#include "lamp_hardware.h"

#include "dali_cg.h"
#include "dali_cg_hardware.h"


// When using this symbol the 10-bit dimming values are stored with 16 bit
// precision. Removing this flag saves ~40 words (on an Enhanced Mid-Range PIC)
// at the cost of losing 2 bits of precision for dimming values > 255.
#define DALI_USE_FULL_TABLE_PRECISION

/**********************Lamp hardware variables*********************************/


///** \brief Logarithmic dimming table (first 204 values).
// *
// * These two arrays, logDimmingTableLow and logDimmingTableHigh hold the raw
// * power levels that the lamp should give at each of the 255 DALI arc power
// * levels. They are scaled to the full range of the 10-bit PWM that is used by
// * this application example, thus 100% corresponds to the value 1023. This PWM
// * value is directly proportional to the LED current. Normally one should
// * recompute these tables to take into account any non-linearity in the LED
// * light output vs. current characteristic. However, the LED used by this
// * application has a fairly linear response such that this step has been
// * skipped.
// *
// * The 255 values are split into two tables to conserve some space. The first 204
// * values only need 8 bits each, whereas the last 51 need more bits.
// * Note that the PWM used in this application has a resolution of 10 bits, so
// * one could pack the bits even tighter.
// */
//static const uint8_t logDimmingTableLow[204] =
//{
//    0,4,4,4,4,5,5,5,5,5,5,5,6,6,6,6,
//    6,6,7,7,7,7,7,7,8,8,8,8,9,9,9,9,
//    10,10,10,10,11,11,11,12,12,12,13,13,13,14,14,14,
//    15,15,16,16,16,17,17,18,18,19,19,20,21,21,22,22,
//    23,24,24,25,26,26,27,28,28,29,30,31,32,33,34,34,
//    35,36,37,38,39,41,42,43,44,45,47,48,49,50,52,53,
//    55,56,58,59,61,63,65,66,68,70,72,74,76,78,80,83,
//    85,87,90,92,95,97,100,103,106,108,111,115,118,121,124,128,
//    131,135,139,142,146,150,155,159,163,168,172,177,182,187,192,198,
//    203,209,215,221,227,233,239,246,253,260,267,274,282,290,298,306,
//
//    315,323,332,341,351,361,370,95,98,100,103,106,109,112,115,118,
//    122,125,128,132,136,139,143,147,151,155,160,164,169,173,178,183,
//    188,193,199,204,210,216,222,228,234,241,247,254
//};
//
//
///** \brief Logarithmic dimming table (last 51 values).
// *
// * To save some program space, the precision of these values can be lowered by
// * dropping the two least significant bits.
// */
//#ifdef DALI_USE_FULL_TABLE_PRECISION
//static const uint16_t logDimmingTableHigh[51] =
//{
//    261,268,276,284,291,299,308,316,325,334,343,353,362,373,383,393,
//    404,416,427,439,451,463,476,489,503,517,531,546,561,577,593,609,
//    626,643,661,679,698,717,737,758,779,800,822,845,868,892,917,943,
//    969,995,1023
//};
//#else
//static const uint8_t logDimmingTableHigh[51] =
//{
//    65,67,69,71,72,74,77,79,81,83,85,88,90,93,95,98,101,104,106,109,112,115,119,
//    122,125,129,132,136,140,144,148,152,156,160,165,169,174,179,184,189,194,200,
//    205,211,217,223,229,235,242,248,255
//};
//#endif /* DALI_USE_FULL_TABLE_PRECISION */

/* 12-bit PWM values calculated according to dali standard. The 4 high bits are
 * stuffed 2 in a byte {#1#0, #3#2,....,#255#254} */
PROGMEM uint8_t const flash_high_pwm_val[]
	= {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	   0x00, 0x00, 0x00, 0x00, 0x10, 0x11, 0x11, 0x11,
	   0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
	   0x11, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22,
	   0x32, 0x33, 0x33, 0x33, 0x33, 0x33, 0x44, 0x44,
	   0x44, 0x44, 0x55, 0x55, 0x55, 0x65, 0x66, 0x66,
	   0x77, 0x77, 0x87, 0x88, 0x98, 0x99, 0xA9, 0xAA,
	   0xBA, 0xBB, 0xCC, 0xDC, 0xDD, 0xEE, 0xFF, 0xFF};

PROGMEM uint8_t const flash_low_pwm_val[]
	=  {0x00, 0x04, 0x04, 0x04, 0x04, 0x05, 0x05, 0x05,
            0x05, 0x05, 0x05, 0x05, 0x06, 0x06, 0x06, 0x06,
	    0x06, 0x06, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
	    0x08, 0x08, 0x08, 0x08, 0x09, 0x09, 0x09, 0x09,
	    0x0A, 0x0A, 0x0A, 0x0A, 0x0B, 0x0B, 0x0B, 0x0C,
	    0x0C, 0x0C, 0x0D, 0x0D, 0x0D, 0x0E, 0x0E, 0x0E,
	    0x0F, 0x0F, 0x10, 0x10, 0x10, 0x11, 0x11, 0x12,
	    0x12, 0x13, 0x13, 0x14, 0x15, 0x15, 0x16, 0x16,
	    0x17, 0x18, 0x18, 0x19, 0x1A, 0x1A, 0x1B, 0x1C,
	    0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x22,
	    0x23, 0x24, 0x25, 0x26, 0x27, 0x29, 0x2A, 0x2B,
	    0x2C, 0x2D, 0x2F, 0x30, 0x31, 0x32, 0x34, 0x35,
	    0x37, 0x38, 0x3A, 0x3B, 0x3D, 0x3F, 0x41, 0x42,
	    0x44, 0x46, 0x48, 0x4A, 0x4C, 0x4E, 0x50, 0x53,
	    0x55, 0x57, 0x5A, 0x5C, 0x5F, 0x61, 0x64, 0x67,
	    0x6A, 0x6C, 0x6F, 0x73, 0x76, 0x79, 0x7C, 0x80,
	    0x83, 0x87, 0x8B, 0x8E, 0x92, 0x96, 0x9B, 0x9F,
	    0xA3, 0xA8, 0xAC, 0xB1, 0xB6, 0xBB, 0xC0, 0xC6,
	    0xCB, 0xD1, 0xD7, 0xDD, 0xE3, 0xE9, 0xEF, 0xF6,
	    0xFD, 0x04, 0x0B, 0x12, 0x1A, 0x22, 0x2A, 0x32,
	    0x3B, 0x43, 0x4C, 0x55, 0x5F, 0x69, 0x72, 0x7D,
	    0x87, 0x92, 0x9D, 0xA9, 0xB4, 0xC1, 0xCD, 0xDA,
	    0xE7, 0xF4, 0x02, 0x10, 0x1F, 0x2E, 0x3D, 0x4D,
	    0x5E, 0x6E, 0x80, 0x91, 0xA4, 0xB6, 0xC9, 0xDD,
	    0xF1, 0x06, 0x1C, 0x32, 0x48, 0x60, 0x78, 0x90,
	    0xA9, 0xC3, 0xDE, 0xF9, 0x16, 0x33, 0x50, 0x6F,
	    0x8E, 0xAF, 0xD0, 0xF2, 0x15, 0x39, 0x5E, 0x84,
	    0xAB, 0xD3, 0xFC, 0x27, 0x52, 0x7F, 0xAD, 0xDD,
	    0x0D, 0x3F, 0x73, 0xA7, 0xDE, 0x15, 0x4F, 0x89,
	    0xC6, 0x04, 0x44, 0x86, 0xC9, 0x0E, 0x56, 0x9F,
	    0xEA, 0x37, 0x87, 0xD9, 0x2D, 0x83, 0xDB, 0x37,
	    0x94, 0xF4, 0x57, 0xBD, 0x25, 0x91, 0xFF, 0xFF};



/**********************Function implementations********************************/

void lamp_init()
{
      /* TCD used for DALI fading output */
      /*One ramp mode*/
      TCD0.CTRLB = 0x00;
      TCD0.CMPBCLR = DALI_PWM_TC_TOP;
      /*LED off firstly*/
      TCD0.CMPBSET = DALI_PWM_TC_TOP;
      /*System clock; Counter Prescaler 4*/
      TCD0.CTRLA = 0x69;
}


void lamp_setPower(uint8_t value, uint8_t nonLogDimming)
{
    uint16_t dutycycle;
    
#ifdef DALI_USE_DEVICE_TYPE_6
    if (nonLogDimming == 0)
    {
#endif /* DALI_USE_DEVICE_TYPE_6 */
        // Retrieve value from table
        /* Upper 4 of the 12 bits PWM value, stored two 4-bit values per
	* byte */
        dutycycle = flash_high_pwm_val[value >> 1];
        dutycycle = (dutycycle << 8);
      
        if ((value & 0x01) == 0x00) 
        {
            /* Even number, data is stored as lower 4 bit */
            dutycycle &= 0x0F00;
        } 
        else 
        {
            /* Odd number, data is stored as upper 4 bit */
            dutycycle = (dutycycle & 0xF000) >> 4;
        }
        dutycycle |= flash_low_pwm_val[value];
        
#ifdef DALI_USE_DEVICE_TYPE_6
    }
    else if (nonLogDimming == 1)
    {
        // Use linear dimming
        /* Linear curve, n*4095/254 */
        dutycycle = ((uint32_t)value * DALI_PWM_TC_TOP) / 254;
	
    }
    else
    {
        dutycycle = 0;
    }
#endif /* DALI_USE_DEVICE_TYPE_6 */

    /* Update CC value in TCD */
    TCD0.CMPBSET = dutycycle ^ (uint16_t)DALI_PWM_TC_TOP;
    TCD0.CTRLE = 1;
}


uint8_t lamp_getStatus()
{
    uint8_t retVal = LAMP_STATUS_OK;

    // This example uses button combinations on the Lighting Communication
    // Main Board to simulate different error conditions.

    // S5 + S2 = Load Increase
    // S5 + S3 = Load Decrease

    // S4 + S2 = Thermal Overload
    // S4 + S3 = Thermal Shutdown

    // S2 = Open Circuit
    // S3 = Short Circuit

//    if (PORTDbits.RD2 == 0)
//    {
//        // S5 pressed configures for load increase/decrease
//        if (PORTDbits.RD6 == 0)
//        {
//            // S2 - Load Increase
//            retVal |= LAMP_STATUS_LOAD_INCREASE;
//        }
//        if (PORTDbits.RD5 == 0)
//        {
//            // S3 - Load Decrease
//            retVal |= LAMP_STATUS_LOAD_DECREASE;
//        }
//    }
//    else if (PORTDbits.RD4 == 0)
//    {
//        // S4 selects thermal overload/shutdown
//        if (PORTDbits.RD6 == 0)
//        {
//            // S2 - Thermal Overload
//            retVal |= LAMP_STATUS_THERMAL_OVEROAD;
//        }
//        if (PORTDbits.RD5 == 0)
//        {
//            // S3 - Thermal Shutdown
//            retVal |= LAMP_STATUS_THERMAL_SHUTDOWN;
//        }
//    }
//    else
//    {
//        // Nothing else pressed
//        if (PORTDbits.RD6 == 0)
//        {
//            // S2 - Open Circuit
//            retVal |= LAMP_STATUS_OPEN_CIRCUIT;
//        }
//        if (PORTDbits.RD5 == 0)
//        {
//            // S3 - Short Circuit
//            retVal |= LAMP_STATUS_SHORT_CIRCUIT;
//        }
//    }

    return retVal;
}


#ifdef DALI_USE_DEVICE_TYPE_6
uint8_t lamp_getOperatingMode()
{
    return OPERATING_MODE_CURRENT_CONTROLLED;
}
#endif /* DALI_USE_DEVICE_TYPE_6 */


#ifdef DALI_USE_DEVICE_TYPE_6

uint8_t lamp_getDimmingTableValue(uint8_t index)
{
      uint16_t dimmingTableValue = 0;
      
      dimmingTableValue = flash_high_pwm_val[index >> 1];
      dimmingTableValue = (dimmingTableValue << 8);
      if ((dimmingTableValue & 0x01) == 0x00) 
      {
            /* Even number, data is stored as lower 4 bit */
            dimmingTableValue &= 0x0F00;
       } 
       else 
       {
           /* Odd number, data is stored as upper 4 bit */
           dimmingTableValue = (dimmingTableValue & 0xF000) >> 4;
       }
       dimmingTableValue |= flash_low_pwm_val[index];
      return (dimmingTableValue >> 4);
//    if (index < 204)
//    {
//        return logDimmingTableLow[index] >> 2;
//    }
//    else
//    {
//        index -= 204;
//	return logDimmingTableHigh[index] >> 2;
//    }
}
#endif /* DALI_USE_DEVICE_TYPE_6 */

/**********************Function implementations*(END)**************************/
